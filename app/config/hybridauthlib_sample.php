<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* ----------------------------------------------------------------------------
/* HybridAuth Guide: http://hybridauth.github.io/hybridauth/userguide.html
/* ------------------------------------------------------------------------- */

$config =
	array(

		'Hauth_base_url' => 'social_auth/endpoint',

		"providers" => array (

			"Google" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "347686766444-e8i630820dpc2plg99et58qen0cvod8a.apps.googleusercontent.com", "secret" => "u8XUKbY9WhZchu1Q5PHyJp2G" ),
			),

			"Facebook" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "205599470729170", "secret" => "eea0277d47bd8739cf47c5c33e5040fa" ),
				"scope"   => "email",
			),

			"Twitter" => array (
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"Yahoo" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" ),
			),

			"Live" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			"MySpace" => array (
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"OpenID" => array (
				"enabled" => false
			),

			"LinkedIn" => array (
				"enabled" => false,
				"keys"    => array ( "key" => "", "secret" => "" )
			),

			"Foursquare" => array (
				"enabled" => false,
				"keys"    => array ( "id" => "", "secret" => "" )
			),

			"AOL"  => array (
				"enabled" => false
			),
		),

		"debug_mode" => (ENVIRONMENT != 'production'),
		"debug_file" => APPPATH.'logs/hybridauth'.date('Y-m-d').'.php',
	);

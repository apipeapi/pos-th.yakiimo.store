<?php defined('BASEPATH') or exit('No direct script access allowed');

class Consignment extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('consignment', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('consignment_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
		
		$this->role_owner = ($this->Owner || $this->Admin) ? true : false;
        $this->data['role_owner'] = $this->role_owner;
		
    }

    function index()
    {
        $this->sma->checkPermissions();
        $temp_warhouse = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_warehouses"
            ,"id > 0 order by id asc"
            ,"arr"
        );
        $this->data['api_warehouse'] = $temp_warhouse;

        $sort_by = $this->input->get('sort_by');
        $sort_order = $this->input->get('sort_order');

        $per_page = $this->input->get('per_page') != '' ? $this->input->get('per_page') : 100;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;
        
        if ($page != '')
            $offset_no = ($page - 1 ) * $per_page;
        else
            $offset_no = 0;

        $search = $this->input->get('search');
        $search = $this->db->escape_str($search);
        
        $temp_url = '';
        if ($page != '') 
            $temp_url .= '?page='; 
        if ($per_page != '') $temp_url .= '&per_page='.$per_page;
        foreach ($_GET as $name => $value) {
            if ($name != 'page' && $name != 'per_page')
                if ($value != '') $temp_url .= '&'.$name.'='.$value;
        }

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_consignment',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => $sort_by,
            'sort_order' => $sort_order,
            'limit' => $per_page,
            'offset_no' => $offset_no,
        );
        $select_data = $this->consignment_model->api_get_consignment($config_data);

        $config_data = array(
            'lg' => 'en',
            'table_name' => 'sma_consignment',
            'table_id' => 'id',
            'search' => $search,
            'sort_by' => '',
            'sort_order' => '',
            'limit' => '',
            'offset_no' => '',
        );
        $temp_count = $this->consignment_model->api_get_consignment($config_data);
        $total_record = count($temp_count);


        /* config pagination */
        $this->data['select_data'] = $select_data;

        $this->data['page'] = $page;
        $this->data['search'] = $search;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['per_page'] = $per_page;
        $this->data['total_rows_sale'] = $total_record;
        $this->data['index_page'] = 'admin/consignment';
        $this->data['show_data'] = $page;

        $config_base_url = site_url() . 'admin/consignment'; //your url where the content is displayed
        $config['base_url'] = $config_base_url.$temp_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $total_record; //$this->data['total_rows_sale'];        
        $this->load->library('pagination');   
        $this->pagination->initialize($config);
        /** end config pagination */
        /**Showing Label Data At Table Footer */
        $showing = $total_record != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $total_record) ?  $per_page  :  $total_record;
        $label_show = "Showing " . $showing . " to ".$label_show_to." of : " . number_format($total_record) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $total_record;
        $show_page_no = $show_page > $total_record ? $total_record : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($total_record) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $temp = array();
        $temp[0]['value'] = 'pending';
        $temp[0]['name'] = lang('Pending');
        $temp[1]['value'] = 'completed';
        $temp[1]['name'] = lang('completed');
        $this->data['sale_status'] = $temp;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('consignment')));
        $meta = array('page_title' => lang('consignment'), 'bc' => $bc);
        $this->page_construct('consignment/index', $meta, $this->data);
    }

    public function index_bk($warehouse_id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }
		if (!$this->role_owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('consignment')));
        $meta = array('page_title' => lang('consignment'), 'bc' => $bc);

        /* config default, query data*/
        $sort_by = $this->input->post('sort_by');
        $sort_order = $this->input->post('sort_order');

        $per_page = $this->input->post('per_page') != '' ? $this->input->post('per_page') : 50;
        $page = $this->input->get('page') != '' ? $this->input->get('page') : 1;

        $offset_no = $page != '' ? ($page - 1 ) * $per_page : $offset_no;

        $search = $this->input->post('search');
        if ($this->input->get('search') != '') $search = $this->input->get('search');

        $data_sale = $this->consignment_model
                                ->get_paging_data($warehouse_id, $search, 
                                    $sort_by, $sort_order, 
                                    $per_page, $offset_no
                                )->result();

/*
        $count_data_sale = $this->consignment_model
                            ->get_paging_data(
                                $warehouse_id, 
                                $search
                            )->num_rows();
  */ 
        /* config pagination */
        if ($warehouse_id) {
            $config_base_url = site_url() . 'admin/consignment/' . $warehouse_id; //your url where the content is displayed
        } else {
            $config_base_url = site_url() . 'admin/consignment'; //your url where the content is displayed
        }
        $this->data['per_page'] = $per_page;
        $this->data['sort_by'] = $sort_by;
        $this->data['sort_order'] = $sort_order;
        $this->data['sl_sale'] = $data_sale;
        $this->data['total_rows_sale'] = $count_data_sale;
        $this->data['index_page'] = 'admin/consignment/index';
        $this->data['show_data'] = $page;

        $config['base_url'] = $config_base_url; //your url where the content is displayed
        $config['query_string_segment'] = 'page';
        $config['per_page'] = $per_page; //$per_page;
        $config['num_links'] = 5;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination pagination-new-bg">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE;
        $config['last_link'] = FALSE;
        $config['prev_link'] = '&lt; Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next &gt;';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['total_rows'] = $count_data_sale; //$this->data['total_rows_sale'];
        $this->pagination->initialize($config);
        /** end config pagination */

        /**Showing Label Data At Table Footer */
        $showing = $count_data_sale != '' ? 1 : 0;
        $label_show_to= ($page == '' || $per_page < $count_data_sale) ?  $per_page  :  $count_data_sale;
        $label_show = "Showing" . $showing . " to ".$label_show_to." of : " . number_format($count_data_sale) . " entries";

        $show_page = $page > 1 ? ($page * $per_page) : $count_data_sale;
        $show_page_no = $show_page > $count_data_sale ? $count_data_sale : $page * $per_page;
        $label_show_page = "Showing " . (($page - 1) * ($per_page) + 1) . " to " . $show_page_no . " (filtered from " . number_format($count_data_sale) . " total entries) ";
        $this->data['show'] = $search != '' || $page > 1 ? $label_show_page : $label_show;
        /** End Showing Label Data At Table Footer */ 

        $this->page_construct('consignment/index', $meta, $this->data);
    }

    public function getSales($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $duplicate_link = anchor('admin/sales/add?sale_id=$1', '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/sales/add_payment/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), 'data-toggle="modal" data-target="#myModal"');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('admin/sales/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $return_link = anchor('admin/sales/return_sale/$1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_sale") . "</b>' data-content=\"<p>"
        . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete/$1') . "'>"
        . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
        . lang('delete_sale') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li>' . $detail_link . '</li>
            <li>' . $duplicate_link . '</li>
            <li>' . $payments_link . '</li>
            <li>' . $add_payment_link . '</li>
            <li>' . $packagink_link . '</li>
            <li>' . $add_delivery_link . '</li>
            <li>' . $edit_link . '</li>
            <li>' . $pdf_link . '</li>
            <li>' . $email_link . '</li>
            <li>' . $return_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';
        //$action = '<div class="text-center">' . $detail_link . ' ' . $edit_link . ' ' . $email_link . ' ' . $delete_link . '</div>';

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("{$this->db->dbprefix('sales')}.id as id, DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('sales')}.customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.attachment, return_id")
                ->from('sales')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("{$this->db->dbprefix('sales')}.id as id, DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('sales')}.customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.attachment, return_id")
                ->from('sales');
        }
        if ($this->input->get('shop') == 'yes') {
            $this->datatables->where('shop', 1);
        } elseif ($this->input->get('shop') == 'no') {
            $this->datatables->where('shop !=', 1);
        }
        if ($this->input->get('delivery') == 'no') {
            $this->datatables->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR {$this->db->dbprefix('deliveries')}.status IS NULL)", NULL);
        }
        if ($this->input->get('attachment') == 'yes') {
            $this->datatables->where('payment_status !=', 'paid')->where('attachment !=', NULL);
        }
        $this->datatables->where('pos !=', 1); // ->where('sale_status !=', 'returned');
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        
        echo $this->datatables->generate();
    }

    public function modal_view($id = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->consignment_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->consignment_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItems($inv->return_id) : NULL;

        $this->load->view($this->theme . 'sales/modal_view', $this->data);
    }
    public function view($id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->consignment_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->consignment_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->consignment_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['paypal'] = $this->consignment_model->getPaypalSettings();
        $this->data['skrill'] = $this->consignment_model->getSkrillSettings();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->page_construct('sales/view', $meta, $this->data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->consignment_model->getInvoiceConsignmentByID($id);
        if ($_GET['print'] == 1) {
            if ($inv->reference_no != '') {
                if ($inv->sale_status == 'print needed' || $inv->sale_status == 'pending' || $inv->sale_status == '') {
                    $update_array = array(
                        'sale_status' => 'preparing',                 
                    );
                    $this->site->api_update_table('sma_consignment',$update_array,"id = ".$id);
                    //$this->api_send_mail_customer_order($id);
                }                
            }
            else {
                $this->session->set_flashdata('error', lang("action_print_error"));
                admin_redirect("consignment");                                
            }
        }
        
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID_v2($inv->customer_id);
        $this->data['payments'] = $this->consignment_model->getPaymentsForSale($id);

        if ($this->data['customer']->vat_no) {
            $temp = $this->site->api_select_some_fields_with_where("id
                "
                ,"sma_companies"
                ,"name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'"
                ,"arr"
            );
            if (count($temp) > 0)
                $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
        }
        else
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);

        $this->data['inv'] = $inv;
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");
        $temp = $this->site->api_select_some_fields_with_where("id
            ".$temp_field
            ,"sma_consignment"
            ,"id = ".$id
            ,"arr"
        );
        foreach ($temp[0] as $key => $value) {
            $this->data['inv']->{$key} = $value;
        }        
                
        $this->data['rows'] = $this->consignment_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItems($inv->return_id) : NULL;

        if ($this->data['customer']->parent_id != '') {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$this->data['customer']->parent_id
                ,"arr"
            );
            if (count($temp) > 0)
                $this->data['inv']->parent_name = $temp[0]['company'];
        }

        if (is_int(strpos($inv->reference_no,"SL"))) {
            $temp_riel = $this->site->api_select_some_fields_with_where("*
                "
                ,"sma_currencies"
                ,"code = 'KHR' order by date desc limit 1"
                ,"arr"
            );
            $this->data['riel_name'] = $temp_riel[0]['name'];

            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");                
            $temp_2 = $this->site->api_select_some_fields_with_where("id 
                ".$temp_field
                ,"sma_sales"
                ,"id = ".$id
                ,"arr"
            );
            if ($temp_2[0]['kh_currency_rate'] > 0)
                $temp_riel[0]['rate'] = intval($temp_2[0]['kh_currency_rate']);                   
            $temp = number_format($temp_riel[0]['rate'],0);

            $this->data['riel_rate'] = '1$ = '.$temp.$temp_riel[0]['code'];
            $this->data['total_riel'] = $inv->grand_total * $temp_riel[0]['rate'];
        }
        
        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'consignment/pdf_consignment', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }

        if ($view) {
            $this->load->view($this->theme . 'consignment/pdf_consignment', $this->data);
        } elseif ($save_bufffer) {
            //$this->data['biller']->invoice_footer
            $temp = 'Thank you for your always support';
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $temp);
        } else {
			//$this->sma->print_arrays($html);
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

    public function combine_pdf($sales_id)
    {

        foreach ($sales_id as $id) {
            if ($id != '') {
                $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
                $inv = $this->consignment_model->getInvoiceConsignmentByID($id);
                if (!$this->session->userdata('view_right')) {
                    $this->sma->view_rights($inv->created_by);
                }

                $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
                $this->data['customer'] = $this->site->getCompanyByID_v2($inv->customer_id);
                //$this->data['payments'] = $this->consignment_model->getPaymentsForSale($id);
        
                if ($this->data['customer']->vat_no) {
                    $temp = $this->site->api_select_some_fields_with_where("id
                        "
                        ,"sma_companies"
                        ,"name = 'DAISHIN TRADING (CAMBODIA) CO., LTD'"
                        ,"arr"
                    );
                    if (count($temp) > 0)
                        $this->data['biller'] = $this->site->getCompanyByID($temp[0]['id']);
                }
                else
                    $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
                
                $this->data['user'] = $this->site->getUser($inv->created_by);
                $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
                $this->data['inv'] = $inv;
                $this->data['rows'] = $this->consignment_model->getAllInvoiceItems($id);
                $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceByID($inv->return_id) : NULL;
                $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItems($inv->return_id) : NULL;

                if ($this->data['customer']->parent_id != '') {
                    $temp = $this->site->api_select_some_fields_with_where("
                        *     
                        "
                        ,"sma_companies"
                        ,"id = ".$this->data['customer']->parent_id
                        ,"arr"
                    );
                    if (count($temp) > 0)
                        $this->data['inv']->parent_name = $temp[0]['company'];
                }


                if (is_int(strpos($inv->reference_no,"SL"))) {
                    $temp_riel = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_currencies"
                        ,"code = 'KHR' order by date desc limit 1"
                        ,"arr"
                    );
                    $this->data['riel_name'] = $temp_riel[0]['name'];
                        
                    $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_sales'");                
                    $temp_2 = $this->site->api_select_some_fields_with_where("id 
                        ".$temp_field
                        ,"sma_sales"
                        ,"id = ".$id
                        ,"arr"
                    );
                    if ($temp_2[0]['kh_currency_rate'] > 0)
                        $temp_riel[0]['rate'] = intval($temp_2[0]['kh_currency_rate']);
                    $temp = number_format($temp_riel[0]['rate'],0);

                    $this->data['riel_rate'] = '1$ = '.$temp.$temp_riel[0]['code'];
                    $this->data['total_riel'] = $inv->grand_total * $temp_riel[0]['rate'];
                }
                $html_data = $this->load->view($this->theme . 'consignment/pdf_consignment', $this->data, true);
                if (! $this->Settings->barcode_img) {
                    $html_data = preg_replace("'\<\?xml(.*)\?\>'", '', $html_data);
                }

                $html[] = array(
                    'content' => $html_data,
                    'footer' => $this->data['biller']->invoice_footer,
                );
            }
            
        }

        $name = lang("Sale_Consignment_".date('Y-m-d H-i-s')) . ".pdf";
        $this->sma->generate_pdf($html, $name);

    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->consignment_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->company && $customer->company != '-' ? '('.$customer->company.')' : '',
                'order_link' => $inv->shop ? shop_url('orders/'.$inv->id.'/'.($this->loggedIn ? '' : $inv->hash)) : base_url(),
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            $paypal = $this->consignment_model->getPaypalSettings();
            $skrill = $this->consignment_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=FC-BuyNow&rm=2&return=' . admin_url('sales/view/' . $inv->id) . '&cancel_return=' . admin_url('sales/view/' . $inv->id) . '&notify_url=' . admin_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';

            }
            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                $btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . admin_url('sales/view/' . $inv->id) . '&cancel_url=' . admin_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . admin_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }

            $btn_code .= '<div class="clearfix"></div></div>';
            $message = $message . $btn_code;
            $attachment = $this->pdf($id, null, 'S');

            try {
                if ($this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc)) {
                    delete_files($attachment);
                    $this->session->set_flashdata('message', lang("email_sent"));
                    admin_redirect("sales");
                }
            } catch (Exception $e) {
                $this->session->set_flashdata('error', $e->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } elseif ($this->input->post('send_email')) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            if (file_exists('./themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html')) {
                $sale_temp = file_get_contents('themes/' . $this->Settings->theme . '/admin/views/email_templates/sale.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/admin/views/email_templates/sale.html');
            }

            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('invoice') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $sale_temp),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);

            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/email', $this->data);
        }
    }

    /* ------------------------------------------------------------------ */

    public function add($quote_id = null)
    {
        $this->sma->checkPermissions();
        $sale_id = $this->input->get('sale_id') ? $this->input->get('sale_id') : NULL;

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('due_date', lang("due_date"), 'required');

        if ($this->form_validation->run() == true) {
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;
            $reference = $this->input->post('reference_no'); 

            $date = $this->input->post('date');
            if ($date == '')
                $date = date('Y-m-d H:i:s');
            else
                $date = $this->sma->fld(trim($this->input->post('date').':'.date('s')));

            $due_date = $this->sma->fld(trim($this->input->post('due_date')));
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');//18-01-2019
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;            

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->consignment_model->getProductByCode($item_code) : null;
                    // $unit_price = $real_unit_price;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            
            if ($customer_details->vat_no) $temp_order_tax = 2; else $temp_order_tax = 1;
            $order_tax = $this->site->calculateOrderTax($temp_order_tax, ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);

            $add_ons = 'initial_first_add_ons:{}:';                
            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"add_ons_"))) {
                    $temp_name = str_replace('add_ons_','',$name);
                    $add_ons .= $temp_name.':{'.$value.'}:';
                }
            }

            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $temp_order_tax,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => 'print needed',
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'add_ons' => $add_ons,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($payment_status == 'partial' || $payment_status == 'paid') {
                if ($this->input->post('paid_by') == 'deposit') {
                    if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                        $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                if ($this->input->post('paid_by') == 'gift_card') {
                    $gc = $this->site->getGiftCardByNO($this->input->post('gift_card_no'));
                    $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                    $gc_balance = $gc->balance - $amount_paying;
                    $payment = array(
                        'date' => $date,
                        'reference_no' => $this->input->post('payment_reference_no'),
                        'amount' => $this->sma->formatDecimal($amount_paying),
                        'paid_by' => $this->input->post('paid_by'),
                        'cheque_no' => $this->input->post('cheque_no'),
                        'cc_no' => $this->input->post('gift_card_no'),
                        'cc_holder' => $this->input->post('pcc_holder'),
                        'cc_month' => $this->input->post('pcc_month'),
                        'cc_year' => $this->input->post('pcc_year'),
                        'cc_type' => $this->input->post('pcc_type'),
                        'created_by' => $this->session->userdata('user_id'),
                        'note' => $this->input->post('payment_note'),
                        'type' => 'received',
                        'gc_balance' => $gc_balance,
                    );
                } else {
                    $payment = array(
                        'date' => $date,
                        'reference_no' => $this->input->post('payment_reference_no'),
                        'amount' => $this->sma->formatDecimal($this->input->post('amount-paid')),
                        'paid_by' => $this->input->post('paid_by'),
                        'cheque_no' => $this->input->post('cheque_no'),
                        'cc_no' => $this->input->post('pcc_no'),
                        'cc_holder' => $this->input->post('pcc_holder'),
                        'cc_month' => $this->input->post('pcc_month'),
                        'cc_year' => $this->input->post('pcc_year'),
                        'cc_type' => $this->input->post('pcc_type'),
                        'created_by' => $this->session->userdata('user_id'),
                        'note' => $this->input->post('payment_note'),
                        'type' => 'received',
                    );
                }
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->addSale($data, $products, $payment))
        {
            $this->session->set_userdata('remove_slls', 1);

            $this->session->set_flashdata('message', lang("Consignment_added"));
            admin_redirect("consignment");
        } else {

            if ($quote_id || $sale_id) {
                if ($quote_id) {
                    $this->data['quote'] = $this->consignment_model->getQuoteByID($quote_id);
                    $items = $this->consignment_model->getAllQuoteItems($quote_id);
                } elseif ($sale_id) {
                    $this->data['quote'] = $this->consignment_model->getInvoiceByID($sale_id);
                    $items = $this->consignment_model->getAllInvoiceItems($sale_id);
                }
                krsort($items);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity;
                    $row->base_quantity = $item->quantity;
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                    $row->unit = $item->product_unit_id;
                    $row->qty = $item->unit_quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $options = $this->consignment_model->getProductOptions($row->id, $item->warehouse_id);
                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }
                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->consignment_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->getUnitsByBUID($row->base_unit);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;

                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                }
                $this->data['quote_items'] = json_encode($pr);
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['quote_id'] = $quote_id ? $quote_id : $sale_id;
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['units'] = $this->site->getAllBaseUnits();
            //$this->data['currencies'] = $this->consignment_model->getAllCurrencies();
            $this->data['slnumber'] = ''; //$this->site->getReference('so');
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $temp = $this->site->api_select_some_fields_with_where("company
                "
                ,"sma_companies"
                ,"id > 0 order by company asc"
                ,"obj"
            );
            $this->data['company'] = $temp;
            $this->load->admin_model('companies_model');           
            $this->data['sales_person'] = $this->companies_model->getAllSalePerson();            
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('consignment'), 'page' => lang('consignment')), array('link' => '#', 'page' => lang('add_consignment')));
            $meta = array('page_title' => lang('add_consignment'), 'bc' => $bc);
            $this->page_construct('consignment/add', $meta, $this->data);
        }
    }

    /* ------------------------------------------------------------------------ */

    public function edit($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $temp = $this->consignment_model->api_consignment_edit_check($id);
        if ($temp['error'] != '') {
            $this->session->set_flashdata('error', $temp['error']);
            redirect($_SERVER["HTTP_REFERER"]);                        
        }

        $inv = $this->consignment_model->getInvoiceByID($id);
        
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('due_date', lang("due_date"), 'required');
                        
        
        if ($this->form_validation->run() == true) {
            
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;


            if ($this->Owner || $this->Admin) {
                $temp = substr($inv->date,-2);
                $date = $this->sma->fld(trim($this->input->post('date').':'.$temp));
            } else {
                $date = $inv->date;
            }

            $due_date = $this->sma->fld(trim($this->input->post('due_date')));

            $warehouse_id = $this->input->post('warehouse');
            //$customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->consignment_model->getProductByCode($item_code) : null;

                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);

            $temp_order_tax_id = $this->input->post('order_tax_id');
            if ($temp_order_tax_id <= 0)
                $temp_order_tax_id = 1;

            $order_tax = $this->site->calculateOrderTax($temp_order_tax_id, ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $temp_order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
        }

        if ($this->form_validation->run() == true && $this->consignment_model->updateSale($id, $data, $products)) {
            $delivery_date = '';
            if ($this->input->post('delivery_date') != '')
                $delivery_date = $this->sma->fld(trim($this->input->post('delivery_date')));
            $kh_currency_rate = $this->input->post('kh_currency_rate');   


            $config_data = array(
                'table_name' => 'sma_consignment',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'kh_currency_rate',
                'add_ons_value' => $kh_currency_rate,
            );
            $this->site->api_update_add_ons_field($config_data);                                
            $config_data = array(
                'table_name' => 'sma_consignment',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $id,
                'add_ons_title' => 'delivery_date',
                'add_ons_value' => $delivery_date,
            );
            $this->site->api_update_add_ons_field($config_data);   

            foreach($_POST as $name => $value) {
                if (is_int(strpos($name,"add_ons_"))) {
                    $value = $this->input->post($name);
                    $temp_name = str_replace('add_ons_','',$name);                        
                    $config_data = array(
                        'table_name' => 'sma_consignment',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => $temp_name,
                        'add_ons_value' => $value,                    
                    );
                    $this->site->api_update_add_ons_field($config_data);                        
                }
            }     
            
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("consignment_updated"));
            admin_redirect('consignment');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $temp = $this->consignment_model->getInvoiceConsignmentByID($id);
            $this->data['inv'] = $temp;
            
            $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_consignment'");
            $temp = $this->site->api_select_some_fields_with_where("id
                ".$temp_field
                ,"sma_consignment"
                ,"id = ".$id
                ,"arr"                
            );
            foreach ($temp[0] as $key => $value) {
                $this->data['inv']->{$key} = $value;
            }
            if ($this->data['inv']->company_branch > 0) {
                $temp2 = $this->site->api_select_some_fields_with_where("id,
                    getTranslate(translate,'en','".f_separate."','".v_separate."') as title,
                    getTranslate(add_ons,'branch_number','".f_separate."','".v_separate."') as branch_number
                    "
                    ,"sma_company_branch"
                    ,"id = ".$this->data['inv']->company_branch." order by title asc"
                    ,"arr"
                );
                $this->data['inv']->temp_company_branch_id = $temp2[0]['id'];
                $this->data['inv']->temp_company_branch_title = $temp2[0]['title'];
                $this->data['inv']->temp_company_branch_branch_number = $temp2[0]['branch_number'];
            }

            $inv_items = $this->consignment_model->getAllInvoiceItems($id);
            $c = rand(100000, 9999999);
            $pr = array();
            if($inv_items) {
                foreach ($inv_items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                        $row->quantity = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        $row->quantity = 0;
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->base_quantity = $item->quantity;
                    $row->base_unit = !empty($row->unit) ? $row->unit : $item->product_unit_id;
                    $row->base_unit_price = !empty($row->price) ? $row->price : $item->unit_price;
                    $row->unit = $item->product_unit_id;
                    $row->qty = $item->unit_quantity;
                    $row->quantity += $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = $item->serial_no;
                    $row->option = $item->option_id;
                    $options = $this->consignment_model->getProductOptions($row->id, $item->warehouse_id);

                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->site->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            $option_quantity += $item->quantity;
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }

                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->consignment_model->getProductComboItems($row->id, $item->warehouse_id);
                        $te = $combo_items;
                        foreach ($combo_items as $combo_item) {
                            $combo_item->quantity = $combo_item->qty * $item->quantity;
                        }
                    }
                    $units = !empty($row->base_unit) ? $this->site->getUnitsByBUID($row->base_unit) : NULL;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;

                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                }
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['billers'] = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $this->site->getAllCompanies('biller') : null;
            $this->data['units'] = $this->site->getAllBaseUnits();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->load->admin_model('companies_model');
            $this->data['sales_person'] = $this->companies_model->getAllSalePerson();
            $this->load->admin_model('sales_model');
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('consignment'), 'page' => lang('consignment')), array('link' => '#', 'page' => lang('edit_consignment')));
            $meta = array('page_title' => lang('edit_consignment'), 'bc' => $bc);
            $this->page_construct('consignment/edit', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function return_sale($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->consignment_model->getInvoiceByID($id);
        if ($sale->return_id) {
            $this->session->set_flashdata('error', lang("sale_already_returned"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('return_surcharge', lang("return_surcharge"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $customer_details = $this->site->getCompanyByID($sale->customer_id);
            $biller_details = $this->site->getCompanyByID($sale->biller_id);

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = (0-$_POST['quantity'][$r]);
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = (0-$_POST['product_base_quantity'][$r]);

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->consignment_model->getProductByCode($item_code) : null;
                    // $unit_price = $real_unit_price;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal(($unit_price - $pr_discount), 4);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity, 4);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = $this->sma->formatDecimal((($item_net_price * $item_unit_quantity) + $pr_item_tax), 4);
                    $unit = $item_unit ? $this->site->getUnitByID($item_unit) : FALSE;

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $item_unit,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $sale->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'sale_item_id' => $sale_item_id,
                    );

                    $si_return[] = array(
                        'id' => $sale_item_id,
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'option_id' => $item_option,
                        'quantity' => (0-$item_quantity),
                        'warehouse_id' => $sale->warehouse_id,
                        );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('discount') ? $this->input->post('order_discount') : null, ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax, 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($return_surcharge) - $order_discount), 4);
            $data = array('date' => $date,
                'sale_id' => $id,
                'reference_no' => $sale->reference_no,
                'customer_id' => $sale->customer_id,
                'customer' => $sale->customer,
                'biller_id' => $sale->biller_id,
                'biller' => $sale->biller,
                'warehouse_id' => $sale->warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('discount') ? $this->input->post('order_discount') : null,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'return_sale_ref' => $reference,
                'sale_status' => 'returned',
                'pos' => $sale->pos,
                'payment_status' => $sale->payment_status == 'paid' ? 'due' : 'pending',
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');
                $payment = array(
                    'date' => $date,
                    'reference_no' => $pay_ref,
                    'amount' => (0-$this->input->post('amount-paid')),
                    'paid_by' => $this->input->post('paid_by'),
                    'cheque_no' => $this->input->post('cheque_no'),
                    'cc_no' => $this->input->post('pcc_no'),
                    'cc_holder' => $this->input->post('pcc_holder'),
                    'cc_month' => $this->input->post('pcc_month'),
                    'cc_year' => $this->input->post('pcc_year'),
                    'cc_type' => $this->input->post('pcc_type'),
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $data['payment_status'] = $grand_total == $this->input->post('amount-paid') ? 'paid' : 'partial';
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $si_return, $payment);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->addSale($data, $products, $payment, $si_return)) {
            $this->session->set_flashdata('message', lang("return_sale_added"));
            admin_redirect($sale->pos ? "pos/sales" : "sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $sale;
            if ($this->data['inv']->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang("sale_status_x_competed"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-'.$this->Settings->disable_editing.' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->consignment_model->getAllInvoiceItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    $row->quantity = 0;
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->sale_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->base_quantity = $item->quantity;
                $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = $row->price ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->consignment_model->getProductOptions($row->id, $item->warehouse_id, true);
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'units' => $units, 'tax_rate' => $tax_rate, 'options' => $options);
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['payment_ref'] = '';
            $this->data['reference'] = ''; // $this->site->getReference('re');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('return_sale')));
            $meta = array('page_title' => lang('return_sale'), 'bc' => $bc);
            $this->page_construct('sales/return_sale', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $temp = $this->consignment_model->api_consignment_delete($id);
        if ($temp['error'] != '') {
            $this->session->set_flashdata('error', $temp['error']);
            redirect($_SERVER["HTTP_REFERER"]);                        
        }
        else {
            $this->session->set_flashdata('message', lang('consignment_deleted'));
            redirect($_SERVER["HTTP_REFERER"]);            
        }
    }

    public function delete_return($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->consignment_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("return_sale_deleted")));
            }
            $this->session->set_flashdata('message', lang('return_sale_deleted'));
            admin_redirect('welcome');
        }
    }

    public function sale_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('api_action', lang("api_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['check_value'])) {
                $temp_val = explode('-',$_POST['check_value']);
                if ($this->input->post('api_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_consignment_delete($id);
                            if ($temp['error'] == '')
                                $b = 1;
                            else
                                $temp_error = $temp['error'];
                        }
                    }
                    if ($b == 1)
                        $this->session->set_flashdata('message', lang("consignment_deleted"));
                    else
                        $this->session->set_flashdata('error', $temp_error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                elseif ($this->input->post('api_action') == 'combine') {
                    $html = $this->combine_pdf($temp_val);
                }
                elseif ($this->input->post('api_action') == 'set_delivering') {
                    $temp = '?sales=';
                    foreach ($temp_val as $id) {
                        if ($id != '')
                            $temp .= $id.'-';
                    }                                           
                    admin_redirect("consignment/set_delivering_bulk".$temp);          
                }                      
                elseif ($this->input->post('api_action') == 'print_combine_pdf') {
                    $j = 0;                    
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $sale = $this->consignment_model->getInvoiceConsignmentByID($id);
                            if ($sale->reference_no == '')
                                $j = 1;
                        }
                    }             
                           
                    if ($j == 1) {                       
                        $this->session->set_flashdata('error', lang("Error"));
                        admin_redirect("consignment");                                                        
                    }
                    else {
                        foreach ($temp_val as $id) {
                            if ($id != '') {
                                $sale = $this->consignment_model->getInvoiceConsignmentByID($id);
                                if ($sale->reference_no != '') {
                                    if ($sale->sale_status == 'print needed' || $sale->sale_status == 'pending' || $sale->sale_status == '') {
                                        $update_array = array(
                                            'sale_status' => 'preparing',                 
                                        );
                                        $this->site->api_update_table('sma_consignment',$update_array,"id = ".$id);
                                    }                       
                                }
                            }
                        }

                        $html = $this->combine_pdf($temp_val);
                    }
                  
                }
                elseif ($this->input->post('api_action') == 'change_status_to_print_needed') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $temp2 = array(
                                'sale_status' => 'pending'
                            );
                            $this->db->update('sma_consignment', $temp2,"id = ".$id);
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }              
                elseif ($this->input->post('api_action') == 'change_status_to_preparing') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $temp2 = array(
                                'sale_status' => 'preparing'
                            );
                            $this->db->update('sma_consignment', $temp2,"id = ".$id);
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }    
                elseif ($this->input->post('api_action') == 'change_status_to_delivering') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $temp2 = array(
                                'sale_status' => 'delivering'
                            );
                            $this->db->update('sma_consignment', $temp2,"id = ".$id);
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }              
                elseif ($this->input->post('api_action') == 'change_status_to_delivered') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $temp2 = array(
                                'sale_status' => 'delivered'
                            );
                            $this->db->update('sma_consignment', $temp2,"id = ".$id);
                        }
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }     
                elseif ($this->input->post('api_action') == 'change_status_to_completed') {
                    $b = 0;
                    $b2 = 0;
                    $b3 = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $config_data = array(
                                'id' => $id,
                                'reference_no' => $temp[0]['reference_no'],
                            );
                            $api_get_consignment_qty = $this->consignment_model->api_get_consignment_qty($config_data);

                            if ($temp[0]['sale_status'] == 'pending' || $temp[0]['sale_status'] != 'preparing')
                                $b = 1;
                            if ($api_get_consignment_qty['available_qty'] == 0) {
                                $temp2 = array(
                                    'sale_status' => 'completed'
                                );
                                $this->db->update('sma_consignment', $temp2,"id = ".$id);
                                $b3 = 1;
                            }
                            else
                                $b2 = 1;

                        }
                    }
                    if ($b3 == 1)
                        $this->session->set_flashdata('message', lang('successfully_change_status'));
                    else if ($b == 1 || $b2 == 1) {
                        if ($b == 1)
                            $temp = lang("Only_consignment_status_is_not_Print_Needed_and_Preparing_are_effected.");
                        if ($b2 == 1)
                            $temp .= '<br>'.lang("Only_consignment_that_sold_all_items_are_effected.");
                        $this->session->set_flashdata('error', $temp);
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }                                                           
                elseif ($this->input->post('api_action') == 'change_delivery_status_to_pending') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            $config_data = array(
                                'id' => $id,
                                'reference_no' => $temp[0]['reference_no'],
                            );
                            $api_get_consignment_qty = $this->consignment_model->api_get_consignment_qty($config_data);

                            if ($temp[0]['sale_status'] != 'delivering' && $temp[0]['sale_status'] != 'completed') {
                                $b = 1;
                            }
                            else {
                                $config_data_2 = array(
                                    'table_name' => 'sma_consignment',
                                    'id_name' => 'id',
                                    'field_add_ons_name' => 'add_ons',
                                    'selected_id' => $id,
                                    'add_ons_title' => 'delivery_status',
                                    'add_ons_value' => 'completed',
                                );
                                $this->site->api_update_add_ons_field($config_data_2);

                                if ($api_get_consignment_qty['available_qty'] == 0) {
                                    $temp2 = array(
                                        'sale_status' => 'completed'
                                    );
                                    $this->db->update('sma_consignment', $temp2,"id = ".$id);
                                }

                            }
                        }
                    }
                    if ($b == 1)
                        $this->session->set_flashdata('error', lang("Only_consignment_with_status_delivering_or_completed_are_effected"));
                    else
                        $this->session->set_flashdata('message', lang('successfully_change_delivering_status'));

                    redirect($_SERVER["HTTP_REFERER"]);
                }
                elseif ($this->input->post('api_action') == 'change_delivery_status_to_completed') {
                    $b = 0;
                    foreach ($temp_val as $id) {
                        if ($id != '') {
                            $temp = $this->consignment_model->api_select_data($id);
                            if ($temp[0]['sale_status'] != 'delivering' && $temp[0]['sale_status'] != 'completed') {
                                $b = 1;
                            }
                            else {
                                $config_data_2 = array(
                                    'table_name' => 'sma_consignment',
                                    'id_name' => 'id',
                                    'field_add_ons_name' => 'add_ons',
                                    'selected_id' => $id,
                                    'add_ons_title' => 'delivery_status',
                                    'add_ons_value' => 'completed',
                                );
                                $this->site->api_update_add_ons_field($config_data_2);

                                $temp2 = array(
                                    'sale_status' => 'completed'
                                );
                                $this->db->update('sma_consignment', $temp2,"id = ".$id);

                            }
                        }
                    }
                    if ($b == 1)
                        $this->session->set_flashdata('error', lang("Only_consignment_with_status_delivering_or_completed_are_effected"));
                    else
                        $this->session->set_flashdata('message', lang('successfully_change_delivering_status'));

                    redirect($_SERVER["HTTP_REFERER"]);
                }                
                elseif ($this->input->post('form_action') == 'change_payment_status_to_due') {
                    foreach ($_POST['val'] as $id) {
                        $this->consignment_model->update_mpayment_status_to_due($id);
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                elseif($this->input->post('form_action') == 'change_payment_status_to_partial'){
                    foreach ($_POST['val'] as $id) {
                        $this->consignment_model->update_mpayment_status_to_partial($id);
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                elseif ($this->input->post('form_action') == 'change_payment_status_to_paid') {
                    foreach ($_POST['val'] as $id) {
                        $this->consignment_model->update_mpayment_status_to_paid($id);
                    }
                    $this->session->set_flashdata('message', lang('successfully_change_payment_status'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }                
                elseif ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('consignment'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('payment_status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->consignment_model->getInvoiceByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sale->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($sale->paid));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, lang($sale->payment_status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'Consignment ' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } 
            else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* ------------------------------- */

    public function deliveries()
    {
        $this->sma->checkPermissions();

        $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('deliveries')));
        $meta = array('page_title' => lang('deliveries'), 'bc' => $bc);
        $this->page_construct('sales/deliveries', $meta, $this->data);

    }

    public function getDeliveries()
    {
        $this->sma->checkPermissions('deliveries');

        $detail_link = anchor('admin/sales/view_delivery/$1', '<i class="fa fa-file-text-o"></i> ' . lang('delivery_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/sales/email_delivery/$1', '<i class="fa fa-envelope"></i> ' . lang('email_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit_delivery/$1', '<i class="fa fa-edit"></i> ' . lang('edit_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $pdf_link = anchor('admin/sales/pdf_delivery/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_delivery") . "</b>' data-content=\"<p>"
        . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete_delivery/$1') . "'>"
        . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
        . lang('delete_delivery') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>' . $detail_link . '</li>
        <li>' . $edit_link . '</li>
        <li>' . $pdf_link . '</li>
        <li>' . $delete_link . '</li>
    </ul>
</div></div>';

        $this->load->library('datatables');
        //GROUP_CONCAT(CONCAT('Name: ', sale_items.product_name, ' Qty: ', sale_items.quantity ) SEPARATOR '<br>')
        $this->datatables
            ->select("deliveries.id as id, date, do_reference_no, sale_reference_no, customer, address, status, attachment")
            ->from('deliveries')
            ->join('sale_items', 'sale_items.sale_id=deliveries.sale_id', 'left')
            ->group_by('deliveries.id');
        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf_delivery($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->consignment_model->getDeliveryByID($id);

        $this->data['delivery'] = $deli;
        $sale = $this->consignment_model->getInvoiceByID($deli->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);

        $name = lang("delivery") . "_" . str_replace('/', '_', $deli->do_reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf_delivery', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }
        if ($view) {
            $this->load->view($this->theme . 'sales/pdf_delivery', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function view_delivery($id = null)
    {
        $this->sma->checkPermissions('deliveries');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->consignment_model->getDeliveryByID($id);
        $sale = $this->consignment_model->getInvoiceByID($deli->sale_id);
        if (!$sale) {
            $this->session->set_flashdata('error', lang('sale_not_found'));
            $this->sma->md();
        }
        $this->data['delivery'] = $deli;
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);
        $this->data['page_title'] = lang("delivery_order");

        $this->load->view($this->theme . 'sales/view_delivery', $this->data);
    }

    public function add_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->consignment_model->getInvoiceByID($id);
        if ($sale->sale_status != 'completed') {
            $this->session->set_flashdata('error', lang('status_is_x_completed'));
            $this->sma->md();
        }

        if ($delivery = $this->consignment_model->getDeliveryBySaleID($id)) {
            $this->edit_delivery($delivery->id);
        } else {

            $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
            $this->form_validation->set_rules('customer', lang("customer"), 'required');
            $this->form_validation->set_rules('address', lang("address"), 'required');

            if ($this->form_validation->run() == true) {
                if ($this->Owner || $this->Admin) {
                    $date = $this->sma->fld(trim($this->input->post('date')));
                } else {
                    $date = date('Y-m-d H:i:s');
                }
                $dlDetails = array(
                    'date' => $date,
                    'sale_id' => $this->input->post('sale_id'),
                    'do_reference_no' => $this->input->post('do_reference_no') ? $this->input->post('do_reference_no') : $this->site->getReference('do'),
                    'sale_reference_no' => $this->input->post('sale_reference_no'),
                    'customer' => $this->input->post('customer'),
                    'address' => $this->input->post('address'),
                    'status' => $this->input->post('status'),
                    'delivered_by' => $this->input->post('delivered_by'),
                    'received_by' => $this->input->post('received_by'),
                    'note' => $this->sma->clear_tags($this->input->post('note')),
                    'created_by' => $this->session->userdata('user_id'),
                    );
                if ($_FILES['document']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = false;
                    $config['encrypt_name'] = true;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('document')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $photo = $this->upload->file_name;
                    $dlDetails['attachment'] = $photo;
                }
            } elseif ($this->input->post('add_delivery')) {
                if ($sale->shop) {
                    $this->load->library('sms');
                    $this->sms->delivering($sale->id, $dlDetails['do_reference_no']);
                }
                $this->session->set_flashdata('error', validation_errors());
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->form_validation->run() == true && $this->consignment_model->addDelivery($dlDetails)) {
                $this->session->set_flashdata('message', lang("delivery_added"));
                admin_redirect("sales/deliveries");
            } else {

                $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
                $this->data['address'] = $this->site->getAddressByID($sale->address_id);
                $this->data['inv'] = $sale;
                $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
                $this->data['modal_js'] = $this->site->modal_js();

                $this->load->view($this->theme . 'sales/add_delivery', $this->data);
            }
        }
    }

    public function edit_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');

        if ($this->form_validation->run() == true) {

            $dlDetails = array(
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'status' => $this->input->post('status'),
                'delivered_by' => $this->input->post('delivered_by'),
                'received_by' => $this->input->post('received_by'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $dlDetails['attachment'] = $photo;
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $dlDetails['date'] = $date;
            }
        } elseif ($this->input->post('edit_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->updateDelivery($id, $dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_updated"));
            admin_redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['delivery'] = $this->consignment_model->getDeliveryByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_delivery', $this->data);
        }
    }

    public function delete_delivery($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->consignment_model->deleteDelivery($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("delivery_deleted")));
        }

    }

    public function delivery_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_delivery');
                    foreach ($_POST['val'] as $id) {
                        $this->consignment_model->deleteDelivery($id);
                    }
                    $this->session->set_flashdata('message', lang("deliveries_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deliveries'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('do_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $delivery = $this->consignment_model->getDeliveryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($delivery->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $delivery->do_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $delivery->sale_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $delivery->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $delivery->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($delivery->status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);

                    $filename = 'deliveries_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_delivery_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* -------------------------------------------------------------------------------- */

    public function payments($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['payments'] = $this->consignment_model->getInvoicePayments($id);
        $this->data['inv'] = $this->consignment_model->getInvoiceByID($id);
        $this->load->view($this->theme . 'sales/payments', $this->data);
    }

    public function payment_note($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->consignment_model->getPaymentByID($id);
        $inv = $this->consignment_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['page_title'] = lang("payment_note");

        $this->load->view($this->theme . 'sales/payment_note', $this->data);
    }

    public function email_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->consignment_model->getPaymentByID($id);
        $inv = $this->consignment_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        if ( ! $customer->email) {
            $this->sma->send_json(array('msg' => lang("update_customer_email")));
        }
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['customer'] =$customer;
        $this->data['page_title'] = lang("payment_note");
        $html = $this->load->view($this->theme . 'sales/payment_note', $this->data, TRUE);

        $html = str_replace(array('<i class="fa fa-2x">&times;</i>', 'modal-', '<p>&nbsp;</p>', '<p style="border-bottom: 1px solid #666;">&nbsp;</p>', '<p>'.lang("stamp_sign").'</p>'), '', $html);
        $html = preg_replace("/<img[^>]+\>/i", '', $html);
        // $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;">'.$html.'</div>';

        $this->load->library('parser');
        $parse_data = array(
            'stylesheet' => '<link href="'.$this->data['assets'].'styles/helpers/bootstrap.min.css" rel="stylesheet"/>',
            'name' => $customer->company && $customer->company != '-' ? $customer->company :  $customer->name,
            'email' => $customer->email,
            'heading' => lang('payment_note').'<hr>',
            'msg' => $html,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_con.html');
        $message = $this->parser->parse_string($msg, $parse_data);
        $subject = lang('payment_note') . ' - ' . $this->Settings->site_name;

        if ($this->sma->send_email($customer->email, $subject, $message)) {
            $this->sma->send_json(array('msg' => lang("email_sent")));
        } else {
            $this->sma->send_json(array('msg' => lang("email_failed")));
        }
    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->consignment_model->getInvoiceByID($id);
        if ($sale->payment_status == 'paid' && $sale->grand_total == $sale->paid) {
            $this->session->set_flashdata('error', lang("sale_already_paid"));
            $this->sma->md();
        }

        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            $sale = $this->consignment_model->getInvoiceByID($this->input->post('sale_id'));
            if ($this->input->post('paid_by') == 'deposit') {
                $customer_id = $sale->customer_id;
                if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
                'type' => $sale->sale_status == 'returned' ? 'returned' : 'received',
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->addPayment($payment, $customer_id)) {
            if ($sale->shop) {
                $this->load->library('sms');
                $this->sms->paymentReceived($sale->id, $payment['reference_no'], $payment['amount']);
            }
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($sale->sale_status == 'returned' && $sale->paid == $sale->grand_total) {
                $this->session->set_flashdata('warning', lang('payment_was_returned'));
                $this->sma->md();
            }
            $this->data['inv'] = $sale;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/add_payment', $this->data);
        }
    }

    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $payment = $this->consignment_model->getPaymentByID($id);
        if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe' || $payment->paid_by == 'paypal' || $payment->paid_by == 'skrill') {
            $this->session->set_flashdata('error', lang('x_edit_payment'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->input->post('paid_by') == 'deposit') {
                $sale = $this->consignment_model->getInvoiceByID($this->input->post('sale_id'));
                $customer_id = $sale->customer_id;
                $amount = $this->input->post('amount-paid')-$payment->amount;
                if ( ! $this->site->check_customer_deposit($customer_id, $amount)) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->updatePayment($id, $payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            admin_redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_payment', $this->data);
        }
    }

    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->consignment_model->deletePayment($id)) {
            //echo lang("payment_deleted");
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $customer_id = $this->input->get('customer_id', true);
        $mode = $this->input->get('mode', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $customer = $this->site->getCompanyByID_v2($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows = $this->sales_model->getProductNames($sr, $warehouse_id);
       
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);

                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                }

                $row->option = $option_id;
/*                
                $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                if ($pis) {
                    $row->quantity = 0;
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                if ($options) {
                    $option_quantity = 0;
                    foreach ($options as $option) {
                        $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $option_quantity += $pi->quantity_balance;
                            }
                        }
                        if ($option->quantity > $option_quantity) {
                            $option->quantity = $option_quantity;
                        }
                    }
                }
*/


                $config_data = array(
                    'id' => $row->id,
                    'customer_id' => $customer_id,
                    'option_id' => $option_id
                );
                $temp = $this->site->api_calculate_product_price($config_data);
        

                $row->price = $temp['price'];

                if ($mode == 'sample')
                    $row->price = 0;


                $price = $row->price;
                                
                $row->price = $this->sma->formatDecimal($price, 2);
                /*** END ***/ 
                
                  
                $row->discount_product = $customer->discount_product;
                              
                $row->real_unit_price = $row->price;
                $row->base_quantity = 1;
                $row->base_unit = $row->unit;
                $row->base_unit_price = $temp['temp_price'];
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);

                $pr[] = array('id' => sha1($c.$r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'category' => $row->category_id,
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    /* ------------------------------------ Gift Cards ---------------------------------- */

    public function gift_cards()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('gift_cards')));
        $meta = array('page_title' => lang('gift_cards'), 'bc' => $bc);
        $this->page_construct('sales/gift_cards', $meta, $this->data);
    }

    public function getGiftCards()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gift_cards') . ".id as id, card_no, value, balance, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name) as created_by, customer, expiry", false)
            ->join('users', 'users.id=gift_cards.created_by', 'left')
            ->from("gift_cards")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('sales/view_gift_card/$1') . "' class='tip' title='" . lang("view_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a> <a href='" . admin_url('sales/topup_gift_card/$1') . "' class='tip' title='" . lang("topup_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-dollar\"></i></a> <a href='" . admin_url('sales/edit_gift_card/$1') . "' class='tip' title='" . lang("edit_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_gift_card") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete_gift_card/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function view_gift_card($id = null)
    {
        $this->data['page_title'] = lang('gift_card');
        $gift_card = $this->site->getGiftCardByID($id);
        $this->data['gift_card'] = $this->site->getGiftCardByID($id);
        $this->data['customer'] = $this->site->getCompanyByID($gift_card->customer_id);
        $this->data['topups'] = $this->consignment_model->getAllGCTopups($id);
        $this->load->view($this->theme . 'sales/view_gift_card', $this->data);
    }

    public function topup_gift_card($card_id)
    {
        $this->sma->checkPermissions('add_gift_card', true);
        $card = $this->site->getGiftCardByID($card_id);
        $this->form_validation->set_rules('amount', lang("amount"), 'trim|integer|required');

        if ($this->form_validation->run() == true) {
            $data = array('card_id' => $card_id,
                'amount' => $this->input->post('amount'),
                'date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('user_id'),
            );
            $card_data['balance'] = ($this->input->post('amount')+$card->balance);
            // $card_data['value'] = ($this->input->post('amount')+$card->value);
            if ($this->input->post('expiry')) {
                $card_data['expiry'] = $this->sma->fld(trim($this->input->post('expiry')));
            }
        } elseif ($this->input->post('topup')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->consignment_model->topupGiftCard($data, $card_data)) {
            $this->session->set_flashdata('message', lang("topup_added"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['card'] = $card;
            $this->data['page_title'] = lang("topup_gift_card");
            $this->load->view($this->theme . 'sales/topup_gift_card', $this->data);
        }
    }

    public function validate_gift_card($no)
    {
        //$this->sma->checkPermissions();
        if ($gc = $this->site->getGiftCardByNO($no)) {
            if ($gc->expiry) {
                if ($gc->expiry >= date('Y-m-d')) {
                    $this->sma->send_json($gc);
                } else {
                    $this->sma->send_json(false);
                }
            } else {
                $this->sma->send_json($gc);
            }
        } else {
            $this->sma->send_json(false);
        }
    }

    public function add_gift_card()
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|is_unique[gift_cards.card_no]|required');
        $this->form_validation->set_rules('value', lang("value"), 'required');

        if ($this->form_validation->run() == true) {
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => $this->input->post('value'),
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
                'created_by' => $this->session->userdata('user_id'),
            );
            $sa_data = array();
            $ca_data = array();
            if ($this->input->post('staff_points')) {
                $sa_points = $this->input->post('sa_points');
                $user = $this->site->getUser($this->input->post('user'));
                if ($user->award_points < $sa_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    admin_redirect("sales/gift_cards");
                }
                $sa_data = array('user' => $user->id, 'points' => ($user->award_points - $sa_points));
            } elseif ($customer_details && $this->input->post('use_points')) {
                $ca_points = $this->input->post('ca_points');
                if ($customer_details->award_points < $ca_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    admin_redirect("sales/gift_cards");
                }
                $ca_data = array('customer' => $this->input->post('customer'), 'points' => ($customer_details->award_points - $ca_points));
            }
            // $this->sma->print_arrays($data, $ca_data, $sa_data);
        } elseif ($this->input->post('add_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->consignment_model->addGiftCard($data, $ca_data, $sa_data)) {
            $this->session->set_flashdata('message', lang("gift_card_added"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->consignment_model->getStaff();
            $this->data['page_title'] = lang("new_gift_card");
            $this->load->view($this->theme . 'sales/add_gift_card', $this->data);
        }
    }

    public function edit_gift_card($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        $gc_details = $this->site->getGiftCardByID($id);
        if ($this->input->post('card_no') != $gc_details->card_no) {
            $this->form_validation->set_rules('card_no', lang("card_no"), 'is_unique[gift_cards.card_no]');
        }
        $this->form_validation->set_rules('value', lang("value"), 'required');
        //$this->form_validation->set_rules('customer', lang("customer"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $gift_card = $this->site->getGiftCardByID($id);
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => ($this->input->post('value') - $gift_card->value) + $gift_card->balance,
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
            );
        } elseif ($this->input->post('edit_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->consignment_model->updateGiftCard($id, $data)) {
            $this->session->set_flashdata('message', lang("gift_card_updated"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['gift_card'] = $this->site->getGiftCardByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_gift_card', $this->data);
        }
    }

    public function sell_gift_card()
    {
        $this->sma->checkPermissions('gift_cards', true);
        $error = null;
        $gcData = $this->input->get('gcdata');
        if (empty($gcData[0])) {
            $error = lang("value") . " " . lang("is_required");
        }
        if (empty($gcData[1])) {
            $error = lang("card_no") . " " . lang("is_required");
        }

        $customer_details = (!empty($gcData[2])) ? $this->site->getCompanyByID($gcData[2]) : null;
        $customer = $customer_details ? $customer_details->company : null;
        $data = array('card_no' => $gcData[0],
            'value' => $gcData[1],
            'customer_id' => (!empty($gcData[2])) ? $gcData[2] : null,
            'customer' => $customer,
            'balance' => $gcData[1],
            'expiry' => (!empty($gcData[3])) ? $this->sma->fsd($gcData[3]) : null,
            'created_by' => $this->session->userdata('user_id'),
        );

        if (!$error) {
            if ($this->consignment_model->addGiftCard($data)) {
                $this->sma->send_json(array('result' => 'success', 'message' => lang("gift_card_added")));
            }
        } else {
            $this->sma->send_json(array('result' => 'failed', 'message' => $error));
        }

    }

    public function delete_gift_card($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->consignment_model->deleteGiftCard($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("gift_card_deleted")));
        }
    }

    public function gift_card_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete_gift_card');
                    foreach ($_POST['val'] as $id) {
                        $this->consignment_model->deleteGiftCard($id);
                    }
                    $this->session->set_flashdata('message', lang("gift_cards_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('gift_cards'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('card_no'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('value'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->site->getGiftCardByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->card_no);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->value);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->customer);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'gift_cards_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_gift_card_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function get_award_points($id = null)
    {
        $this->sma->checkPermissions('index');

        $row = $this->site->getUser($id);
        $this->sma->send_json(array('sa_points' => $row->award_points));
    }

    /* -------------------------------------------------------------------------------------- */

    public function sale_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {

            //$reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');
            $customer_id = $this->input->post('customer');//18-01-2019
            $customers = $this->site->getCompanyByID($customer_id);//18-01-2019
            $is_vat = $customers->vat_no;
            $reference = $this->input->post('reference_no') ? 
                                $this->input->post('reference_no') : 
                                $this->site->getSaleReference($is_vat); 
                         
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("sales/sale_by_csv");
                }
                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'net_unit_price', 'quantity', 'variant', 'item_tax_rate', 'discount', 'serial');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {

                    if (isset($csv_pr['code']) && isset($csv_pr['net_unit_price']) && isset($csv_pr['quantity'])) {

                        if ($product_details = $this->consignment_model->getProductByCode($csv_pr['code'])) {

                            if ($csv_pr['variant']) {
                                $item_option = $this->consignment_model->getProductVariantByName($csv_pr['variant'], $product_details->id);
                                if (!$item_option) {
                                    $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $product_details->name . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $item_option = json_decode('{}');
                                $item_option->id = null;
                            }

                            $item_id = $product_details->id;
                            $item_type = $product_details->type;
                            $item_code = $product_details->code;
                            $item_name = $product_details->name;
                            $item_net_price = $this->sma->formatDecimal($csv_pr['net_unit_price']);
                            $item_quantity = $csv_pr['quantity'];
                            $item_tax_rate = $csv_pr['item_tax_rate'];
                            $item_discount = $csv_pr['discount'];
                            $item_serial = $csv_pr['serial'];

                            if (isset($item_code) && isset($item_net_price) && isset($item_quantity)) {
                                $product_details = $this->consignment_model->getProductByCode($item_code);
                                $pr_discount = $this->site->calculateDiscount($item_discount, $item_net_price);
                                $item_net_price = $this->sma->formatDecimal(($item_net_price - $pr_discount), 4);
                                $pr_item_discount = $this->sma->formatDecimal(($pr_discount * $item_quantity), 4);
                                $product_discount += $pr_item_discount;

                                $tax = "";
                                $pr_item_tax = 0;
                                $unit_price = $item_net_price;
                                $tax_details = ((isset($item_tax_rate) && !empty($item_tax_rate)) ? $this->consignment_model->getTaxRateByName($item_tax_rate) : $this->site->getTaxRateByID($product_details->tax_rate));
                                if ($tax_details) {
                                    $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                                    $item_tax = $ctax['amount'];
                                    $tax = $ctax['tax'];
                                    if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                                        $item_net_price = $unit_price - $item_tax;
                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity, 4);
                                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                                        $total_cgst += $gst_data['cgst'];
                                        $total_sgst += $gst_data['sgst'];
                                        $total_igst += $gst_data['igst'];
                                    }
                                }

                                $product_tax += $pr_item_tax;
                                $subtotal = $this->sma->formatDecimal((($item_net_price * $item_quantity) + $pr_item_tax), 4);
                                $unit = $this->site->getUnitByID($product_details->unit);

                                $product = array(
                                    'product_id' => $product_details->id,
                                    'product_code' => $item_code,
                                    'product_name' => $item_name,
                                    'product_type' => $item_type,
                                    'option_id' => $item_option->id,
                                    'net_unit_price' => $item_net_price,
                                    'quantity' => $item_quantity,
                                    'product_unit_id' => $product_details->unit,
                                    'product_unit_code' => $unit->code,
                                    'unit_quantity' => $item_quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $tax_details ? $tax_details->id : null,
                                    'tax' => $tax,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'subtotal' => $subtotal,
                                    'serial_no' => $item_serial,
                                    'unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax), 4),
                                    'real_unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax + $pr_discount), 4),
                                );

                                $products[] = ($product+$gst_data);
                                $total += $this->sma->formatDecimal(($item_net_price * $item_quantity), 4);
                            }

                        } else {
                            $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . lang("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $rw++;
                    }

                }
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount), 4);
            $data = array(
                'is_vat' => $is_vat,
                'date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($payment_status == 'paid') {

                $payment = array(
                    'date' => $date,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $grand_total,
                    'paid_by' => 'cash',
                    'cheque_no' => '',
                    'cc_no' => '',
                    'cc_holder' => '',
                    'cc_month' => '',
                    'cc_year' => '',
                    'cc_type' => '',
                    'created_by' => $this->session->userdata('user_id'),
                    'note' => lang('auto_added_for_sale_by_csv') . ' (' . lang('sale_reference_no') . ' ' . $reference . ')',
                    'type' => 'received',
                );

            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $this->consignment_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_added"));
            admin_redirect("sales");
        } else {
            
            $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            //$this->data['slnumber'] = $this->site->getReference('so');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale_by_csv')));
            $meta = array('page_title' => lang('add_sale_by_csv'), 'bc' => $bc);
            $this->page_construct('sales/sale_by_csv', $meta, $this->data);

        }
    }

    public function update_status($id)
    {

        $this->form_validation->set_rules('status', lang("sale_status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        }

        if ($this->form_validation->run() == true && $this->consignment_model->updateStatus($id, $status, $note)) {
            $this->session->set_flashdata('message', lang('status_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        } else {

            $this->data['inv'] = $this->consignment_model->getInvoiceByID($id);
            $this->data['returned'] = FALSE;
            if ($this->data['inv']->sale_status == 'returned' || $this->data['inv']->return_id) {
                $this->data['returned'] = TRUE;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme.'sales/update_status', $this->data);

        }
    }

    public function packaging($id)
    {

            $sale = $this->consignment_model->getInvoiceByID($id);
            $this->data['returned'] = FALSE;
            if ($sale->sale_status == 'returned' || $sale->return_id) {
                $this->data['returned'] = TRUE;
            }
            $this->data['warehouse'] = $this->site->getWarehouseByID($sale->warehouse_id);
            $items = $this->consignment_model->getAllInvoiceItems($sale->id);
            foreach ($items as $item) {
                $packaging[] = array(
                    'name' => $item->product_code.' - '.$item->product_name,
                    'quantity' => $item->quantity.' '.$item->product_unit_code,
                    'rack' => $this->consignment_model->getItemRack($item->product_id, $sale->warehouse_id),
                    );
            }
            $this->data['packaging'] = $packaging;
            $this->data['sale'] = $sale;

            $this->load->view($this->theme.'sales/packaging', $this->data);

    }

	/*** Bulk Actions On Sale ***/
	public function getPaymentCate($id) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id', TRUE);
        }
        $rows['results'] = $this->consignment_model->getInvoiceByIDPayCate($id);
        echo json_encode($rows);
    }
    
    public function add_sale_consignment($cs_reference_no = null)
    {
        if ($this->Owner || $this->Admin) {
            $this->sma->checkPermissions();
            $this->form_validation->set_rules('date', lang("date"), 'required');
            $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');


            if ($this->form_validation->run() == true) {
                $temp_qty = 0;
                foreach($_POST as $name => $value) {
                    if ($name != 'date')
                        ${$name} = $this->input->post($name);
                    else
                        ${$name} = $this->sma->fld(trim($this->input->post('date')));
                    if ($name != 'date' && $name != 'reference_no' && $name != 'add_payment' && $name != 'payment_status')
                        $temp_qty = $temp_qty + ${$name};
                    
                }

                $config_data = array(
                    'table_name' => 'sma_consignment',
                    'select_table' => 'sma_consignment',
                    'translate' => '',
                    'select_condition' => "reference_no = '".$reference_no."'",
                );
                $temp_data = $this->site->api_select_data_v2($config_data);

                $temp_id = $temp_data[0]['id'];
                if ($temp_id != '') {
                    if ($temp_qty > 0) {      

                        if ($_GET['return'] == '') {
                            $temp = date_create($date);


                            $temp2 = $this->site->api_auto_id('sma_sales','id');
                            
                            $customers = $this->site->getCompanyByID($temp_data[0]['customer_id']);
                            $is_vat = $customers->vat_no;                                                
                            $temp_ref = '';
                            //$up_sale_ref = $this->site->getSaleReference($is_vat, true);
                            //$this->site->updateReference($up_sale_ref);
                            if ($customers->vat_no) $temp_order_tax = 2; else $temp_order_tax = 1;

                            if ($date == '')
                                $date = date('Y-m-d H:i:s');
                            else
                                $date = $this->sma->fld(trim($this->input->post('date').':'.date('s')));
                            
                            $config_data = array(
                                'duplicate_table' => 'sma_consignment',
                                'into_table' => 'sma_sales',
                                'duplicate_id_col_name' => 'id',
                                'condition' => "reference_no = '".$reference_no."'",
                                'update_1' => "date = '".$date."'",
                                'update_2' => "payment_status = '".$payment_status."'",
                                'update_3' => "sale_status = 'pending'",
                                'update_4' => "reference_no = ''",
                                'update_5' => "add_ons = 'initial_first_add_ons:{}:cs_reference_no:{".$reference_no."}:'",                            
                                'update_6' => "order_discount = 0",
                                'update_7' => "shipping = 0",
                                'update_8' => "order_tax_id = ".$temp_order_tax,
                                'update_9' => "payment_status = 'pending'",
                                'count_update' => 9,
                            );
                            $this->site->api_duplicate_record_update($config_data);

                            
                            $temp_max = $this->site->api_select_some_fields_with_where("MAX(id) as max","sma_sales","add_ons like '%:cs_reference_no:{".$reference_no."}:%' order by id desc limit 1","arr");
                                                                                      
                            $temp_sale_items = $this->site->api_select_some_fields_with_where("*
                                "
                                ,"sma_consignment_items"
                                ,"sale_id = '".$temp_id."'"
                                ,"arr"
                            );
                            for ($i=0;$i<count($temp_sale_items);$i++) {
                                if (${'consignment_qty_'.$temp_sale_items[$i]['id']} > 0) {
                                    $temp_subtotal = ${'consignment_qty_'.$temp_sale_items[$i]['id']} * $temp_sale_items[$i]['unit_price'];
                                    $config_data = array(
                                        'duplicate_table' => 'sma_consignment_items',
                                        'into_table' => 'sma_sale_items',
                                        'duplicate_id_col_name' => 'id',
                                        'condition' => "id = ".$temp_sale_items[$i]['id'],
                                        'update_1' => "sale_id = ".$temp_max[0]['max'],
                                        'update_2' => "quantity = ".${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                        'update_3' => "unit_quantity = ".${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                        'update_4' => "subtotal = ".$temp_subtotal,
                                        'count_update' => 4,
                                    );                        
                                    $this->site->api_duplicate_record_update($config_data);

                                    $temp_2 = array(
                                        'consignment_id' => $temp_data[0]['id'],
                                        'sale_id' => $temp_max[0]['max'],
                                        'product_id' => $temp_sale_items[$i]['product_id'],
                                        'option_id' => $temp_sale_items[$i]['option_id'],
                                        'quantity' => ${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                    );
                                    $this->db->insert('sma_consignment_sale_items', $temp_2);                                    
                                }
                            }
                            $temp = $this->site->api_select_some_fields_with_where("subtotal
                                "
                                ,"sma_sale_items"
                                ,"sale_id = '".$temp_max[0]['max']."'"
                                ,"arr"
                            );
                            $temp_grand_total = 0;
                            $temp_total = 0;
                            for ($i=0;$i<count($temp);$i++) {
                                $temp_total = $temp_total + $temp[$i]['subtotal'];
                            }
                            $temp_total = $this->sma->formatDecimal($temp_total, 4);
                                                    
                            $order_tax = 0;
                            $total_tax = 0;
                            $temp_grand_total = $this->sma->formatDecimal($temp_total, 4);
                            if ($customers->vat_no) {
                                $order_tax = $this->site->calculateOrderTax($temp_order_tax, $temp_total);
                                $temp_grand_total = $this->sma->formatDecimal(($temp_total + $order_tax), 4);
                                $order_tax = $this->site->calculateOrderTax($temp_order_tax, $temp_total);
                                $total_tax = $this->sma->formatDecimal($order_tax, 4);
                            }                         
                            
                            $this->site->api_update_table('sma_sales',array('total' => $temp_total, 'grand_total' => $temp_grand_total, 'order_tax' => $order_tax, 'total_tax' => $total_tax),"id = ".$temp_max[0]['max']);

                            //$this->site->api_update_sale_consignment_status($temp_data[0]['id']);

                            $this->session->set_flashdata('message', lang("consignment_added"));
                            admin_redirect("consignment");        
                        }
                        else {

                            $temp = date_create($date);
                            $temp2 = $this->site->api_auto_id('sma_returns','id');
                            
                            $customers = $this->site->getCompanyByID($temp_data[0]['customer_id']);
                            $is_vat = $customers->vat_no;                                                

                            if ($customers->vat_no) $temp_order_tax = 2; else $temp_order_tax = 1;
                            $temp_reference_no = $this->site->getReference('re');
                            $temp = array(
                                'reference_no' => $temp_reference_no,
                                'date' => $date,
                                'customer_id' => $temp_data[0]['customer_id'],
                                'customer' => $temp_data[0]['customer'],
                                'biller_id' => $temp_data[0]['biller_id'],
                                'biller' => $temp_data[0]['biller'],
                                'warehouse_id' => $temp_data[0]['warehouse_id'],
                                'total' => 0,
                                'order_tax' => 0,
                                'total_tax' => 0,
                                'grand_total' => 0,
                                'paid' => 0,
                                'order_tax_id' => $temp_data[0]['order_tax_id'],
                                'created_by' => $this->session->userdata('user_id'),
                                'add_ons' => "initial_first_add_ons:{}:cs_reference_no:{".$reference_no."}:kh_currency_rate:{}:delivery_date:{}:'",
                            );                            
                            $this->db->insert('returns', $temp);
                            $this->site->updateReference('re');
                            
                            $temp_max = $this->site->api_select_some_fields_with_where("MAX(id) as max","sma_returns","id > 0","arr");
                                                                                      
                            $temp_sale_items = $this->site->api_select_some_fields_with_where("*
                                "
                                ,"sma_consignment_items"
                                ,"sale_id = ".$temp_id
                                ,"arr"
                            );
                            //$this->sma->print_arrays($temp_sale_items);
                            for ($i=0;$i<count($temp_sale_items);$i++) {
                                if (${'consignment_qty_'.$temp_sale_items[$i]['id']} > 0) {
                                    $temp_subtotal = ${'consignment_qty_'.$temp_sale_items[$i]['id']} * $temp_sale_items[$i]['unit_price'];
                                    $temp = array(
                                        'return_id' => $temp_max[0]['max'],
                                        'product_id' => $temp_sale_items[$i]['product_id'],
                                        'product_code' => $temp_sale_items[$i]['product_code'],
                                        'product_name' => $temp_sale_items[$i]['product_name'],
                                        'option_id' => $temp_sale_items[$i]['option_id'],
                                        'unit_price' => $temp_sale_items[$i]['unit_price'],
                                        'quantity' => ${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                        'unit_quantity' => ${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                        'warehouse_id' => $temp_sale_items[$i]['warehouse_id'],
                                        'subtotal' => $temp_subtotal,
                                        'product_unit_id' => $temp_sale_items[$i]['product_unit_id'],
                                        'product_unit_code' => $temp_sale_items[$i]['product_unit_code'],
                                        'product_type' => $temp_sale_items[$i]['product_type'],
                                    );                            
                                    $this->db->insert('return_items', $temp);

                                    $config_data_2 = array(
                                        'product_id' => $temp_sale_items[$i]['product_id'],
                                        'type' => 'addition',
                                        'quantity' => ${'consignment_qty_'.$temp_sale_items[$i]['id']},
                                        'warehouse_id' => $temp_sale_items[$i]['warehouse_id'],
                                        'option_id' => $temp_sale_items[$i]['option_id'],
                                    );
                                    $this->site->api_update_quantity_stock($config_data_2);

                                }
                            }
                            $temp = $this->site->api_select_some_fields_with_where("subtotal
                                "
                                ,"sma_return_items"
                                ,"return_id = ".$temp_max[0]['max']
                                ,"arr"
                            );
                            $temp_grand_total = 0;
                            $temp_total = 0;
                            for ($i=0;$i<count($temp);$i++) {
                                $temp_total = $temp_total + $temp[$i]['subtotal'];
                            }
                            $temp_total = $this->sma->formatDecimal($temp_total, 4);
                                                    
                            $order_tax = 0;
                            $total_tax = 0;
                            $temp_grand_total = $this->sma->formatDecimal($temp_total, 4);
                            if ($customers->vat_no) {
                                $order_tax = $this->site->calculateOrderTax($temp_order_tax, $temp_total);
                                $temp_grand_total = $this->sma->formatDecimal(($temp_total + $order_tax), 4);
                                $order_tax = $this->site->calculateOrderTax($temp_order_tax, $temp_total);
                                $total_tax = $this->sma->formatDecimal($order_tax, 4);
                            }                         
                            
                            $this->site->api_update_table('sma_returns',array('total' => $temp_total, 'grand_total' => $temp_grand_total, 'order_tax' => $order_tax, 'total_tax' => $total_tax),"id = ".$temp_max[0]['max']);

                            //$this->site->api_update_sale_consignment_status($temp_data[0]['id']);

                            $this->session->set_flashdata('message', lang("Return_Added"));
                            admin_redirect("consignment/index");                                    
                        }


                    }
                    else {
                        $this->session->set_flashdata('error', lang("please_select_consignment_qty"));
                        admin_redirect("consignment/index");
                    }
                }
                else {
                    $this->session->set_flashdata('error', lang("consignment_not_found"));
                    admin_redirect("consignment/index");
                }
            }
            else {
                if ($cs_reference_no) {
                    $this->data['modal_title'] = lang('edit').' '.lang('consignment');
                    $select_data = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_consignment"
                        ,"reference_no = '".$cs_reference_no."'"
                        ,"arr"
                    );

                    // if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
                    //     $this->session->set_flashdata('error', lang("Can_not_add_sale_with_consignment_status_completed."));
                    //     admin_redirect("consignment/index");
                    // }


                    $this->data['select_data'] = $select_data;
                    $inv = $this->consignment_model->getInvoiceConsignmentByID($select_data[0]['id']);
                    $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
                    $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
                    $this->data['created_by'] = $this->site->getUser($inv->created_by);
                    $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
                    $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
                    $this->data['inv'] = $inv;
                    $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsConsignment($select_data[0]['id']);
                    $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceConsignmentByID($inv->return_id) : NULL;
                    $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItemsConsignment($inv->return_id) : NULL;
                    $rows = $this->data['rows'];
                    $temp_display = $this->consignment_model->get_consignment_table($inv,$rows,$select_data[0]['id']);

                    $this->data['get_consignment_table'] = $temp_display;
                                
                }

                if ($_GET['return'] == '') {
                    $this->data['modal_title'] = lang('add_sale_consignment');
                    $this->data['modal_submit_label'] = lang('Add_Sale');
                }
                else {
                    $this->data['modal_title'] = lang('Add_Return_Consignment');
                    $this->data['modal_submit_label'] = lang('Add_Return');
                }
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'consignment/add_sale_consignment', $this->data);
            }
        }
    }

    public function api_get_table_sale_items()
    {
        if ($this->Owner || $this->Admin) {
            $temp = $this->input->get('reference_no', true);
            $temp_data = $this->site->api_select_some_fields_with_where("id
                "
                ,"sma_consignment"
                ,"reference_no = '".$temp."'"
                ,"arr"
            );
            $id = $temp_data[0]['id'];
                         
            if ($id != '') {
                $inv = $this->consignment_model->getInvoiceConsignmentByID($id);
                if (!$this->session->userdata('view_right')) {
                    $this->sma->view_rights($inv->created_by, true);
                }        
                $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
                $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
                $this->data['created_by'] = $this->site->getUser($inv->created_by);
                $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
                $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
                $this->data['inv'] = $inv;
                $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsConsignment($id);
                $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceConsignmentByID($inv->return_id) : NULL;
                $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItemsConsignment($inv->return_id) : NULL;
                $rows = $this->data['rows'];                
            }
            $temp_display = $this->consignment_model->get_consignment_table($inv,$rows,'');
            $this->sma->send_json(array(array('api_result' => $temp_display)));                        
        }
        else
            admin_redirect('welcome');        
    }
    public function modal_view_consignment($id = null)
    {
        $this->sma->checkPermissions('index', true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->consignment_model->getInvoiceConsignmentByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_consignment'");
        $temp = $this->site->api_select_some_fields_with_where("*
            ".$temp_field
            ,"sma_consignment"
            ,"id = ".$id
            ,"arr"
        );
        $this->data['inv'] = $inv;
        foreach ($temp[0] as $key => $value) {
            $this->data['inv']->{$key} = $value;
        }

    
        if ($this->data['customer']->parent_id != '') {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$this->data['customer']->parent_id
                ,"arr"
            );
            if (count($temp) > 0)
                $this->data['inv']->parent_name = $temp[0]['company'];
        }

        $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsConsignment($id);
        $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceConsignmentByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItemsConsignment($inv->return_id) : NULL;

        $this->load->view($this->theme . 'consignment/modal_view_consignment', $this->data);
    }




    public function pdf_consignment($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->consignment_model->getInvoiceConsignmentByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->consignment_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_consignment'");
        $temp = $this->site->api_select_some_fields_with_where("id
            ".$temp_field
            ,"sma_consignment"
            ,"id = ".$id
            ,"arr"
        );
        foreach ($temp[0] as $key => $value) {
            $this->data['inv']->{$key} = $value;
        }        

        if ($this->data['customer']->parent_id != '') {
            $temp = $this->site->api_select_some_fields_with_where("
                *     
                "
                ,"sma_companies"
                ,"id = ".$this->data['customer']->parent_id
                ,"arr"
            );
            if (count($temp) > 0)
                $this->data['inv']->parent_name = $temp[0]['company'];
        }

        /*
        if ($this->data['inv']->company_branch > 0) {

            $config_data = array(
                'table_name' => 'sma_company_branch',
                'select_table' => 'sma_company_branch',
                'translate' => 'en',
                'select_condition' => "id = ".$this->data['inv']->company_branch,
            );
            $temp = $this->site->api_select_data_v2($config_data);

            if ($temp[0]['id'] > 0)
                $this->data['inv']->company_branch = lang('Branch').': '.$temp[0]['title'].'<br>'.$temp[0]['address'];          
        }
        */

        $this->data['rows'] = $this->consignment_model->getAllInvoiceItemsConsignment($id);
        $this->data['return_sale'] = $inv->return_id ? $this->consignment_model->getInvoiceConsignmentByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->consignment_model->getAllInvoiceItemsConsignment($inv->return_id) : NULL;
        //$this->data['paypal'] = $this->consignment_model->getPaypalSettings();
        //$this->data['skrill'] = $this->consignment_model->getSkrillSettings();

        $name = lang("consignment") . "_" .'CS'.str_pad($inv->id, 5, '0', STR_PAD_LEFT). ".pdf";
        $html = $this->load->view($this->theme . 'consignment/pdf', $this->data, true);
        if (! $this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }

        if ($view) {
            $this->load->view($this->theme . 'consignment/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
			//$this->sma->print_arrays($html);
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }
    function getSelectCompanyBranch()
    {
        if ($this->session->userdata('user_id') != ''){        
            foreach($_GET as $name => $value) {
                ${$name} = $value;
            }
            $temp = $this->site->api_select_some_fields_with_where("*
                "
                ,"sma_companies"
                ,"id = ".$id
                ,"arr"
            );
            $temp2 = $this->site->api_select_some_fields_with_where("id,
                getTranslate(translate,'en','".f_separate."','".v_separate."') as title,
                getTranslate(add_ons,'branch_number','".f_separate."','".v_separate."') as branch_number
                "
                ,"sma_company_branch"
                ,"parent_id = '".$temp[0]['id']."' order by title asc"
                ,"arr"
            );
            for ($i=0;$i<count($temp2);$i++) {
                $temp_value = $temp2[$i]['id'];
                $tr[$temp_value] = $temp2[$i]['title'];
            }
            if (count($temp2) <= 0)
                $tr[''] = lang("none");            
            echo form_dropdown('add_ons_company_branch', $tr, '', 'data-placeholder="'.lang("none").'" class="form-control" id="add_ons_company_branch"');
        }
    }	

    public function consignment_sale_history($id)
    {
        if ($this->Owner || $this->Admin) {
            $this->sma->checkPermissions();
            
            $select_data = $this->site->api_select_some_fields_with_where("*
                "
                ,"sma_consignment_sale_items"
                ,"consignment_id = ".$id." order by id desc"
                ,"arr"
            );
            $total_quantity = 0;

            $temp = $this->site->api_select_some_fields_with_where("reference_no
                "
                ,"sma_consignment"
                ,"id = ".$id
                ,"arr"
            );
            $temp2 = $this->site->api_select_some_fields_with_where("quantity
                "
                ,"sma_consignment_items"
                ,"sale_id = ".$id
                ,"arr"
            );
            for ($i=0;$i<count($temp2);$i++) {
                $total_quantity = $total_quantity + $temp2[$i]['quantity'];
            }

            $select_data[0]['reference_no'] = $temp[0]['reference_no'];
            $this->data['total_quantity'] = $total_quantity;
            $this->data['select_data'] = $select_data;

            $this->data['modal_title'] = lang('Sale_Consignment_History');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'consignment/consignment_sale_history', $this->data);
        }
    }
    function set_delivering_bulk($val)
    {
        $select_data = array();

        $this->form_validation->set_rules('add_ons_delivery_person', lang("delivery_person"), 'trim|required');
        if ($this->form_validation->run() == true) {
            
            $temp = $this->input->post('sales');
            $temp = explode('-', $temp);
            $temp2 = array();
            for ($i=0;$i<count($temp);$i++) {
                if ($temp[$i] > 0)
                    array_push($temp2, $temp[$i]);
            }
            
            $temp_count = 0;
            foreach ($temp2 as $id) {                
                $select_data = $this->consignment_model->api_select_data($id);
                
                if ($select_data[0]['sale_status'] != '' && $select_data[0]['sale_status'] != 'pending' && $select_data[0]['sale_status'] != 'print needed') {
                    $temp_count++;
                    $config_data = array(
                        'table_name' => 'sma_consignment',
                        'id_name' => 'id',
                        'field_add_ons_name' => 'add_ons',
                        'selected_id' => $id,
                        'add_ons_title' => 'delivery_person',
                        'add_ons_value' => $this->input->post('add_ons_delivery_person'),
                    );
                    $this->site->api_update_add_ons_field($config_data);                
                    
                    if ($select_data[0]['sale_status'] == 'preparing') {
                        $update_array = array(
                            'sale_status' => 'delivering',
                        );
                        $this->site->api_update_table('sma_consignment',$update_array,"id = ".$id);

                        $config_data_3 = array(
                            'sale_id' => $id,
                        );
                        $this->consignment_model->api_reduce_stock_from_consignment($config_data_3);
                        //$this->sales_model->api_calculate_consignment_purchase_wrapper($id);
                    }
                }
            }
            if ($temp_count > 0)
                $this->session->set_flashdata('message', $temp_count.' '.lang("record").'(s) '.lang("updated"));
            else
                $this->session->set_flashdata('error', lang("Can_not_set_delivering_with_consignment_status_print_needed."));
            admin_redirect("consignment");
        }
        else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['select_data'] = '';
            $this->load->admin_model('sales_model');
            $this->data['delivery_person'] = $this->sales_model->get_delivery_person();
            $this->data['sales'] = $val; 
            $this->load->view($this->theme . 'consignment/set_delivering', $this->data);
        }        
    }
    	
}

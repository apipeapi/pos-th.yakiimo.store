<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Consignment_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $warehouse_id, $limit = 5)
    {
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->db->select('products.*, FWP.quantity as quantity, categories.id as category_id, categories.name as category_name', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("(products.track_quantity = 0 OR FWP.quantity > 0) AND FWP.warehouse_id = '" . $warehouse_id . "' AND "
                . "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }
        // $this->db->order_by('products.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name,products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncQuantity($sale_id)
    {
        if ($consignment_items = $this->getAllInvoiceItems($sale_id)) {
            foreach ($consignment_items as $item) {
                $this->site->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->site->syncVariantQty($item->option_id, $item->warehouse_id);
                }
            }
        }
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $all = NULL)
    {
        $wpv = "( SELECT option_id, warehouse_id, quantity from {$this->db->dbprefix('warehouses_products_variants')} WHERE product_id = {$product_id}) FWPV";
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, FWPV.quantity as quantity', FALSE)
            ->join($wpv, 'FWPV.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');

        if (! $this->Settings->overselling && ! $all) {
            $this->db->where('FWPV.warehouse_id', $warehouse_id);
            $this->db->where('FWPV.quantity >', 0);
        }
        $q = $this->db->get('product_variants');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {

        $q = $this->db->get_where('consignment_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllInvoiceItems($sale_id, $return_id = NULL)
    {
        $this->db->select('consignment_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=consignment_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=consignment_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=consignment_items.tax_rate_id', 'left')
            ->group_by('consignment_items.id')
            ->order_by('id', 'asc');

        // if ($sale_id && !$return_id) {
        if ($sale_id) {
            $this->db->where('sale_id', $sale_id);
        } elseif ($return_id) {
            $this->db->where('sale_id', $return_id);
        }
        $q = $this->db->get('consignment_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllInvoiceItemsConsignment($sale_id, $return_id = NULL)
    {
        $this->db->select('consignment_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=consignment_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=consignment_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=consignment_items.tax_rate_id', 'left')
            ->group_by('consignment_items.id')
            ->order_by('id', 'asc');

        // if ($sale_id && !$return_id) {
        if ($sale_id) {
            $this->db->where('sale_id', $sale_id);
        } elseif ($return_id) {
            $this->db->where('sale_id', $return_id);
        }
        $q = $this->db->get('consignment_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItemsWithDetails($sale_id)
    {
        $this->db->select('consignment_items.*, products.details, product_variants.name as variant');
        $this->db->join('products', 'products.id=consignment_items.product_id', 'left')
        ->join('product_variants', 'product_variants.id=consignment_items.option_id', 'left')
        ->group_by('consignment_items.id');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('consignment_items', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoiceByID($id)
    {
        //$q = $this->db->get_where('consignment', array('id' => $id), 1);

        $config_data = array(
            'table_name' => 'sma_consignment',
            'select_table' => 'sma_consignment',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        if (is_array($select_data)) if (count($select_data) > 0) {
            return $select_data;
        }
/*                
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
*/
        return FALSE;
    }
    public function getInvoiceConsignmentByID($id)
    {
        $q = $this->db->get_where('consignment', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnBySID($sale_id)
    {
        $q = $this->db->get_where('sales', array('sale_id' => $sale_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addSale($data = array(), $items = array(), $payment = array(), $si_return = array())
    {

        if (empty($si_return)) {
            $cost = $this->site->costing($items);
            // $this->sma->print_arrays($cost);
        }
        $is_vat = isset($data['is_vat']) ? $data['is_vat'] : false;
        unset($data['is_vat']);


        $config_data = array(
            'type' => 'consignment',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference_no'] = $temp['reference_no'];
        
        if ($this->db->insert('consignment', $data)) {
          
            $sale_id = $this->db->insert_id();
            
            foreach ($items as $item) {

                $item['sale_id'] = $sale_id;
                $this->db->insert('consignment_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed' && empty($si_return)) {

                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $sale_id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                //$this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $sale_id;
                                $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    //$this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }

                }
            }

            if (!empty($si_return)) {
                $this->db->update('consignment', array('return_sale_ref' => $data['return_sale_ref'], 'surcharge' => $data['surcharge'],'return_sale_total' => $data['grand_total'], 'return_id' => $sale_id), array('id' => $data['sale_id']));
            }

            // $config_data_3 = array(
            //     'sale_id' => $sale_id,
            // );
            // $this->api_reduce_stock_from_consignment($config_data_3);

            $config_data = array(
                'type' => 'consignment',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            $temp_riel = $this->site->api_select_some_fields_with_where("rate
                "
                ,"sma_currencies"
                ,"code = 'KHR' and YEAR(date) = YEAR('".$data['date']."') and MONTH(date) = MONTH('".$data['date']."') and DAY(date) = DAY('".$data['date']."') order by date desc limit 1"
                ,"arr"
            );
            if ($temp_riel[0]['rate'] > 0) {
                $config_data_2 = array(
                    'table_name' => 'sma_consignment',
                    'id_name' => 'id',
                    'field_add_ons_name' => 'add_ons',
                    'selected_id' => $sale_id,
                    'add_ons_title' => 'kh_currency_rate',
                    'add_ons_value' => $temp_riel[0]['rate'],
                );
                $this->site->api_update_add_ons_field($config_data_2);                
            }            

            return $sale_id;
        }

        return false;
    }

    public function api_reduce_stock_from_consignment($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_consignment_items"
            ,"sale_id = ".$config_data['sale_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'subtraction',
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $temp[$i]['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }        
    }

    public function api_update_stock_sale($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_consignment_items"
            ,"sale_id = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => $config_data['type'],
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $config_data['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }
    }

    public function updateSale($id, $data, $items = array())
    {

        $config_data = array(
            'table_name' => 'sma_consignment',
            'select_table' => 'sma_consignment',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $select_data[0]['warehouse_id'],
                'type' => 'addition',
            );
            $this->api_update_stock_sale($config_data_2);
        }

        $is_vat = isset($data['is_vat']) ? $data['is_vat'] : false;
        unset($data['is_vat']);
        if ($this->db->update('consignment', $data, array('id' => $id)) && $this->db->delete('consignment_items', array('sale_id' => $id))) {

            $sale_id = $this->db->insert_id();
            
            foreach ($items as $item) {

                $item['sale_id'] = $id;
                $this->db->insert('consignment_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed' && $this->site->getProductByID($item['product_id'])) {
                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $id;
                                $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }
                }

            }
            //$this->site->syncSalePayments($id);
            //$this->site->syncQuantity($id);
            //$sale = $this->getInvoiceByID($id);
            //$this->sma->update_award_points($data['grand_total'], $data['customer_id'], $sale->created_by);
            $return = true;
        }
        else
            $return = false;

        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $data['warehouse_id'],
                'type' => 'subtraction',
            );
            $this->api_update_stock_sale($config_data_2);
        }

        return $return;
    }

    public function updateStatus($id, $status, $note)
    {

        $sale = $this->getInvoiceByID($id);
        $items = $this->getAllInvoiceItems($id);
        $cost = array();
        if ($status == 'completed' && $sale->sale_status != 'completed') {
            foreach ($items as $item) {
                $items_array[] = (array) $item;
            }
            $cost = $this->site->costing($items_array);
        }
        if ($status != 'completed' && $sale->sale_status == 'completed') {
            $this->resetSaleActions($id);
        }

        if ($this->db->update('sales', array('sale_status' => $status, 'note' => $note), array('id' => $id)) && $this->db->delete('costing', array('sale_id' => $id))) {
            if ($status == 'completed' && $sale->sale_status != 'completed') {
                foreach ($items as $item) {
                    $item = (array) $item;
                    if ($this->site->getProductByID($item['product_id'])) {
                        $item_costs = $this->site->item_costing($item);
                        foreach ($item_costs as $item_cost) {
                            $item_cost['sale_item_id'] = $item['id'];
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($sale->date));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        }
                    }
                }
            }

            if (!empty($cost)) { $this->site->syncPurchaseItems($cost); }
            $this->site->syncQuantity($id);
            return true;
        }
        return false;
    }

    public function deleteSale($id)
    {
        $consignment_items = $this->resetSaleActions($id);
        if ($this->db->delete('consignment_items', array('sale_id' => $id)) &&
        $this->db->delete('consignment', array('id' => $id)) &&
        $this->db->delete('costing', array('sale_id' => $id))) {
            $this->db->delete('consignment_items', array('sale_id' => $id));
            $this->db->delete('consignment', array('id' => $id));
            return true;
        }
        return FALSE;
    }

    public function resetSaleActions($id, $return_id = NULL, $check_return = NULL)
    {
        if ($sale = $this->getInvoiceByID($id)) {
            if ($check_return && $sale->sale_status == 'returned') {
                $this->session->set_flashdata('warning', lang('sale_x_action'));
                redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }

            if ($sale->sale_status == 'completed') {
                if ($costings = $this->getSaleCosting($id)) {
                    foreach ($costings as $costing) {
                        if ($pi = $this->getPurchaseItemByID($costing->purchase_item_id)) {
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        } else {
                            // $sale_item = $this->getSaleItemByID($costing->sale_item_id);
                            $pi = $this->site->getPurchasedItem(['product_id' => $costing->product_id, 'option_id' => $costing->option_id ? $costing->option_id : NULL, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $sale->warehouse_id]);
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        }
                    }
                }
                $items = $this->getAllInvoiceItems($id);
                $this->site->syncQuantity(NULL, NULL, $items);
                $this->sma->update_award_points($sale->grand_total, $sale->customer_id, $sale->created_by, TRUE);
                return $items;
            }
        }
    }

    public function getPurchaseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCostingLines($sale_item_id, $product_id, $sale_id = NULL)
    {
        if ($sale_id) { $this->db->where('sale_id', $sale_id); }
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('id', $orderby);
        $q = $this->db->get_where('costing', array('sale_item_id' => $sale_item_id, 'product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleItemByID($id)
    {
        $q = $this->db->get_where('consignment_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addDelivery($data = array())
    {
        if ($this->db->insert('deliveries', $data)) {
            if ($this->site->getReference('do') == $data['do_reference_no']) {
                $this->site->updateReference('do');
            }
            return true;
        }
        return false;
    }

    public function updateDelivery($id, $data = array())
    {
        if ($this->db->update('deliveries', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getDeliveryByID($id)
    {
        $q = $this->db->get_where('deliveries', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDeliveryBySaleID($sale_id)
    {
        $q = $this->db->get_where('deliveries', array('sale_id' => $sale_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDelivery($id)
    {
        if ($this->db->delete('deliveries', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getInvoicePayments($sale_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id)
    {
        $q = $this->db->get_where('payments', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentsForSale($sale_id)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array(), $customer_id = null)
    {
        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('pay') == $data['reference_no']) {
                $this->site->updateReference('pay');
            }
            $this->site->syncSalePayments($data['sale_id']);
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array(), $customer_id = null)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncSalePayments($data['sale_id']);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                if (!$customer_id) {
                    $sale = $this->getInvoiceByID($opay->sale_id);
                    $customer_id = $sale->customer_id;
                }
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncSalePayments($opay->sale_id);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                $sale = $this->getInvoiceByID($opay->sale_id);
                $customer = $this->site->getCompanyByID($sale->customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    /* ----------------- Gift Cards --------------------- */

    public function addGiftCard($data = array(), $ca_data = array(), $sa_data = array())
    {
        if ($this->db->insert('gift_cards', $data)) {
            if (!empty($ca_data)) {
                $this->db->update('companies', array('award_points' => $ca_data['points']), array('id' => $ca_data['customer']));
            } elseif (!empty($sa_data)) {
                $this->db->update('users', array('award_points' => $sa_data['points']), array('id' => $sa_data['user']));
            }
            return true;
        }
        return false;
    }

    public function updateGiftCard($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('gift_cards', $data)) {
            return true;
        }
        return false;
    }

    public function deleteGiftCard($id)
    {
        if ($this->db->delete('gift_cards', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get_where('paypal', array('id' => 1));
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get_where('skrill', array('id' => 1));
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaff()
    {
        if (!$this->Owner) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function topupGiftCard($data = array(), $card_data = NULL)
    {
        if ($this->db->insert('gift_card_topups', $data)) {
            $this->db->update('gift_cards', $card_data, array('id' => $data['card_id']));
            return true;
        }
        return false;
    }

    public function getAllGCTopups($card_id)
    {
        $this->db->select("{$this->db->dbprefix('gift_card_topups')}.*, {$this->db->dbprefix('users')}.first_name, {$this->db->dbprefix('users')}.last_name, {$this->db->dbprefix('users')}.email")
        ->join('users', 'users.id=gift_card_topups.created_by', 'left')
        ->order_by('id', 'desc')->limit(10);
        $q = $this->db->get_where('gift_card_topups', array('card_id' => $card_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemRack($product_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            $wh = $q->row();
            return $wh->rack;
        }
        return FALSE;
    }

    public function getSaleCosting($sale_id)
    {
        $q = $this->db->get_where('costing', array('sale_id' => $sale_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function UpdateCostingAndPurchaseItem($return_item, $product_id, $quantity)
    {
        $bln_quantity = $quantity;
        if ($costings = $this->getCostingLines($return_item['id'], $product_id)) {
            foreach ($costings as $costing) {
                if ($costing->quantity > $bln_quantity && $bln_quantity != 0) {
                    $qty = $costing->quantity - $bln_quantity;
                    $bln = $costing->quantity_balance && $costing->quantity_balance >= $bln_quantity ? $costing->quantity_balance - $bln_quantity : 0;
                    $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $costing->id));
                    $bln_quantity = 0;
                    break;
                } elseif ($costing->quantity <= $bln_quantity && $bln_quantity != 0) {
                    $this->db->delete('costing', array('id' => $costing->id));
                    $bln_quantity = ($bln_quantity - $costing->quantity);
                }
            }
        }
        $clause = ['product_id' => $product_id, 'warehouse_id' => $return_item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $return_item['option_id']];
        $this->site->setPurchaseItem($clause, $quantity);
        $this->site->syncQuantity(null, null, null, $product_id);
    }
	
	/*** Bulk Actions On Sale ***/
	public function getInvoiceByIDPayCate($id)
    {
        $this->db->select('sales.*, companies.payment_category');
        $this->db->join('companies', 'companies.id=sales.customer_id', 'left');
        $q = $this->db->get_where('sales', array('sales.id' => $id));
     
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;

    }
	
	public function update_mpayment_status_to_due($id)
    {
        $pc = $this->getInvoiceByIDPayCate($id);
        $pay_cate=$pc->payment_category;
        if ($pay_cate=="weekly" || $pay_cate=="cash on delivery" || $pay_cate=="") {
			$update_payment_status=array(
				 'payment_status'=>'due',
				 'paid'=>0,
            );
            $this->db->where(array('id' => $id));
            $this->db->update('sales',$update_payment_status); 
        }    
    }
	
	public function update_mpayment_status_to_partial($id)
    {
        $sale = $this->getInvoiceByIDPayCate($id);
        $pay_cate=$sale->payment_category;
        if ($pay_cate=="monthly" || $pay_cate=="2 month" || $pay_cate=="") {
			$update_payment_status=array(
				'payment_status'=>'partial',
            );
            $this->db->where(array('id' => $id));
            $this->db->update('sales',$update_payment_status); 
        }    
    }
	
	public function getSaleID($id) {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
	public function update_mpayment_status_to_paid($id)
    {
        $sale = $this->getSaleID($id);
        $amount=$sale->grand_total;
            $update_payment_status=array(
				'payment_status'=>'paid',
				'paid'=>$amount,  
            );
            $this->db->where(array('id' => $id));
            $this->db->update('sales',$update_payment_status); 
    }
    
    /* custom function for pagination */
	public function get_paging_data($warehouse_id = "", $keyword = "", $sort_by = "", $sort_order = "", $limit = "",$offset = "") {

        $sql = $this->db->select("
                    {$this->db->dbprefix('consignment')}.id as id, 
                    {$this->db->dbprefix('consignment')}.date as date, 
                    {$this->db->dbprefix('consignment')}.due_date as due_date, 
                    reference_no,
                    biller, 
                    {$this->db->dbprefix('consignment')}.customer, 
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status, 
                    {$this->db->dbprefix('consignment')}.attachment, 
                    return_id"
                )
                ->join('companies','companies.id = consignment.customer_id','left')
                ->where('pos !=', 1); // ->where('sale_status !=', 'returned');

        if ($this->input->get('shop') == 'yes') {
            $this->db->where('shop', 1);

        } elseif ($this->input->get('shop') == 'no') {
            $this->db->where('shop !=', 1);
        }

        if ($this->input->get('delivery') == 'no') {
            $this->db->join('deliveries', 'deliveries.sale_id=consignment.id', 'left')
                ->where('consignment.sale_status', 'completed')
                ->where('consignment.payment_status', 'paid')
                ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR 
                        {$this->db->dbprefix('deliveries')}.status IS NULL)", NULL);
        }

        if ($this->input->get('attachment') == 'yes') {
            $this->db->where('payment_status !=', 'paid')->where('attachment !=', NULL);
        }

        if ((!$this->Customer) && !$this->Supplier && 
                    !$this->Owner && !$this->Admin && 
                    !$this->session->userdata('view_right')) {
            $sql_query = $sql_query->where('created_by', $this->session->userdata('user_id'));
        }
        if ($this->Customer){
            $sql_query = $sql_query->where('customer_id', $this->session->userdata('user_id'));
        }

        if ($warehouse_id != '') {
            $this->db->where("{$this->db->dbprefix('consignment')}.warehouse_id", $warehouse_id);
        }

        if ($keyword != '') {
            if ($keyword == 'Today Reminder') {
                $this->db->where("
                    DATE(due_date) = CURDATE() OR
                    CURDATE() = DATE(due_date) - INTERVAL getTranslate(sma_consignment.add_ons,'reminder_day','".f_separate."','".v_separate."') DAY
                    ", 
                    NULL, 
                    FALSE
                );                
            }            
            else {
                $this->db->where("
                    (customer LIKE '%". $keyword ."%' OR 
                    sale_status LIKE '%". $keyword ."%' OR 
                    biller LIKE '%". $keyword ."%' OR 
                    reference_no LIKE '%". $keyword ."%' OR 
                    date LIKE '%". $keyword ."%' OR 
                    payment_status LIKE '%". $keyword ."%')", 
                    NULL, 
                    FALSE
                );
            }
        }   

        if ($sort_by != '') {
            $this->db->order_by("$sort_by", "$sort_order");
        }
        else
            $this->db->order_by("id", "DESC");
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        // echo '<pre>';print_r($keyword);  exit();
        // $result = $this->db->get('sales');
      
        return $this->db->get('consignment');
    }

	public function get_paging_data_consignment($warehouse_id = "", $keyword = "", $sort_by = "", $sort_order = "", $limit = "",$offset = "") {
        
        $sql = $this->db->select("
                    {$this->db->dbprefix('consignment')}.id as id, 
                    {$this->db->dbprefix('consignment')}.date as date, 
                    reference_no, 
                    biller, 
                    {$this->db->dbprefix('consignment')}.customer, 
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status, 
                    {$this->db->dbprefix('consignment')}.attachment, 
                    return_id"
                )
                ->join('companies','companies.id = consignment.customer_id','left')
                ->where('pos !=', 1); // ->where('sale_status !=', 'returned');

        if ($this->input->get('shop') == 'yes') {
            $this->db->where('shop', 1);

        } elseif ($this->input->get('shop') == 'no') {
            $this->db->where('shop !=', 1);
        }

        if ($this->input->get('delivery') == 'no') {
            $this->db->join('deliveries', 'deliveries.sale_id=consignment.id', 'left')
                ->where('consignment.sale_status', 'completed')
                ->where('consignment.payment_status', 'paid')
                ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR 
                        {$this->db->dbprefix('deliveries')}.status IS NULL)", NULL);
        }

        if ($this->input->get('attachment') == 'yes') {
            $this->db->where('payment_status !=', 'paid')->where('attachment !=', NULL);
        }

        if ((!$this->Customer) && !$this->Supplier && 
                    !$this->Owner && !$this->Admin && 
                    !$this->session->userdata('view_right')) {
            $sql_query = $sql_query->where('created_by', $this->session->userdata('user_id'));
        }
        if ($this->Customer){
            $sql_query = $sql_query->where('customer_id', $this->session->userdata('user_id'));
        }

        if ($warehouse_id != '') {
            $this->db->where("{$this->db->dbprefix('consignment')}.warehouse_id", $warehouse_id);
        }

        if (is_numeric($temp_keyword)) {
            $temp_keyword = $this->db->escape($keyword);
            $temp_keyword = str_replace("'",'',$temp_keyword);
        }
        else
            $temp_keyword = $this->db->escape($keyword);
        if ($keyword != '') {
            $this->db->where("
                (customer LIKE '%". $keyword ."%' OR 
                sale_status LIKE '%". $keyword ."%' OR 
                biller LIKE '%". $keyword ."%' OR 
                reference_no LIKE '%". $keyword ."%' OR 
                {$this->db->dbprefix('consignment')}.id = ".$temp_keyword." OR 
                date LIKE '%". $keyword ."%' OR 
                payment_status LIKE '%". $keyword ."%')", 
                NULL, 
                FALSE
            );
        }

        if ($sort_by != '') {
            $this->db->order_by("$sort_by", "$sort_order");
        }
        else
            $this->db->order_by("date", "DESC");
            
        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }
        // echo '<pre>';print_r($keyword);  exit();
        // $result = $this->db->get('sales');
      
        return $this->db->get('consignment');
    }
	public function get_consignment_table($inv,$rows,$edit_id) {	   
        if ($_GET['return'] == 1)
            $temp = lang("Return_Quantity");
        else
            $temp = lang("Add_Quantity");
        $temp_display = '
            <table class="table table-bordered table-hover table-striped print-table order-table">    
            <thead>    
            <tr>
                <th>'.lang("no.").'</th>
                <th>'.lang("description").'</th>
                <th>'.lang("unit_price").'</th>
                <th>'.lang("consignment_quantity").'</th>
                <th>'.lang("sold_quantity").'</th>
                <th>'.lang("returned_quantity").'</th>
                <th>'.lang("available_quantity").'</th>
                <th>'.$temp.'</th>
            </tr>        
            </thead>
            <tbody>
        ';
            $r = 1;
            if ($rows) {
                foreach ($rows as $row):
                    $temp_display .= '        
                    <tr>
                        <td style="text-align:center; width:40px; vertical-align:middle;">'.$r.'</td>
                        <td style="vertical-align:middle;">
                    ';
                    $temp_display .= '
                    '.$row->product_code.' - '.$row->product_name;
                    if ($row->variant)
                    $temp_display .= '(' . $row->variant . ')';
                    if ($row->second_name)
                    $temp_display .= '<br>' . $row->second_name;
                    if ($row->details) 
                    $temp_display .= '<br>' . $row->details;
                    if ($row->serial_no) 
                    $temp_display .= '<br>' . $row->serial_no;

                    $temp_display .= '
                        </td>
                    ';

                    $temp = $this->site->api_select_some_fields_with_where("*
                        "
                        ,"sma_consignment_sale_items"
                        ,"consignment_id = ".$row->sale_id." and product_id = ".$row->product_id
                        ,"arr"
                    );
                    $temp_qty = 0;
                    for ($i5=0;$i5<count($temp);$i5++) {
                        $temp_qty = $temp_qty + $temp[$i5]['quantity'];
                    }

                    $temp = $this->site->api_select_some_fields_with_where("id
                        "
                        ,"sma_returns"
                        ,"add_ons LIKE '%:cs_reference_no:{".$inv->reference_no."}:%'"
                        ,"arr"
                    );
                    $temp_retured_qty = 0;
                    for ($i=0;$i<count($temp);$i++) {                       
                        $temp_2 = $this->site->api_select_some_fields_with_where("quantity
                            "
                            ,"sma_return_items"
                            ,"return_id = ".$temp[$i]['id']." and product_id = ".$row->product_id
                            ,"arr"
                        );
                        $temp_retured_qty = $temp_retured_qty + $temp_2[0]['quantity'];
                    }                    

                    $temp_consignment_qty = $row->quantity - $temp_qty - $temp_retured_qty;
                    if ($temp_consignment_qty <= 0) $temp_disable = 'disabled="disabled"'; else $temp_disable = '';
                    $temp_display .= '
                        <td style="text-align:right; width:100px;">'.$this->sma->formatMoney($row->unit_price).'</td>        
                        <td style="width: 80px; text-align:center; vertical-align:middle;">'.$this->sma->formatQuantity($row->quantity).'</td>
                        <td style="width: 80px; text-align:center; vertical-align:middle;"">'.$this->sma->formatQuantity($temp_qty).'</td>
                        <td style="width: 80px; text-align:center; vertical-align:middle;"">'.$this->sma->formatQuantity($temp_retured_qty).'</td>
                        <td style="width: 80px; text-align:center; vertical-align:middle;"">'.$this->sma->formatQuantity($temp_consignment_qty).'</td>
                        <td style="text-align:right; width:100px;">
                            <select class="form-control" name="consignment_qty_'.$row->id.'" id="consignment_qty_'.$row->id.'" '.$temp_disable.'>
                    ';
                    /*
                    if ($edit_id != '') {
                        $temp_2 = $this->site->api_select_some_fields_with_where("quantity
                            "
                            ,"sma_consignment_items"
                            ,"sale_id = '".$edit_id."' and product_id = ".$row->product_id
                            ,"arr"
                        );
                    }
                    */
                    for ($i=0;$i<=$temp_consignment_qty;$i++) {
                        $temp1 = '';
                        /*
                        if ($edit_id != '') {                      
                            if ($i == $temp_2[0]['quantity']) $temp1 = 'selected="selected"';
                        }
                        */
                        $temp_display .= '        
                            <option value="'.$i.'" '.$temp1.'>'.$i.'</option>
                        ';
                    }

                    $temp_display .= '        
                            </select>                                
                        </td>
                    ';
                    if ($Settings->tax1 && $inv->product_tax > 0) {
                        $temp_display .= '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                    }
                    if ($Settings->product_discount && $inv->product_discount != 0) {
                        $temp_display .= '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                    }
                    $temp_display .= '        
                    </tr>
                    ';  
                    $r++;
                endforeach;
            }
            else {
                $temp_display .= '
                    <tr>
                        <td colspan="5">
                           '.lang("consignment_not_found").'
                        </td>
                    </tr>        
                ';                   
            }
            
        $temp_display .= '            
            </tbody>    
            </table>
        ';
        return $temp_display;
    }

    public function api_consignment_delete($id) {
        $return = array();

        $config_data = array(
            'table_name' => 'sma_consignment',
            'select_table' => 'sma_consignment',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        $temp_sale = $this->site->api_select_some_fields_with_where("
        id"
            ,"sma_consignment_sale_items"
            ,"consignment_id = ".$id
            ,"arr"
        );
        if (is_array($temp_sale)) if (count($temp_sale) > 0) {
            $return['error'] = lang('Can_not_delete_any_consignment_with_a_sale_record.');
            return $return;
        }

        $temp_return = $this->site->api_select_some_fields_with_where("id
            "
            ,"sma_returns"
            ,"add_ons like '%:cs_reference_no:{".$select_data[0]['reference_no']."}:%'"
            ,"arr"
        );
        if (is_array($temp_return)) if (count($temp_return) > 0) {
            $return['error'] = lang('Can_not_delete_any_consignment_with_a_return_record.');
            return $return;
        }

        if ($select_data[0]['sale_status'] == 'delivering' || $select_data[0]['sale_status'] == 'completed') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $select_data[0]['warehouse_id'],
                'type' => 'addition',
            );
            $this->api_update_stock_consignment($config_data_2);
        }

        $this->db->delete('consignment_items', array('sale_id' => $id));
        $this->db->delete('consignment', array('id' => $id));
        $this->db->delete('consignment_sale_items', array('consignment_id' => $id));

        return $return;
    }
    public function api_update_stock_consignment($config_data) {
        if ($config_data['warehouse_id'] <= 0) $config_data['warehouse_id'] = $this->Settings->default_warehouse;
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_consignment_items"
            ,"sale_id = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => $config_data['type'],
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $config_data['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }
    }

    public function api_consignment_edit_check($id) {
        $return = array();

        $config_data = array(
            'table_name' => 'sma_consignment',
            'select_table' => 'sma_consignment',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        $temp_sale = $this->site->api_select_some_fields_with_where("
        id"
            ,"sma_consignment_sale_items"
            ,"consignment_id = ".$id
            ,"arr"
        );
        if (is_array($temp_sale)) if (count($temp_sale) > 0) {
            $return['error'] = lang('Can_not_edit_any_consignment_with_a_sale_record.');
            return $return;
        }

        $temp_return = $this->site->api_select_some_fields_with_where("id
            "
            ,"sma_returns"
            ,"add_ons LIKE '%:cs_reference_no:{".$select_data[0]['reference_no']."}:%'"
            ,"arr"
        );
        if (is_array($temp_return)) if (count($temp_return) > 0) {
            $return['error'] = lang('Can_not_edit_any_consignment_with_a_return_record.');
            return $return;
        }

        return $return;
    }

    public function api_get_consignment($config_data) {
        $condition = '';
        $config_data['search'] = trim($config_data['search']);

        if ($_GET['adjustment_consignment_track'] != '') {
            $temp = explode('-',$_GET['adjustment_consignment_track']);
            $condition = " and (t1.id = ".$temp[1];
            for ($i=2;$i<count($temp);$i++) {
                $condition .= " or t1.id = ".$temp[$i];
            }
            $condition .= ')';
        }        
        elseif ($config_data['search'] != '') {
            if ($config_data['search'] == 'Today Reminder') {
                $condition .= " and DATE(t1.due_date) = CURDATE() OR
                    CURDATE() = DATE(t1.due_date) - INTERVAL getTranslate(t1.add_ons,'reminder_day','".f_separate."','".v_separate."') DAY";
            }
            else {
                $condition .= "
                    and (t1.reference_no LIKE '%". $config_data['search'] ."%'
                    OR t1.customer LIKE '%". $config_data['search'] ."%'
                    OR t1.biller LIKE '%". $config_data['search'] ."%'
                    OR t1.date LIKE '%". $config_data['search'] ."%'
                    OR t1.payment_status LIKE '%". $config_data['search'] ."%'
                    OR t1.sale_status LIKE '%". $config_data['search'] ."%'
                ";
                if (is_numeric($config_data['search']))
                    $condition .= ' OR t1.'.$config_data['table_id']." = ".$config_data['search'];
                $condition .= ')';
            }
        }

        $temp = $this->input->get('warehouse_id');
        if (is_numeric($temp))
            $condition .= " and t1.warehouse_id = ".$temp;

        $temp = $this->input->get('sale_status');
        if ($temp != '')
            $condition .= " and t1.sale_status = '".$temp."'";


        $temp1 = $this->input->get('start_date');
        $temp2 = $this->input->get('end_date');
        if ($temp1 != '' && $temp1 != '')
            $condition .= " and (convert(t1.date, Date) between STR_TO_DATE('".$temp1."', '%d/%m/%Y') and STR_TO_DATE('".$temp2."', '%d/%m/%Y'))";

        $order_by = '';
        $order_by = 'order by t1.'.$config_data['table_id'].' Desc';
        if ($config_data['sort_by'] != '') {
            if ($config_data['sort_by'] == 'sold_amount')
                $order_by = 'order by t1.grand_total';
            else
                $order_by = 'order by t1.'.$config_data['sort_by'];
            $order_by .= ' '.$config_data['sort_order'];
        }
        else
            $order_by = 'order by t1.date Desc';

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");

        if ($config_data['limit'] != '') {
            $temp_select = "t1.*".$temp_field;
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }
        else
            $temp_select = "t1.id";

        $select_data = $this->site->api_select_some_fields_with_where(
            $temp_select
            ,$config_data['table_name']." as t1"
            ,"t1.".$config_data['table_id']." > 0 ".$condition." ".$order_by
            ,"arr"
        );
                   
        return $select_data;
    }
	public function api_select_data($id)
    {
        $temp_field = $this->site->api_get_condition_add_ons_field("add_ons","sma_add_ons","table_name = 'sma_consignment'");
        $select_data = $this->site->api_select_some_fields_with_where("* 
            ".$temp_field
            ,"sma_consignment"
            ,"id = ".$id
            ,"arr"
        );
        return $select_data;        
    }

public function api_get_consignment_qty($config_data) {
    $temp_sold = 0;
    $temp = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_consignment_sale_items"
        ,"consignment_id = ".$config_data['id']
        ,"arr"
    );
    for ($i2=0;$i2<count($temp);$i2++) {
       $temp_sold = $temp_sold + $temp[$i2]['quantity'];
    }    

    $temp = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_returns"
        ,"add_ons LIKE '%:cs_reference_no:{".$config_data['reference_no']."}:%'"
        ,"arr"
    );
    $temp_returned = 0;
    if (is_array($temp)) if (count($temp) > 0) {
        $temp2 = $this->site->api_select_some_fields_with_where("*
            "
            ,"sma_return_items"
            ,"return_id = ".$temp[0]['id']
            ,"arr"
        );
        for ($i2=0;$i2<count($temp2);$i2++) {                       
            $temp_returned = $temp_returned + $temp2[$i2]['quantity'];
        }                        

    }

    $temp = $this->site->api_select_some_fields_with_where("
        quantity
        "
        ,"sma_consignment_items"
        ,"sale_id = ".$config_data['id']
        ,"arr"
    );
    $temp_total = 0;
    for ($i2=0;$i2<count($temp);$i2++) {
        $temp_total = $temp_total + $temp[$i2]['quantity'];
    }     

    $temp_available = $temp_total - $temp_sold - $temp_returned;

    $return['total_qty'] = $temp_total;
    $return['available_qty'] = $temp_available;
    $return['sold_qty'] = $temp_sold;
    $return['returned_qty'] = $temp_returned;

    return $return;
}

}

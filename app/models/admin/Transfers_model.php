<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transfers_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $warehouse_id, $limit = 20)
    {
        $this->db->select('products.id, code, name, warehouses_products.quantity, cost, tax_rate, type, unit, purchase_unit, tax_method')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->db->where("type = 'standard' AND (name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("type = 'standard' AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND warehouses_products.quantity > 0 AND "
                . "(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        }
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getWHProduct($id)
    {
        $this->db->select('products.id, code, name, warehouses_products.quantity, cost, tax_rate')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('products.id');
        $q = $this->db->get_where('products', array('warehouses_products.product_id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function api_calculate_transfers($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_transfer_items"
            ,"transfer_id = ".$config_data['id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            if ($temp[$i]['option_id'] <= 0) {
                $temp_2 = $this->site->api_select_some_fields_with_where("
                    quantity     
                    "
                    ,"sma_warehouses_products"
                    ,"product_id = ".$temp[$i]['product_id']." and warehouse_id = ".$config_data['warehouse_id']
                    ,"arr"
                );
                if (count($temp_2) <= 0) {
                    $temp_3 = array(
                        'product_id' => $temp[$i]['product_id'],
                        'warehouse_id' => $config_data['warehouse_id'],
                        'quantity' => 0,
                    );
                    $this->db->insert('sma_warehouses_products', $temp_3);   
                    $temp_2[0]['quantity'] = 0;                 
                }
            }
            else {
                $temp_2 = $this->site->api_select_some_fields_with_where("
                    quantity     
                    "
                    ,"sma_warehouses_products_variants"
                    ,"option_id = ".$temp[$i]['option_id']." and warehouse_id = ".$config_data['warehouse_id']
                    ,"arr"
                );
                if (count($temp_2) <= 0) {
                    $temp_3 = array(
                        'option_id' => $temp[$i]['option_id'],
                        'product_id' => $temp[$i]['product_id'],
                        'warehouse_id' => $config_data['warehouse_id'],
                        'quantity' => 0,
                    );
                    $this->db->insert('sma_warehouses_products_variants', $temp_3);   
                    $temp_2[0]['quantity'] = 0;
                }
            }

            if ($config_data['type'] == 'substraction')
                $temp_3 = $temp_2[0]['quantity'] - $temp[$i]['quantity'];
            else
                $temp_3 = $temp_2[0]['quantity'] + $temp[$i]['quantity'];
            $temp_4 = array(
                'quantity' => $temp_3
            );

            if ($temp[$i]['option_id'] <= 0) {
                $this->db->update('sma_warehouses_products', $temp_4,'product_id = '.$temp[$i]['product_id'].' and warehouse_id = '.$config_data['warehouse_id']);
            }
            else {
                $this->db->update('sma_product_variants', $temp_4,'id = '.$temp[$i]['option_id']);            
                $this->db->update('sma_warehouses_products_variants', $temp_4,'option_id = '.$temp[$i]['option_id'].' and warehouse_id = '.$config_data['warehouse_id']);
            }

            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'warehouse_id' => $config_data['warehouse_id']
            );
            $this->site->api_update_product_quantity($config_data_2);             
        }
    }

    public function addTransfer($data = array(), $items = array())
    {
        $config_data = array(
            'type' => 'transfer',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['transfer_no'] = $temp['reference_no'];

        $status = $data['status'];        
        if ($this->db->insert('transfers', $data)) {
            $transfer_id = $this->db->insert_id();
            foreach ($items as $item) {
                $item['transfer_id'] = $transfer_id;
                $this->db->insert('transfer_items', $item);
            }

            if ($status == 'sent' || $status == 'completed') {
                $config_data = array(
                    'id' => $transfer_id,
                    'type' => 'substraction',
                    'warehouse_id' => $data['from_warehouse_id'],
                );
                $this->api_calculate_transfers($config_data);
                $config_data = array(
                    'id' => $transfer_id,
                    'type' => 'addition',
                    'warehouse_id' => $data['to_warehouse_id'],
                );
                $this->api_calculate_transfers($config_data);
            }

            $config_data = array(
                'type' => 'transfer',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            return true;
        }
        return false;
    }

    public function addTransfer_bk($data = array(), $items = array())
    {
        $status = $data['status'];
        if ($this->db->insert('transfers', $data)) {
            $transfer_id = $this->db->insert_id();
            if ($this->site->getReference('to') == $data['transfer_no']) {
                $this->site->updateReference('to');
            }
            foreach ($items as $item) {
                $item['transfer_id'] = $transfer_id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                if ($status == 'completed') {
                    $item['date'] = date('Y-m-d');
                    $item['warehouse_id'] = $data['to_warehouse_id'];
                    $item['status'] = 'received';
                    $this->db->insert('purchase_items', $item);
                } else {
                    $this->db->insert('transfer_items', $item);
                }

                if ($status == 'sent' || $status == 'completed') {
                    $this->syncTransderdItem($item['product_id'], $data['from_warehouse_id'], $item['quantity'], $item['option_id']);
                }
            }

            return true;
        }
        return false;
    }

    public function api_reverse_transfer($id) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_transfers"
            ,"id = ".$id
            ,"arr"
        );
        $config_data = array(
            'id' => $id,
            'type' => 'addition',
            'warehouse_id' => $temp[0]['from_warehouse_id'],
        );
        $this->api_calculate_transfers($config_data);

        $config_data = array(
            'id' => $id,
            'type' => 'substraction',
            'warehouse_id' => $temp[0]['to_warehouse_id'],
        );
        $this->api_calculate_transfers($config_data);                
    }

    public function updateTransfer($id, $data = array(), $items = array())
    {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_transfers"
            ,"id = ".$id
            ,"arr"
        );
        if ($temp[0]['status'] == 'sent' || $temp[0]['status'] == 'completed')
            $this->api_reverse_transfer($id);

        $status = $data['status'];
        if ($this->db->update('transfers', $data, array('id' => $id))) {
            $this->db->delete('transfer_items', array('transfer_id' => $id));
            foreach ($items as $item) {
                $item['transfer_id'] = $id;
                $this->db->insert('transfer_items', $item);
            }

            if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                $config_data = array(
                    'id' => $id,
                    'type' => 'substraction',
                    'warehouse_id' => $temp[0]['from_warehouse_id'],
                );
                $this->api_calculate_transfers($config_data);

                $config_data = array(
                    'id' => $id,
                    'type' => 'addition',
                    'warehouse_id' => $data['to_warehouse_id'],
                );
                $this->api_calculate_transfers($config_data);                
            }
            return true;
        }

        return false;
    }

    public function updateTransfer_bk($id, $data = array(), $items = array())
    {
        $ostatus = $this->resetTransferActions($id);
        $status = $data['status'];

        if ($this->db->update('transfers', $data, array('id' => $id))) {
            $tbl = $ostatus == 'completed' ? 'purchase_items' : 'transfer_items';
            $this->db->delete($tbl, array('transfer_id' => $id));

            foreach ($items as $item) {
                $item['transfer_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                if ($status == 'completed') {
                    $item['date'] = date('Y-m-d');
                    $item['warehouse_id'] = $data['to_warehouse_id'];
                    $item['status'] = 'received';
                    $this->db->insert('purchase_items', $item);
                } else {
                    $this->db->insert('transfer_items', $item);
                }

                if ($data['status'] == 'sent' || $data['status'] == 'completed') {
                    $this->syncTransderdItem($item['product_id'], $data['from_warehouse_id'], $item['quantity'], $item['option_id']);
                }

            }

            return true;
        }

        return false;
    }

    public function updateStatus($id, $status, $note)
    {
        $ostatus = $this->resetTransferActions($id);
        $transfer = $this->getTransferByID($id);
        $items = $this->getAllTransferItems($id, $transfer->status);

        if ($this->db->update('transfers', array('status' => $status, 'note' => $note), array('id' => $id))) {
            $tbl = $ostatus == 'completed' ? 'purchase_items' : 'transfer_items';
            $this->db->delete($tbl, array('transfer_id' => $id));

            foreach ($items as $item) {
                $item = (array) $item;
                $item['transfer_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                unset($item['id'], $item['variant'], $item['unit'], $item['hsn_code'], $item['second_name']);
                if ($status == 'completed') {
                    $item['date'] = date('Y-m-d');
                    $item['warehouse_id'] = $transfer->to_warehouse_id;
                    $item['status'] = 'received';
                    $this->db->insert('purchase_items', $item);
                } else {
                    $this->db->insert('transfer_items', $item);
                }

                if ($status == 'sent' || $status == 'completed') {
                    $this->syncTransderdItem($item['product_id'], $transfer->from_warehouse_id, $item['quantity'], $item['option_id']);
                } else {
                    $this->site->syncQuantity(NULL, NULL, NULL, $item['product_id']);
                }
            }
            return true;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return true;
        }

        return FALSE;
    }

    public function getProductQuantity($product_id, $warehouse = DEFAULT_WAREHOUSE)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity)
    {
        if ($this->db->update('warehouses_products', array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function getProductByCode($code)
    {

        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getProductByName($name)
    {

        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTransferByID($id)
    {

        $q = $this->db->get_where('transfers', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }
    public function getAllTransferItems($transfer_id, $status)
    {
        $this->db->select('transfer_items.*, product_variants.name as variant, products.unit, products.hsn_code as hsn_code, products.second_name as second_name')
            ->from('transfer_items')
            ->join('products', 'products.id=transfer_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=transfer_items.option_id', 'left')
            ->group_by('transfer_items.id')
            ->where('transfer_id', $transfer_id);
            
        $q = $this->db->get();
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function getAllTransferItems_bk($transfer_id, $status)
    {
        if ($status == 'completed') {
            $this->db->select('purchase_items.*, product_variants.name as variant, products.unit, products.hsn_code as hsn_code, products.second_name as second_name')
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
                ->group_by('purchase_items.id')
                ->where('transfer_id', $transfer_id);
        } else {
            $this->db->select('transfer_items.*, product_variants.name as variant, products.unit, products.hsn_code as hsn_code, products.second_name as second_name')
                ->from('transfer_items')
                ->join('products', 'products.id=transfer_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=transfer_items.option_id', 'left')
                ->group_by('transfer_items.id')
                ->where('transfer_id', $transfer_id);
        }
        $q = $this->db->get();
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getWarehouseProduct($warehouse_id, $product_id, $variant_id)
    {
        if ($variant_id) {
            return $this->getProductWarehouseOptionQty($variant_id, $warehouse_id);
        } else {
            return $this->getWarehouseProductQuantity($warehouse_id, $product_id);
        }
        return FALSE;
    }

    public function getWarehouseProduct_v2($warehouse_id, $product_id, $variant_id)
    {
        if ($variant_id) {
            $temp = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_warehouses_products_variants"
                ,"option_id = ".$variant_id." and product_id = ".$product_id." and warehouse_id = ".$warehouse_id
                ,"arr"
            );
            if (count($temp) <= 0)
                $temp[0]['quantity'] = 0;
            return $temp[0]['quantity'];
        } else {
            $config_data = array(
                'table_name' => 'sma_products',
                'select_table' => 'sma_products',
                'translate' => '',
                'select_condition' => "id = ".$product_id,
            );
            $temp = $this->site->api_select_data_v2($config_data);
            if ($warehouse_id == 1)
                $temp2 = $temp[0]['quantity'];
            else
                $temp2 = $temp[0]['quantity_'.$warehouse_id];

            if (count($temp) <= 0)
                $temp2 = 0;
            return $temp2;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function resetTransferActions($id, $delete = NULL)
    {
        $otransfer = $this->getTransferByID($id);
        $oitems = $this->getAllTransferItems($id, $otransfer->status);
        $ostatus = $otransfer->status;
        if ($ostatus == 'sent' || $ostatus == 'completed') {
            foreach ($oitems as $item) {
                $option_id = (isset($item->option_id) && ! empty($item->option_id)) ? $item->option_id : NULL;
                $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item->product_id, 'warehouse_id' => $otransfer->from_warehouse_id, 'option_id' => $option_id);
                $this->site->setPurchaseItem($clause, $item->quantity);
                if ($delete) {
                    $option_id = (isset($item->option_id) && ! empty($item->option_id)) ? $item->option_id : NULL;
                    $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item->product_id, 'warehouse_id' => $otransfer->to_warehouse_id, 'option_id' => $option_id);
                    $this->site->setPurchaseItem($clause, ($item->quantity_balance - $item->quantity));
                }
            }
        }
        return $ostatus;
    }

    public function deleteTransfer($id)
    {
        $temp = $this->site->api_select_some_fields_with_where("
            *
            "
            ,"sma_transfers"
            ,"id = ".$id
            ,"arr"
        );
        if ($temp[0]['status'] == 'sent' || $temp[0]['status'] == 'completed') {
            $config_data = array(
                'id' => $id,
                'type' => 'addition',
                'warehouse_id' => $temp[0]['from_warehouse_id'],
            );
            $this->api_calculate_transfers($config_data);
            $config_data = array(
                'id' => $id,
                'type' => 'substraction',
                'warehouse_id' => $temp[0]['to_warehouse_id'],
            );
            $this->api_calculate_transfers($config_data);
        }

        if ($this->db->delete('transfers', array('id' => $id)) && $this->db->delete('transfer_items', array('transfer_id' => $id))) {

            return true;
        }
        return FALSE;
    }
    public function deleteTransfer_bk($id)
    {
        $ostatus = $this->resetTransferActions($id, 1);
        $oitems = $this->getAllTransferItems($id, $ostatus);
        $tbl = $ostatus == 'completed' ? 'purchase_items' : 'transfer_items';
        if ($this->db->delete('transfers', array('id' => $id)) && $this->db->delete($tbl, array('transfer_id' => $id))) {
            foreach ($oitems as $item) {
                $this->site->syncQuantity(NULL, NULL, NULL, $item->product_id);
            }
            return true;
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $zero_check = TRUE)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.cost as cost, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->group_by('product_variants.id');
        if ($zero_check) {
            $this->db->where('warehouses_products_variants.quantity >', 0);
        }
        $q = $this->db->get('product_variants');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncTransderdItem($product_id, $warehouse_id, $quantity, $option_id = NULL)
    {
        if ($pis = $this->site->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $balance_qty = $quantity;
            foreach ($pis as $pi) {
                if ($balance_qty <= $quantity && $quantity > 0) {
                    if ($pi->quantity_balance >= $quantity) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $this->db->update('purchase_items', array('quantity_balance' => $balance_qty), array('id' => $pi->id));
                        $quantity = 0;
                    } elseif ($quantity > 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $this->db->update('purchase_items', array('quantity_balance' => 0), array('id' => $pi->id));
                    }
                }
                if ($quantity == 0) { break; }
            }
        } else {
            $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
            $this->site->setPurchaseItem($clause, (0-$quantity));
        }
        $this->site->syncQuantity(NULL, NULL, NULL, $product_id);
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}

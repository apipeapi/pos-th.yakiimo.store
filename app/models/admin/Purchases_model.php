<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $limit = 20)
    {
        $this->db->where("type = 'standard' AND (name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR supplier1_part_no LIKE '%" . $term . "%' OR supplier2_part_no LIKE '%" . $term . "%' OR supplier3_part_no LIKE '%" . $term . "%' OR supplier4_part_no LIKE '%" . $term . "%' OR supplier5_part_no LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProducts()
    {
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductsByCode($code)
    {
        $this->db->select('*')->from('products')->like('code', $code, 'both');
        $q = $this->db->get();
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllPurchases()
    {
        $q = $this->db->get('purchases');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllPurchaseItems($purchase_id)
    {
        $this->db->select('purchase_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
            ->group_by('purchase_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseByID($id)
    {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function resetProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getOverSoldCosting($product_id)
    {
        $q = $this->db->get_where('costing', array('overselling' => 1));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPurchase($data, $items)
    {
        $config_data = array(
            'type' => 'purchase',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference_no'] = $temp['reference_no'];

        if ($this->db->insert('purchases', $data)) {
            $purchase_id = $this->db->insert_id();
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                $this->db->insert('purchase_items', $item);
                if ($this->Settings->update_cost) {
                    $this->db->update('products', array('cost' => $item['real_unit_cost']), array('id' => $item['product_id']));
                }
                if($item['option_id']) {
                    $this->db->update('product_variants', array('cost' => $item['real_unit_cost']), array('id' => $item['option_id'], 'product_id' => $item['product_id']));
                }
                if ($data['status'] == 'received') {
                    //$this->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['real_unit_cost']));
                    $config_data = array(
                        'product_id' => $item['product_id'],
                        'type' => 'addition',
                        'quantity' => $item['quantity'],
                        'warehouse_id' => $item['warehouse_id'],
                        'option_id' => $item['option_id'],
                    );
                    $this->site->api_update_quantity_stock($config_data);                    
                }
            }

            if ($data['status'] == 'returned') {
                //$this->db->update('purchases', array('return_purchase_ref' => $data['return_purchase_ref'], 'surcharge' => $data['surcharge'],'return_purchase_total' => $data['grand_total'], 'return_id' => $purchase_id), array('id' => $data['purchase_id']));
            }

            if ($data['status'] == 'received' || $data['status'] == 'returned') {
                //$this->site->syncQuantity(NULL, $purchase_id);                
            }
            $config_data = array(
                'type' => 'purchase',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);

            return true;
        }
        return false;
    }

    public function api_reversePurchase($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_purchase_items"
            ,"purchase_id = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'subtraction',
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $config_data['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }
    }

    public function updatePurchase($id, $data, $items = array())
    {

        $config_data = array(
            'table_name' => 'sma_purchases',
            'select_table' => 'sma_purchases',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $select_data = $this->site->api_select_data_v2($config_data);

        if ($select_data[0]['status'] == 'received') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $data['warehouse_id'],
            );
            $this->api_reversePurchase($config_data_2);
        }

        if ($this->db->update('purchases', $data, array('id' => $id)) && $this->db->delete('purchase_items', array('purchase_id' => $id))) {
            $purchase_id = $id;
            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                $this->db->insert('purchase_items', $item);
            }
            //$this->site->syncQuantity(NULL, NULL, $oitems);
            if ($data['status'] == 'received' || $data['status'] == 'partial') {
                /*
                $this->site->syncQuantity(NULL, $id);
                foreach ($oitems as $oitem) {
                    $this->updateAVCO(array('product_id' => $oitem->product_id, 'warehouse_id' => $oitem->warehouse_id, 'quantity' => (0-$oitem->quantity), 'cost' => $oitem->real_unit_cost));
                }
                */
            }
            $this->site->syncPurchasePayments($id);
            $return = true;
        }
        else
            $return = false;

        if ($data['status'] == 'received') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $data['warehouse_id'],
                'type' => 'addition',
            );
            $this->api_update_stock_purchase($config_data_2);
        }

        return $return;
    }

    public function api_update_stock_purchase($config_data) {
        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_purchase_items"
            ,"purchase_id = ".$config_data['id']." and warehouse_id = ".$config_data['warehouse_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => $config_data['type'],
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $config_data['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }
    }

    public function updateStatus($id, $status, $note)
    {
        $purchase = $this->getPurchaseByID($id);
        $items = $this->site->getAllPurchaseItems($id);

        if ($this->db->update('purchases', array('status' => $status, 'note' => $note), array('id' => $id))) {
            if (($purchase->status != 'received' || $purchase->status != 'partial') && ($status == 'received' || $status == 'partial')) {
                foreach ($items as $item) {
                    $qb = $status == 'received' ? ($item->quantity_balance + ($item->quantity - $item->quantity_received)) : $item->quantity_balance;
                    $qr = $status == 'received' ? $item->quantity : $item->quantity_received;
                    $this->db->update('purchase_items', array('status' => $status, 'quantity_balance' => $qb, 'quantity_received' => $qr), array('id' => $item->id));
                    $this->updateAVCO(array('product_id' => $item->product_id, 'warehouse_id' => $item->warehouse_id, 'quantity' => $item->quantity, 'cost' => $item->real_unit_cost));
                }
                $this->site->syncQuantity(NULL, NULL, $items);
            } else if (($purchase->status == 'received' || $purchase->status == 'partial') && ($status == 'ordered' || $status == 'pending') ) {
                foreach ($items as $item) {
                    $qb = 0;
                    $qr = 0;
                    $this->db->update('purchase_items', array('status' => $status, 'quantity_balance' => $qb, 'quantity_received' => $qr), array('id' => $item->id));
                    $this->updateAVCO(array('product_id' => $item->product_id, 'warehouse_id' => $item->warehouse_id, 'quantity' => $item->quantity, 'cost' => $item->real_unit_cost));
                }
                $this->site->syncQuantity(NULL, NULL, $items);
            }
            return true;
        }
        return false;
    }

    public function deletePurchase($id)
    {
        $config_data = array(
            'table_name' => 'sma_purchases',
            'select_table' => 'sma_purchases',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $purchase = $this->site->api_select_data_v2($config_data);

        $temp_return = $this->site->api_select_some_fields_with_where("id
            "
            ,"sma_returns"
            ,"add_ons like '%:purchase_id:{".$id."}:%'"
            ,"arr"
        );
        $return['error'] = '';
        if (is_array($temp_return)) if (count($temp_return) > 0) {
            $return['error'] = lang('Can_not_delete_any_Consignment_Purchase_with_a_return_record.');
            return $return;
        }

        $temp = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_consignment_purchase_items"
            ,"purchase_id = ".$id." limit 1"
            ,"arr"
        );    
        if (count($temp) > 0) {
            $return['error'] = lang('Can_not_delete_any_Consignment_Purchase_with_a_sale_record.');
            return $return;            
        }


        $purchase_items = $this->site->getAllPurchaseItems($id);

        if ($purchase[0]['status'] == 'received') {
            $config_data_2 = array(
                'id' => $id,
                'warehouse_id' => $purchase[0]['warehouse_id'],
            );
            $this->api_reversePurchase($config_data_2);                
        }

        if ($this->db->delete('purchase_items', array('purchase_id' => $id)) && $this->db->delete('purchases', array('id' => $id))) {
            $this->db->delete('payments', array('purchase_id' => $id));
            if ($purchase[0]['status'] == 'received' || $purchase[0]['status'] == 'partial') {
                foreach ($purchase_items as $oitem) {

                    //$this->updateAVCO(array('product_id' => $oitem->product_id, 'warehouse_id' => $oitem->warehouse_id, 'quantity' => (0-$oitem->quantity), 'cost' => $oitem->real_unit_cost));

                    $received = $oitem->quantity_received ? $oitem->quantity_received : $oitem->quantity;
                    if ($oitem->quantity_balance < $received) {
                        //$clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $oitem->product_id, 'warehouse_id' => $oitem->warehouse_id, 'option_id' => $oitem->option_id);

                        //$this->site->setPurchaseItem($clause, ($oitem->quantity_balance - $received));
                    }                    
                }
            }
            //$this->site->syncQuantity(NULL, NULL, $purchase_items);

            $this->db->delete('consignment_purchase_items', array('purchase_id' => $id));
        }
        return $return;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id)
    {
        $q = $this->db->get_where('payments', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getPaymentsForPurchase($purchase_id)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array())
    {
        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('ppay') == $data['reference_no']) {
                $this->site->updateReference('ppay');
            }
            $this->site->syncPurchasePayments($data['purchase_id']);
            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array())
    {
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncPurchasePayments($data['purchase_id']);
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncPurchasePayments($opay->purchase_id);
            return true;
        }
        return FALSE;
    }

    public function getProductOptions($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseByID($id)
    {
        $q = $this->db->get_where('expenses', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addExpense($data = array())
    {
        $config_data = array(
            'type' => 'expense',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference'] = $temp['reference_no'];
        if ($this->db->insert('expenses', $data)) {
            $config_data = array(
                'type' => 'expense',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);
            return true;
        }

        return false;
    }

    public function updateExpense($id, $data = array())
    {
        if ($this->db->update('expenses', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteExpense($id)
    {
        if ($this->db->delete('expenses', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('return_purchases', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllReturnItems($return_id)
    {
        $this->db->select('return_purchase_items.*, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=return_purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_purchase_items.option_id', 'left')
            ->group_by('return_purchase_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('return_purchase_items', array('return_id' => $return_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPurcahseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function returnPurchase($data = array(), $items = array())
    {

        $purchase_items = $this->site->getAllPurchaseItems($data['purchase_id']);

        if ($this->db->insert('return_purchases', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('rep') == $data['reference_no']) {
                $this->site->updateReference('rep');
            }
            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_purchase_items', $item);

                if ($purchase_item = $this->getPurcahseItemByID($item['purchase_item_id'])) {
                    if ($purchase_item->quantity == $item['quantity']) {
                        $this->db->delete('purchase_items', array('id' => $item['purchase_item_id']));
                    } else {
                        $nqty = $purchase_item->quantity - $item['quantity'];
                        $bqty = $purchase_item->quantity_balance - $item['quantity'];
                        $rqty = $purchase_item->quantity_received - $item['quantity'];
                        $tax = $purchase_item->unit_cost - $purchase_item->net_unit_cost;
                        $discount = $purchase_item->item_discount / $purchase_item->quantity;
                        $item_tax = $tax * $nqty;
                        $item_discount = $discount * $nqty;
                        $subtotal = $purchase_item->unit_cost * $nqty;
                        $this->db->update('purchase_items', array('quantity' => $nqty, 'quantity_balance' => $bqty, 'quantity_received' => $rqty, 'item_tax' => $item_tax, 'item_discount' => $item_discount, 'subtotal' => $subtotal), array('id' => $item['purchase_item_id']));
                    }

                }
            }
            $this->calculatePurchaseTotals($data['purchase_id'], $return_id, $data['surcharge']);
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            $this->site->syncQuantity(NULL, $data['purchase_id']);
            return true;
        }
        return false;
    }

    public function calculatePurchaseTotals($id, $return_id, $surcharge)
    {
        $purchase = $this->getPurchaseByID($id);
        $items = $this->getAllPurchaseItems($id);
        if (!empty($items)) {
            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            foreach ($items as $item) {
                $product_tax += $item->item_tax;
                $product_discount += $item->item_discount;
                $total += $item->net_unit_cost * $item->quantity;
            }
            if ($purchase->order_discount_id) {
                $percentage = '%';
                $order_discount_id = $purchase->order_discount_id;
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = (($total + $product_tax) * (Float)($ods[0])) / 100;
                } else {
                    $order_discount = $order_discount_id;
                }
            }
            if ($purchase->order_tax_id) {
                $order_tax_id = $purchase->order_tax_id;
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $order_tax_details->rate;
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = (($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100;
                    }
                }
            }
            $total_discount = $order_discount + $product_discount;
            $total_tax = $product_tax + $order_tax;
            $grand_total = $total + $total_tax + $purchase->shipping - $order_discount + $surcharge;
            $data = array(
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'return_id' => $return_id,
                'surcharge' => $surcharge
            );

            if ($this->db->update('purchases', $data, array('id' => $id))) {
                return true;
            }
        } else {
            $this->db->delete('purchases', array('id' => $id));
        }
        return FALSE;
    }

    public function getExpenseCategories()
    {
        $q = $this->db->get('expense_categories');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getExpenseCategoryByID($id)
    {
        $q = $this->db->get_where("expense_categories", array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateAVCO($data)
    {
        if ($wp_details = $this->getWarehouseProductQuantity($data['warehouse_id'], $data['product_id'])) {
            $total_cost = (($wp_details->quantity * $wp_details->avg_cost) + ($data['quantity'] * $data['cost']));
            $total_quantity = $wp_details->quantity + $data['quantity'];
            if (!empty($total_quantity)) {
                $avg_cost = ($total_cost / $total_quantity);
                $this->db->update('warehouses_products', array('avg_cost' => $avg_cost), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']));
            }
        } else {
            $this->db->insert('warehouses_products', array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id'], 'avg_cost' => $data['cost'], 'quantity' => 0));
        }
    }
	public function getExpenses($config_data) {
        $condition = '';
        if ($config_data['search'] != '') {
            $condition .= "and t1.reference LIKE '%". $config_data['search'] ."%'";
        }

        if ($_GET['category_id'] > 0) {
            $condition .= "and t1.category_id = ".$_GET['category_id'];
        }

        $order_by = '';
        if ($config_data['sort_by'] != '')
            $order_by .= 'order by '.$config_data['sort_by'].' '.$config_data['sort_order'];
        else
            $order_by .= 'order by t1.id Desc';

        if ($limit != '') {
            $this->db->limit($limit, $offset);
        }

        if ($config_data['limit'] != '')
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        
        if ($config_data['start_date'] != '' && $config_data['end_date'] != '')
            $condition .= " and convert(t1.date, Date) between STR_TO_DATE('".$config_data['start_date']."', '%d/%m/%Y') and STR_TO_DATE('".$config_data['end_date']."', '%d/%m/%Y')";

        if ($this->session->userdata('group_id') != 1 && $this->session->userdata('group_id') != 2 && !$this->session->userdata('view_right')) {
            $condition .= "and t1.created_by = ".$this->session->userdata('user_id');
        }

        $select_data = $this->site->api_select_some_fields_with_where("t1.*,t3.name as category,
        getTranslate(add_ons,'status','".f_separate."','".v_separate."') as status"
            ,"sma_expenses as t1 inner join sma_expense_categories as t3 on t3.id = t1.category_id"
            ,"t1.id > 0 ".$condition." ".$order_by
            ,"arr"
        );

        return $select_data;
    }

    public function api_get_error_consignment_purchase_items($config_data)
    {
        $temp_products = explode('-', $config_data['products']);
        $select_data = $this->site->api_select_some_fields_with_where("
            id, supplier
            "
            ,"sma_purchases"
            ,"getTranslate(add_ons,'cs_reference_no','".f_separate."','".v_separate."') != '' and supplier_id != ".$config_data['supplier_id']
            ,"arr"
        );
        $return['error'] = '';     
        if ($select_data[0]['id'] > 0) {
            $temp_3 = '';
            for ($i3=0;$i3<count($select_data);$i3++) {
                $temp = $this->site->api_select_some_fields_with_where("
                    id, product_id, product_name     
                    "
                    ,"sma_purchase_items"
                    ,"purchase_id = ".$select_data[$i3]['id']
                    ,"arr"
                );                
                if ($temp[0]['id'] > 0) {
                    for ($i=0;$i<count($temp);$i++) {

                        $b = 0;
                        for ($i2=0;$i2<count($temp_products);$i2++) {
                            if ($temp_products[$i2] != '') {
                                if ($temp_products[$i2] == $temp[$i]['product_id']) {
                                    $b = 1;
                                    break;
                                }
                            }
                        }
                        if ($b == 1)
                            $temp_3 .= '-'.$temp[$i]['product_name'].' (Supplier: '.$select_data[$i3]['supplier'].')';
                    }
                }
            }
            $temp_4 = explode('-', $temp_3);
            $temp_4 = array_unique($temp_4);
            $temp_5 = '';
            foreach ($temp_4 as $value){
                if ($value != '')
                    $temp_5 .= '<br>'.$value;
            }

            if ($temp_5 != '') {
                $return['error'] = 'Products: '.$temp_5;
                $return['error'] .= ' <br>are already added.';
            }
        }
        return $return;
    }
    public function api_generate_return_purchase($id,$post) {
        $config_data = array(
            'table_name' => 'sma_purchases',
            'select_table' => 'sma_purchases',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $select_purchase = $this->site->api_select_data_v2($config_data);        

        $temp_3 = $this->site->api_select_some_fields_with_where("
            COLUMN_NAME
            "
            ,"INFORMATION_SCHEMA.COLUMNS"
            ,"table_name = 'sma_returns'"
            ,"arr"
        );
        $temp_4 = $this->site->api_select_some_fields_with_where("
            COLUMN_NAME
            "
            ,"INFORMATION_SCHEMA.COLUMNS"
            ,"table_name = 'sma_return_items'"
            ,"arr"
        );

        $data = array();
        foreach(array_keys($select_purchase[0]) as $key) {
            for ($j=0;$j<count($temp_3);$j++) {
                if ($temp_3[$j]['COLUMN_NAME'] == $key) {
                    $data[$key] = $select_purchase[0][$key];
                    break;
                }
            }
        }
        unset($data['id']);

        $data['reference_no'] = $this->site->getReference('re');
        $data['customer_id'] = $select_purchase[0]['supplier_id'];
        $data['customer'] = $select_purchase[0]['supplier'];
        $data['biller_id'] = 3;
        $data['biller'] = 'DTC ORDER  SYSTEM';
        $data['created_by'] = $this->session->userdata('user_id');
        $data['date'] = date('Y-m-d H:i:s');
        $data['add_ons'] = "initial_first_add_ons:{}:purchase_id:{".$select_purchase[0]['id']."}:";

        $this->db->insert('sma_returns', $data);
        $this->site->updateReference('re');


        $temp_id = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_returns"
            ,"id > 0 order by id desc limit 1"
            ,"arr"
        );
        $return_id = $temp_id[0]['id'];

        $grand_total = 0;
        foreach($post as $name => $value) {
            if (is_int(strpos($name,"return_quantity_"))) {
                $temp_purchase_item_id = str_replace('return_quantity_','',$name);

                $temp = $this->site->api_select_some_fields_with_where("
                    *
                    "
                    ,"sma_purchase_items"
                    ,"id = ".$temp_purchase_item_id
                    ,"arr"
                );     
                $data = array();
                foreach(array_keys($temp[0]) as $key) {
                    for ($j=0;$j<count($temp_4);$j++) {
                        if ($temp_4[$j]['COLUMN_NAME'] == $key) {
                            $data[$key] = $temp[0][$key];
                            break;
                        }
                    }
                }
                unset($data['id']);
                $data['return_id'] = $return_id;
                $data['quantity'] = $value;
                $data['unit_quantity'] = $value;
                $data['unit_price'] = $temp[0]['unit_cost'];
                $data['subtotal'] = $temp[0]['unit_cost'] * $value;
                $grand_total = $grand_total + $data['subtotal'];
                if ($value > 0) {
                    $this->db->insert('sma_return_items', $data);

                    $config_data_2 = array(
                        'product_id' => $data['product_id'],
                        'type' => 'subtraction',
                        'quantity' => $data['quantity'],
                        'warehouse_id' => $data['warehouse_id'],
                        'option_id' => $data['option_id'],
                    );
                    $this->site->api_update_quantity_stock($config_data_2);
                }
            }
        }
        $temp = array(
            'total' => $grand_total,
            'grand_total' => $grand_total,
        );
        $this->db->update('sma_returns', $temp,'id = '.$return_id);                    

    }

    public function api_get_data($config_data) {
        $condition = '';
        $config_data['search'] = trim($config_data['search']);

        if ($_GET['adjustment_purchase_track'] != '') {
            $temp = explode('-',$_GET['adjustment_purchase_track']);
            $condition = " and (t1.id = ".$temp[1];
            for ($i=2;$i<count($temp);$i++) {
                $condition .= " or t1.id = ".$temp[$i];
            }
            $condition .= ')';       
        }
        elseif ($_GET['adjustment_consignment_purchase_track'] != '') {
            $temp = explode('-',$_GET['adjustment_consignment_purchase_track']);
            $condition = " and (t1.id = ".$temp[1];
            for ($i=2;$i<count($temp);$i++) {
                $condition .= " or t1.id = ".$temp[$i];
            }
            $condition .= ')';
        }
        elseif ($config_data['search'] != '') {
            $condition .= "
                and (t1.reference_no LIKE '%". $config_data['search'] ."%'
                OR t1.supplier LIKE '%". $config_data['search'] ."%'
                OR t1.status LIKE '%". $config_data['search'] ."%'
                OR t1.payment_status LIKE '%". $config_data['search'] ."%'
            ";
            if (is_numeric($config_data['search']))
                $condition .= ' OR t1.'.$config_data['table_id']." = ".$config_data['search'];
            $condition .= ')';
        }

        if ($_GET['adjustment_purchase_track'] == '' && $_GET['adjustment_consignment_purchase_track'] == '') {
            if ($_GET['mode'] != 'consignment')
                $condition .= " and getTranslate(t1.add_ons,'consignment_status','".f_separate."','".v_separate."') = ''";
            else
                $condition .= " and getTranslate(t1.add_ons,'consignment_status','".f_separate."','".v_separate."') != ''";
        }

        $order_by = '';
        if ($config_data['sort_by'] != '') {
            $order_by = 'order by '.$config_data['sort_by'];
            /*
            if ($config_data['sort_by'] == '')
                $order_by = 'order by delivery_date';
            */
            $order_by .= ' '.$config_data['sort_order'];
        }
        else
            $order_by = 'order by t1.date desc, t1.id desc';

        $temp_field = $this->site->api_get_condition_add_ons_field("t1.add_ons","sma_add_ons","table_name = '".$config_data['table_name']."'");

        if ($config_data['limit'] != '') {
            $temp_select = "t1.*".$temp_field;
            $order_by .= ' limit '.$config_data['offset_no'].', '.$config_data['limit'];
        }
        else
            $temp_select = "t1.id";

        $select_data = $this->site->api_select_some_fields_with_where(
            $temp_select
            ,$config_data['table_name']." as t1"
            ,"t1.".$config_data['table_id']." > 0 ".$condition." ".$order_by
            ,"arr"
        );
        return $select_data;
    }

}





<?php defined('BASEPATH') or exit('No direct script access allowed');

class Returns_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->where("(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('returns', ['id' => $id], 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturnItems($return_id)
    {
        $this->db->select('return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=return_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=return_items.tax_rate_id', 'left')
            ->where('return_id', $return_id)
            ->group_by('return_items.id')
            ->order_by('id', 'asc');

        $q = $this->db->get('return_items');
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function addReturn($data = array(), $items = array(), $sale_id)
    {
        $config_data = array(
            'type' => 'return',
            'date' => $data['date'],
            'update' => 0,
        );
        $temp = $this->site->api_calculate_reference_no($config_data);
        $data['reference_no'] = $temp['reference_no'];

        if ($this->db->insert('returns', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('re') == $data['reference_no']) {
                $this->site->updateReference('re');
            }

            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    //$this->site->setPurchaseItem($clause, $item['quantity']);
                    //$this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        //$this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        //$this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }

                $config_data_3 = array(
                    'sale_id' => $sale_id,
                    'return_quantity' => $item['quantity'],
                    'product_id' => $item['product_id'],
                );
                $this->api_reduce_stock_from_sale_return($config_data_3);

            }

            $config_data = array(
                'type' => 'return',
                'date' => $data['date'],
                'update' => 1,
            );
            $temp = $this->site->api_calculate_reference_no($config_data);
            $data['reference_no'] = $temp['reference_no'];

            return true;
        }

        return false;
    }

    public function api_reduce_stock_from_sale_return($config_data) {
        $temp2 = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_sales"
            ,"id = '".$config_data['sale_id']."'"
            ,"arr"
        );

        $temp = $this->site->api_select_some_fields_with_where("
            *     
            "
            ,"sma_sale_items"
            ,"sale_id = ".$temp2[0]['id']." and product_id = ".$config_data['product_id']
            ,"arr"
        );
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data_2 = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'addition',
                'quantity' => $config_data['return_quantity'],
                'warehouse_id' => $temp[$i]['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data_2);
        }        
    }

    public function updateReturn($id, $data = array(), $items = array())
    {
        //$this->resetSaleActions($id);

        if ($this->db->update('returns', $data, array('id' => $id)) && $this->db->delete('return_items', array('return_id' => $id))) {
            // $return_id = $id;
            foreach ($items as $item) {
                // $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            return true;
        }

        return false;
    }

    public function resetSaleActions($id)
    {
        if ($items = $this->getReturnItems($id)) {
            foreach ($items as $item) {
                if ($item->product_type == 'standard') {
                    $clause = ['product_id' => $item->product_id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item->option_id];
                    $this->site->setPurchaseItem($clause, (0-$item->quantity));
                    $this->site->syncQuantity(null, null, null, $item->product_id);
                } elseif ($item->product_type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item->product_id);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, (0-($combo_item->qty*$item->quantity)));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
        }
    }

    public function deleteReturn($id)
    {
        //$this->resetSaleActions($id);
        $config_data = array(
            'table_name' => 'sma_return_items',
            'select_table' => 'sma_return_items',
            'select_condition' => "return_id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        if (is_array($temp)) if (count($temp) > 0)
        for ($i=0;$i<count($temp);$i++) {
            $config_data = array(
                'product_id' => $temp[$i]['product_id'],
                'type' => 'subtraction',
                'quantity' => $temp[$i]['quantity'],
                'warehouse_id' => $temp[$i]['warehouse_id'],
                'option_id' => $temp[$i]['option_id'],
            );
            $this->site->api_update_quantity_stock($config_data);
        }

        $this->load->admin_model('sales_model');

        $config_data = array(
            'table_name' => 'sma_returns',
            'select_table' => 'sma_returns',
            'translate' => '',
            'select_condition' => "id = ".$id,
        );
        $temp = $this->site->api_select_data_v2($config_data);
        
        $return = false;
        if ($this->db->delete('return_items', array('return_id' => $id)) && $this->db->delete('returns', array('id' => $id))) {
            $return = true;
        }

        if ($temp[0]['sale_id'] > 0) {
            $this->sales_model->api_reverse_consignment_purchase($temp[0]['sale_id']);
            $this->sales_model->api_calculate_consignment_purchase_wrapper($temp[0]['sale_id']);
        }
        if ($temp[0]['cs_reference_no'] != '') {
            $temp5 = $this->site->api_select_some_fields_with_where("
                *
                "
                ,"sma_consignment"
                ,"reference_no = '".$temp[0]['cs_reference_no']."'"
                ,"arr"
            );
            $temp6 = array(
                'sale_status' => 'pending'
            );
            $this->db->update('sma_consignment', $temp6,'id = '.$temp5[0]['id']);                  
        }


        return $return;
    }

    public function getProductOptions($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q != false && $q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q != false && $q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
}

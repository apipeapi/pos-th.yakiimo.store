var initAppendingDOM;
var inc = 0;
/*******************
 * Author: TEP Afril
 * 18th May, 2018
 * odadcambodia.com
********************/
$(document).ready(function () {
	var grand_total = Number(0);
	var paid = Number(0);
	var balance = Number(0);
	var arrIndex = [];

	var updateData = function(){
		$('#custom_g_t').html(numberWithCommas(grand_total));
		$('#custom_paid').html(numberWithCommas(paid));
		$('#custom_balance').html(numberWithCommas(balance));
	};

	numberWithCommas = (x) => {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	var sumData = function( gbl, vbl, sign ){
		gbl = Number(gbl);
		vbl = vbl.replace(/\$/g,'');
		vbl = vbl.replace(/\,/g,'');
		vbl = Number(vbl);

		switch(sign)
		{
			case "+":
				gbl += (Number(vbl));
			break;

			case "-":
				gbl -= (Number(vbl));
			break;
		}
		gbl = gbl.toFixed(2);
		return gbl;
	};

	var resetData = function(){
		grand_total = '0.00';
		paid = '0.00';
		balance = '0.00';
		updateData();
	};

	initAppendingDOM = function(gt,pd,bl,idx){
		domStr = '<div style="width:30vw;position:fixed;top:50px;right:280px;z-index:1000;opacity:100">'+
		'<a id="expand_sum_table" class="btn btn-success btn-sm" style="top:45px;right:30px;position:fixed;display:none"><i style="" class="fa fa-2x fa-arrows-alt"></i></a>'+
		'<div id="sum_table_container">'+
		'<a id="close_sum_table" class="btn btn-sm btn-danger" style="padding: 2px 5px;cursor:pointer;top:45px;right:270px;position:fixed;"><i class="fa fa-times"></i></a>'+
		'<table  class="table table-bordered table-hover table-striped dataTable">'+
		'<thead>'+
		'<tr>'+
		'<td style="background-color:#333;border-color:#000;">'+gt+'</td>'+
		'<td style="background-color:#333;border-color:#000;">'+pd+'</td>'+
		'<td style="background-color:#333;border-color:#000;">'+bl+'</td>'+
		'</tr>'+
		'</thead>'+
		'<tbody>'+
		'<tr style="font-weight:bold;">'+
		'<td id="custom_g_t" style="background-color:#d1d1d1;">0.00</td>'+
		'<td id="custom_paid" style="background-color:#d1d1d1;">0.00</td>'+
		'<td id="custom_balance" style="background-color:#d1d1d1;">0.00</td>'+
		'</tr>'+
		'</tbody>'+
		'</table>'+
		'</div>'+
		'</div>';
		$('.box').append(domStr);
		arrIndex = idx;
	};

	$('body').on('ifChecked','.icheckbox_square-blue',function(){
		inc++;

		var row = $(this).parent().parent().parent();
		var td_g_t = $(row[0]).children().eq(arrIndex[0]);
		var td_paid = $(row[0]).children().eq(arrIndex[1]);
		var td_balance = $(row[0]).children().eq(arrIndex[2]);


		var selected_g_t = $(td_g_t).text();
		var selected_paid = $(td_paid).text();
		var selected_balance = $(td_balance).text();

		var temp = grand_total;

		grand_total = sumData(grand_total,selected_g_t,'+');
		paid = sumData(paid,selected_paid,'+');
		balance = sumData(balance,selected_balance,'+');

		updateData();
	});

	$('body').on('ifUnchecked','.icheckbox_square-blue',function(){


		var row = $(this).parent().parent().parent();

		var td_g_t = $(row[0]).children().eq(arrIndex[0]);
		var td_paid = $(row[0]).children().eq(arrIndex[1]);
		var td_balance = $(row[0]).children().eq(arrIndex[2]);

		var selected_g_t = $(td_g_t).text();
		var selected_paid = $(td_paid).text();
		var selected_balance = $(td_balance).text();

		grand_total = sumData(grand_total,selected_g_t,'-');
		paid = sumData(paid,selected_paid,'-');
		balance = sumData(balance,selected_balance,'-');

		updateData();
	});

	$('body').on('click','#close_sum_table',function(){
		$('#sum_table_container').hide(300,function(){
			$('#expand_sum_table').show(300);
		});
		});
		$('body').on('click','#expand_sum_table',function(){
		$('#expand_sum_table').hide(300,function(){
			$('#sum_table_container').show(300);
		});
	});

	$('body').on('change', 'select[name="POData_length"]', function (e) {
		resetData();
	});
	$('body').on('click', '.pagination a', function (e) {
		resetData();
	});
});
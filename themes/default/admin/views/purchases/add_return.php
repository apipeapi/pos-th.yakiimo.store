<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $modal_title; ?></h4>
        </div>

        <div class="modal-body">
            <?php
                echo '<p><strong>'.lang('reference_no').': </strong>'.$select_purchase[0]['reference_no'].'</p>';
            ?>            

            <div class="table-responsive" id="api_get_table_sale_items">
                <?php

                    $attrib = array('role' => 'form', 'name' => 'frm_add_return', 'id' => 'frm_add_return');
                    echo admin_form_open_multipart("purchases/add_return/".$select_purchase[0]['id'], $attrib);
echo '
    <table class="table table-bordered table-hover table-striped print-table order-table">
        <thead>
        <tr>
            <th>'.lang("no.").'</th>
            <th>'.lang("description").'</th>
            <th>'.lang("unit_cost").'</th>
            <th>'.lang("quantity").'</th>
            <th>'.lang("Sold_Quantity").'</th>
            <th>'.lang("Remained_Quantity").'</th>
            <th>'.lang("Returned_Quantity").'</th>
            <th>'.lang("Return_Quantity").'</th>
        </tr>
        </thead>
        <tbody>
';

$temp_total = 0;
if ($select_purchase_item[0]['product_id'] > 0) {
    for ($i=0;$i<count($select_purchase_item);$i++) {
        $temp = $this->site->api_select_some_fields_with_where("
            code, name
            "
            ,"sma_products"
            ,"id = ".$select_purchase_item[$i]['product_id']
            ,"arr"
        );    
        echo '
            <tr>
            <td style="text-align:center; width:40px; vertical-align:middle;">
                '.($i + 1).'
            </td>
            <td style="vertical-align:middle;">
                '.$temp[0]['code'].' - '.$temp[0]['name'].'
            </td>
            <td style="text-align:right; width:100px;">
                '.$this->sma->formatMoney($select_purchase_item[$i]['unit_cost']).'
            </td>
            <td style="width: 80px; text-align:center; vertical-align:middle;">
                '.$this->sma->formatQuantity($select_purchase_item[$i]['quantity']).'
            </td>
            <td style="width: 80px; text-align:center; vertical-align:middle;">
                '.$this->sma->formatQuantity($select_purchase_item[$i]['sold_quantity']).'
            </td>
            <td style="width: 80px; text-align:center; vertical-align:middle;">
                '.$this->sma->formatQuantity($select_purchase_item[$i]['remained_quantity']).'
            </td>            
            <td style="width: 80px; text-align:center; vertical-align:middle;">
                '.$this->sma->formatQuantity($select_purchase_item[$i]['returned_quantity']).'
            </td>
            <td style="width: 80px; text-align:center; vertical-align:middle;">
        ';
        
        if ($select_purchase_item[$i]['remained_quantity'] <= 0) 
            $temp_disable = 'disabled="disabled"'; 
        else 
            $temp_disable = '';

        echo '
            <select class="form-control" name="return_quantity_'.$select_purchase_item[$i]['id'].'" id="return_quantity_'.$select_purchase_item[$i]['id'].'" '.$temp_disable.'>
        ';
        for ($i2=0;$i2<=$select_purchase_item[$i]['remained_quantity'];$i2++) {
            echo '
                <option value="'.$i2.'">'.$i2.'</option>
            ';
        }
        echo '
            </select>
        ';

        echo '
            </td>            
            </tr>
        ';
    }
}
else
    echo '
        <tr><td colspan="8">'.lang('No_record').'</td></tr>
    ';
echo '
        </tbody>
';


echo '
    </table>
';
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <input type="submit" name="frm_add_return" value="<?php echo lang('Add_Consignment_Purchase_Return'); ?>" onclick="document.frm_add_return.submit();" class="btn btn-primary">
        </div>
    </div>
    <?php echo form_close(); ?>
</div>


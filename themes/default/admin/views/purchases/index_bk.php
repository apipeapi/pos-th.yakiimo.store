<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $temp_get = '';
    $temp_class = '';
    if ($_GET['mode'] == 'consignment') {
        $temp_get = '?mode=consignment';
        $temp_class = 'consignment_';
    }
?>

<script>
    $(document).ready(function () {
        oTable = $('#POData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=admin_url('purchases/getPurchases'.($warehouse_id ? '/'.$warehouse_id : '').'?search='.$_GET['search'].'&mode='.$_GET['mode'])?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false,"mRender": checkbox}, {"mRender": fld}, null, null, null, {"mRender": row_status}, {"mRender": currencyFormat}, {"mRender": pay_status}, {"bSortable": false,"mRender": attachment}, {"bSortable": false}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "<?php echo $temp_class; ?>purchase_link";
                return nRow;
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var total = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    total += parseFloat(aaData[aiDisplay[i]][6]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[6].innerHTML = currencyFormat(total);
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('ref_no');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('purchase_status');?>]", filter_type: "text", data: []},
            {column_number: 7, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer");

        <?php if ($this->session->userdata('remove_pols')) {?>
        if (localStorage.getItem('poitems')) {
            localStorage.removeItem('poitems');
        }
        if (localStorage.getItem('podiscount')) {
            localStorage.removeItem('podiscount');
        }
        if (localStorage.getItem('potax2')) {
            localStorage.removeItem('potax2');
        }
        if (localStorage.getItem('poshipping')) {
            localStorage.removeItem('poshipping');
        }
        if (localStorage.getItem('poref')) {
            localStorage.removeItem('poref');
        }
        if (localStorage.getItem('powarehouse')) {
            localStorage.removeItem('powarehouse');
        }
        if (localStorage.getItem('ponote')) {
            localStorage.removeItem('ponote');
        }
        if (localStorage.getItem('posupplier')) {
            localStorage.removeItem('posupplier');
        }
        if (localStorage.getItem('pocurrency')) {
            localStorage.removeItem('pocurrency');
        }
        if (localStorage.getItem('poextras')) {
            localStorage.removeItem('poextras');
        }
        if (localStorage.getItem('podate')) {
            localStorage.removeItem('podate');
        }
        if (localStorage.getItem('postatus')) {
            localStorage.removeItem('postatus');
        }
        if (localStorage.getItem('popayment_term')) {
            localStorage.removeItem('popayment_term');
        }
        <?php $this->sma->unset_data('remove_pols');}
        ?>
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
	    echo admin_form_open('purchases/purchase_actions', 'id="action-form"');
	}
?>
<?php 
    if ($_GET['mode'] != 'consignment') 
        $temp = lang('purchases');
    else
        $temp = lang('Consignment_Purchases');
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-star"></i><?= $temp . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')';?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=admin_url('purchases/add'.$temp_get)?>">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_purchase')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" onclick="api_bulk_action_export('export_excel','<?php echo $_GET['mode']; ?>');" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?=lang("delete_purchases")?></b>"
                                data-content="<p><?=lang('r_u_sure')?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>"
                                data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?=lang('delete_purchases')?>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if (!empty($warehouses)) {
                    ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?=lang("warehouses")?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?=admin_url('purchases')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                            <li class="divider"></li>
                            <?php
                            	foreach ($warehouses as $warehouse) {
                            	        echo '<li ' . ($warehouse_id && $warehouse_id == $warehouse->id ? 'class="active"' : '') . '><a href="' . admin_url('purchases/' . $warehouse->id).$temp_get.'"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            	    }
                                ?>
                        </ul>
                    </li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">            
            <div class="col-lg-12">

<?php
    echo '
    <div style="background: #F9F9F9; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">
    ';

    echo '    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
                    <button type="button" class="btn btn-success" style="margin:5px 0px 5px 5px !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Payment').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_payment_status_to_pending\');">
                            <i class="fa fa-edit"></i>'.lang('to_pending').'
                        </a>    
                    </li>

                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_payment_status_to_partial\');">
                            <i class="fa fa-edit"></i>'.lang('to_partial').'
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_payment_status_to_paid\');">
                            <i class="fa fa-edit"></i>'.lang('to_paid').'
                        </a>    
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle api_link_box_none" href="#">                    
                    <button type="button" class="btn btn-info" style="margin:5px 0px 5px 5px !important; background-color:#004f93  !important; border-color:#004f93  !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Change_Status').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_status_to_received\');">
                            <i class="fa fa-edit"></i>'.lang('to_received').'
                        </a>    
                    </li>

                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_status_to_pending\');">
                            <i class="fa fa-edit"></i>'.lang('to_pending').'
                        </a>
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_status_to_ordered\');">
                            <i class="fa fa-edit"></i>'.lang('to_ordered').'
                        </a>    
                    </li>

                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions_v2(\'admin/purchases/purchase_actions\',\'change_status_to_on_the_way\');">
                            <i class="fa fa-edit"></i>'.lang('to_on_the_way').'
                        </a>    
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '
        <div class="api_clear_both"></div>
    </div>
    ';
?>


                <div class="table-responsive">
                    <table id="POData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th><?= lang("date"); ?></th>
                            <th><?= lang("ref_no"); ?></th>
                            <th><?= lang("Due_Date"); ?></th>
                            <th><?= lang("supplier"); ?></th>
                            <th><?= lang("purchase_status"); ?></th>
                            <th><?= lang("grand_total"); ?></th>
                            <th><?= lang("payment_status"); ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                            <th style="width:100px;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?=lang('loading_data_from_server');?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>
<script type="text/javascript">
    $('body').on('click', '.consignment_purchase_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/modal_view_consignment/' + $(this).parent('.consignment_purchase_link').attr('id') + '?mode=consignment'});
        $('#myModal').modal('show');
    });    
</script>

<style type="text/css">   
    .<?php echo $temp_class; ?>purchase_link{
        cursor: pointer;
    }
    .<?php echo $temp_class; ?>purchase_link td:nth-child(3){
        text-align: center
    }
    .<?php echo $temp_class; ?>purchase_link .pending{
        background-color: #f0ad4e;
    }
    .<?php echo $temp_class; ?>purchase_link .completed{
        background-color: #5cb85c;
    }    
    .purchase_link td:nth-child(5){
        cursor: pointer;
    }     
    .<?php echo $temp_class; ?>purchase_link td:nth-child(9), .<?php echo $temp_class; ?>purchase_link td:nth-child(10){
        cursor: default;
    }
</style>


<script>
    function api_bulk_action_export(action,mode){
        $("#action-form").attr('action', 'admin/purchases/purchase_actions?mode=' + mode);
        $('#form_action').val(action);
        $("#action-form").submit();
    }
</script>








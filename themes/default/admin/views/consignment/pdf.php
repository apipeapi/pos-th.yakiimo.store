<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $this->lang->line('sale') . ' ' . $inv->reference_no; ?></title>
    <link href="<?= $assets ?>styles/pdf/bootstrap.min.css" rel="stylesheet">
    <link href="<?= $assets ?>styles/pdf/pdf.css" rel="stylesheet">
	<link href="<?php echo $assets ?>styles/style.css" rel="stylesheet">
</head>
 <style type="text/css">
        html, body {
            height: 100%;
            background: #FFF;
            font-size: 11px;
        }
        body:before, body:after {
            display: none !important;
        }
        .table th {
            text-align: center;
            padding: 5px;
        }
        .table td {
            padding: 4px;
        }
        .p-style table tr td h2{
            font-size: 14px;
            z-index: 1;
            //padding-top:-15px;
        }
        .p-style p{
            font-size: 11px;
        }
        .t-style table th{
            font-size: 12px;
        }
        .t-style table  tr td{
            font-size: 11px;
        }
        .img_logo{
           max-width: 250px;
           margin-top:-20px;
        }
        
    </style>
<body>
<div id="wrap">
    <div class="row">
        <div class="col-lg-12">
          
			 <?php
                if (is_int(strpos($inv->reference_no,"SL")))
                    $temp = $biller->logo;
                else
                    $temp = 'delivery-logo.png';             
                $path = 'assets/uploads/logos/'.$temp;                
                $type = pathinfo($path, PATHINFO_EXTENSION);
                $data = file_get_contents($path);
                $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
                ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= $base64; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="col-xs-5">
                    <div class="p-style">
                        <table>
                            <tr><td><h2 style="font-size: 14px" class=""><?= $biller->company_kh != '-' ? $biller->company_kh : $biller->name_kh; ?><br><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                            <?= $biller->company_kh ? '' : ' ' . $biller->name_kh; ?><br><?= $biller->company ? '' : ' ' . $biller->name; ?>
                            </td></tr>
                        </table>
                            
                              
                        <br>
                        <?php
                            echo $biller->address_kh !=''? $biller->address_kh:$biller->address;
                            echo '<br/>';
                            echo $biller->city_kh !=''?$biller->city_kh:$biller->city;
                            echo ' ';
                            echo $biller->postal_code;
                            echo ' ';
                            echo $biller->state_kh !=''?$biller->state_kh:$biller->state;

                            echo $biller->country_kh !=''?'<br/>' .$biller->country_kh:'<br/>' .$biller->country;
                            
                            echo '<p>';
                            if ($customer->vat_no) {
                                if ($biller->vat_no != "-" && $biller->vat_no != "") {
                                    echo lang("vat_no_kh") . '&nbsp; (' . lang("vat_no") . ") : " . $biller->vat_no;                               echo '<br>';
                                }
                            }
                            if ($biller->cf1 != '-' && $biller->cf1 != '') {
                                echo lang('bcf1_kh') . '&nbsp; (' . lang('bcf1') . ') : ' . $biller->cf1;
                                                            echo '<br>';
                            }
                            if ($biller->cf2 != '-' && $biller->cf2 != '') {
                                echo lang('bcf2_kh') . '&nbsp; (' . lang('bcf2') . ') : ' . $biller->cf2;
                                                            echo '<br>';
                            }
                            if ($biller->cf3 != '-' && $biller->cf3 != '') {
                                echo lang('bcf3_kh') . '&nbsp; (' . lang('bcf3') . ') : ' . $biller->cf3;
                                                            echo '<br>';
                            }
                            if ($biller->cf4 != '-' && $biller->cf4 != '') {
                                echo lang('bcf4_kh') . '&nbsp; (' . lang('bcf4') . ') : ' . $biller->cf4; 
                                                            echo '<br>';
                            }
                            if ($biller->cf5 != '-' && $biller->cf5 != '') {
                                echo lang('bcf5_kh') . '&nbsp; (' . lang('bcf5') . ') : ' . $biller->cf5;
                                                            echo '<br>';
                            }
                            if ($biller->cf6 != '-' && $biller->cf6 != '') {
                                echo lang('bcf6_kh') . '&nbsp; (' . lang('bcf6') . ') : ' . $biller->cf6; 
                                                            echo '<br>';
                            }
                            echo '</p>';
                            echo lang('tel_kh') . '&nbsp;' . lang('tel') . ': ' . $biller->phone . '<br />'. lang('email_kh') . '&nbsp;' . lang('email') . ': ' . $biller->email;
                        ?>
                    </div>    
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-5">
                    <div class="p-style">
                        <table>
                            <tr>
                                <td>
                                    <h2 style="font-size: 14px" class="">
                                        <?php
                                            if ($customer->company_kh != '')
                                                echo $customer->company_kh.'<br>';
                                            else
                                                echo $customer->name_kh.'<br>';                                               
                                            if ($customer->company != '')
                                                echo $customer->company;
                                            else
                                                echo $customer->name;
                                        ?>
                                    </h2>


                                </td>
                            </tr>
                        </table>
                            
                                         
			<br>
                        <?php
                            if ($inv->parent_name != '')
                                echo '
                                    '.lang('Parent_Company').': <b>'.$inv->parent_name.'</b><br>
                                ';
                        ?>
                        <?php
                            echo $customer->address_kh !=''? $customer->address_kh:$customer->address;
                            echo '<br/>';
                            echo $customer->city_kh !=''?$customer->city_kh:$customer->city;
                            echo ' ';
                            echo $customer->postal_code;
                            echo ' ';
                            echo $customer->state_kh !=''?$customer->state_kh:$customer->state;
                            echo $customer->country_kh !=''?'<br/>' .$customer->country_kh:'<br/>' .$customer->country.'<br><br>';
                          
                            if ($customer->vat_no != "-" && $customer->vat_no != "") {
                                echo lang("vat_no_kh") . '&nbsp; (' . lang("vat_no") . ") : " . $customer->vat_no."<br>";
                            }
                            if ($customer->cf1 != '-' && $customer->cf1 != '') {
                                echo lang('ccf1_kh') . '&nbsp; (' . lang('ccf1') . ') : ' . $customer->cf1;
                                                            echo '<br>';
                            }
                            if ($customer->cf2 != '-' && $customer->cf2 != '') {
                                echo lang('ccf2_kh') . '&nbsp; (' . lang('ccf2') . ') : ' . $customer->cf2;
                                                            echo '<br>';
                            }
                            if ($customer->cf3 != '-' && $customer->cf3 != '') {
                                echo lang('ccf3_kh') . '&nbsp; (' . lang('ccf3') . ') : ' . $customer->cf3;
                                                            echo '<br>';
                            }
                            if ($customer->cf4 != '-' && $customer->cf4 != '') {
                                echo lang('ccf4_kh') . '&nbsp; (' . lang('ccf4') . ') : ' . $customer->cf4;
                                                            echo '<br>';
                            }
                            if ($customer->cf5 != '-' && $customer->cf5 != '') {
                                echo lang('ccf5_kh') . '&nbsp; ('. lang('ccf5') . ') : ' . $customer->cf5;
                                                            echo '<br>';

                            }
                            if ($customer->cf6 != '-' && $customer->cf6 != '') {
                                echo lang('ccf6_kh') . '&nbsp; ('. lang('ccf6') . ') : ' . $customer->cf6; 
                                                            echo '<br>';
                            }
                            echo lang('tel_kh') . '&nbsp;' . lang('tel') . ': ' . $customer->phone. '<br>' . lang('email_kh') . '&nbsp;' . lang('email') . ': ' . $customer->email;
                        ?>
                    </div>       
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row padding10 bold-style">
                <?php /*
                <div class="col-xs-5">
                    <?php
                    echo $Settings->site_name_kh !=''?'<span class="bold">' . $Settings->site_name_kh.'<span><br/>' :'';
                    echo $Settings->site_name !=''?'<span class="bold">' . $Settings->site_name.'<span><br/>' :'';
                    
                    echo $warehouse->name_kh !=''?$warehouse->name_kh.', &nbsp;' :'';
                    echo $warehouse->address_kh !=''?$warehouse->address_kh.'<br/>' :'';
                    echo $warehouse->name !=''?$warehouse->name.', &nbsp;' :'';
                    echo $warehouse->address !=''? $warehouse->address.'<br/>' :'';
                  
                    echo ($warehouse->phone ? lang('tel_kh') .'&nbsp; ('. lang('tel') . ') : ' . $warehouse->phone . '<br>' : '');
                    echo ($warehouse->email ? lang('email_kh') .'&nbsp; ('. lang('email') . '): ' . $warehouse->email : '');
                    ?>
                    <div class="clearfix"></div>
                </div>
                */?>
                <div class="col-xs-5">
                    <div class="bold">
                        <?=lang('date_kh'); ?>&nbsp;<?=lang('date'); ?>: 
                        <?php
                        
                            /*
                            if ($inv->date > $inv->generate_date)
                                echo $this->sma->hrld($inv->date);
                            else
                                echo $this->sma->hrld($inv->generate_date);                        
                            */
                            $date = date_create($inv->date);
                            echo date_format($date, 'd/m/Y');;

                        ?><br>
                        <?= lang('invoice_number_kh'); ?>&nbsp;<?= lang('invoice_number'); ?>: <?= $inv->reference_no; ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive t-style">
                <table class="table">
                    <thead class="table-bordered table-hover table-striped">
                    <tr>
                        <th><?= lang('no_kh'); ?><br><?= lang('no'); ?></th>
                        <th colspan="2"><?= lang('description_kh'); ?><br><?= lang('description'); ?> (<?= lang('code_kh'); ?>&nbsp;<?= lang('code'); ?>)</th>
                        <th><?= lang('quantity_kh'); ?><br><?= lang('quantity'); ?></th>
                        <?php
                            if ($Settings->product_serial) {
                                echo '<th style="text-align:center; vertical-align:middle;">'. lang('serial_no_kh') . '&nbsp; (' . lang('serial_no') . ') </th>';
                            }
                        ?>
                        <th><?= lang('unit_price_kh'); ?><br><?= lang('unit_price'); ?></th>
                        <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<th>'.lang('tax_kh') . '&nbsp;' . lang('tax') . '</th>';
                            }
                            if ( $Settings->product_discount && $inv->product_discount != 0) {
                                echo '<th>'.lang('discount_kh') . '&nbsp;' . lang('discount') . '</th>';
                            }
                        ?>
                        <th><?= lang('subtotal_kh'); ?><br><?= lang('subtotal'); ?></th>
                    </tr>
                    </thead>
                    <tbody class="table-bordered table-hover table-striped">
                    <?php $r = 1;
                        foreach ($rows as $row):
                            ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td colspan="2" style="vertical-align:middle;">
								<?= $row->product_name . ' (' . $row->product_code . ')' . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                </td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity); ?></td>
                                <?php
                                    if ($Settings->product_serial) {
                                        echo '<td>' . $row->serial_no . '</td>';
                                    }
                                ?>
                                <td style="text-align:right; width:90px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                <?php
                                    if ($Settings->tax1 && $inv->product_tax > 0) {
                                        echo '<td style="width: 90px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>(' . $row->tax_code . ')</small> ' : '') . $this->sma->formatMoney($row->item_tax) . '</td>';
                                    }
                                    if ( $Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="width: 90px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                    }
                                ?>
                                <td style="vertical-align:middle; text-align:right; width:110px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                    ?>
                    <?php
                        $col = 5;
                        if ($Settings->product_serial) {
                            $col++;
                        }
                        if ( $Settings->product_discount && $inv->product_discount != 0) {
                            $col++;
                        }
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            $col++;
                        }
                        if ( $Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 2;
                        } elseif ( $Settings->product_discount && $inv->product_discount != 0) {
                            $tcol = $col - 1;
                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 1;
                        } else {
                            $tcol = $col;
                        }
                    ?>
                    <?php if ($inv->grand_total != $inv->total) {
                        ?>
                        <tr>
                            <td colspan="<?= $tcol; ?>" style="text-align:right;"><?= lang('total_kh'); ?><?= lang('total'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                                }
                                if ( $Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                                }
                            ?>
                            <td style="text-align:right;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                        </tr>
                    <?php }
                    ?>
                    <?php if ($return_sale && $return_sale->surcharge != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('surcharge_kh') . '&nbsp;' . lang('surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                    }
                    ?>
                    <?php if ($inv->order_discount != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">'. lang('order_discount_kh') . '&nbsp;' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                    }
                    ?>
                    <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">'. lang('vat_in_kh') . '&nbsp;</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                    }
                    ?>
                    <?php if ($inv->shipping != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">'. lang('shipping_kh') . '&nbsp;' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang('total_amount_kh'); ?>&nbsp;<?= lang('total_amount'); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                    </tr>

                    <?php
                    if (is_int(strpos($inv->reference_no,"SL"))) {
                        echo '
                            <tr>
                                <td colspan="'.$col.'"
                                    style="text-align:right; font-weight:bold;">'.lang('total_amount_kh').'&nbsp;'.lang('total_amount').'
                                    ('.$riel_name.')
                                </td>
                                <td style="text-align:right; font-weight:bold;">
                                    '.$this->sma->formatMoneyRiel($total_riel).' 
                                </td>
                            </tr>                    
                        ';
                    }
                    ?>                    
                    </tbody>
                    <tfoot>
                    <?php
                    if (is_int(strpos($inv->reference_no,"SL"))) {
                        echo '
                            <tr style="border:0px !important;">
                                <td colspan="7" style="text-align:right; border:0px !important;">                            
                                    '.$riel_rate.' 
                                </td>
                            </tr>
                        ';
                    }
                    ?>                    
                    </tfoot>                    
                </table>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php if ($inv->note || $inv->note != '') { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang('note_kh'); ?> | <?= lang('note'); ?>:</p>

                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php }
                    ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-4  pull-left">
                    <p style="height: 80px;">
                        <?= lang('seller_kh'); ?>
                        : <?= $biller->company_kh != '-' ? $biller->company_kh : $biller->name_kh?>
			<br>
			 <?= lang('seller'); ?>
                        : <?= $biller->company != '-' ? $biller->company : $biller->name;?>
			<br>
                    </p>
                    <hr>
                    <p><?= lang('stamp_sign_kh'); ?>&nbsp;<?= lang('stamp_sign'); ?></p>
                </div>
                <div class="col-xs-4  pull-right">
                    <p style="height: 80px;">
                        <?= lang('customer_kh'); ?> 
                        : <?= $customer->company_kh ? $customer->company_kh : $customer->name_kh; ?>
			<br>
                        <?= lang('customer'); ?> 
                        : <?= $customer->company ? $customer->company : $customer->name; ?>
			<br>
							
                    </p>
                    <hr>
                    <p><?= lang('stamp_sign_kh'); ?>&nbsp;<?= lang('stamp_sign'); ?></p>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>

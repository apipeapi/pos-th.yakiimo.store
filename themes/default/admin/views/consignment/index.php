<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 

    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    foreach ($_GET as $name => $value) {
        if ($name != 'page' && $name != 'per_page' && $name != 'mode')
            if ($value != '') $temp_url .= '&'.$name.'='.$value;
    }

    echo admin_form_open('consignment'.$temp_url,'id="action-form" name="action-form" method="GET"');
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-heart"></i>
            <?php
                $temp = '';
                if ($_GET['warehouse_id'] != '') {
                    for ($i=0;$i<count($api_warehouse);$i++) {
                        if ($_GET['warehouse_id'] == $api_warehouse[$i]['id']) {
                            $temp2 = $api_warehouse[$i]['name'];
                            break;
                        }
                    }
                }
                else
                    $temp2 = lang('all_warehouses');
                echo $temp.lang('list_consignment').' ('.$temp2.')'; 
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
<?php
                            $temp = '';
                            echo '
                                <li>
                                    <a href="'.admin_url('consignment/add'.$temp).'">
                                        <i class="fa fa-plus-circle"></i> '.lang('add_consignment').'
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0);" onclick="alert(\'Under construction\')">
                                        <i class="fa fa-plus-circle"></i> '.lang('add_sale').'
                                    </a>
                                </li>                                    
                                <li>
                                    <a href="javascript:void(0)" onclick="api_bulk_actions(\'combine\');" data-action="combine">
                                        <i class="fa fa-file-pdf-o"></i> '.lang('combine_to_pdf').'
                                    </a>
                                </li>
                            ';
                            echo '
                                <li class="divider"></li>
                            ';
?>
                                <li>
                                    <a href="#" class="bpo" title="<b><?=lang("delete_consignment")?></b>" data-content="<p><?=lang('r_u_sure')?></p>
                                    <button type='button' class='btn btn-danger' onclick='bulk_actions_delete();' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>" data-html="true" data-placement="left">
                                        <i class="fa fa-trash-o"></i> <?=lang('delete_consignment')?>
                                    </a>
                                </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
            <?php                

    echo '
    <div style="background: #F9F9F9; margin: -20px -20px 20px -20px; padding: 10px; border-bottom: 1px solid #DBDEE0;">    
        <div class="api_float_right">
            <ul style="list-style: none; margin:0px;">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">                    
                    <button type="button" class="btn btn-success" style="margin:5px 0px 5px 5px !important;">
                        <li class="fa fa-pencil-square-o"></li> '.lang('Set_Status').'
                    </button>
                </a>

                <ul class="dropdown-menu tasks-menus" role="menu" aria-labelledby="dLabel" style="">
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_print_needed\');">
                            <i class="fa fa-edit"></i>'.lang('Print_Needed').'
                        </a>    
                    </li>    
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_preparing\');">
                            <i class="fa fa-edit"></i>'.lang('Preparing').'
                        </a>    
                    </li>              
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_delivering\');">
                            <i class="fa fa-edit"></i>'.lang('Delivering').'
                        </a>    
                    </li>                                               
                    <li>
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_delivered\');">
                            <i class="fa fa-edit"></i>'.lang('Delivered').'
                        </a>    
                    </li>                                               
                    <li> 
                        <a href="javascript:void(0);" onclick="api_bulk_actions(\'change_status_to_completed\');">
                            <i class="fa fa-edit"></i>'.lang('Completed').'
                        </a>
                    </li>
                </ul>
            </li>
            </ul>
        </div>
    ';

    echo '
    <div class="api_float_right">
    ';

    echo '
        <a href="javascript:void(0);" onclick="api_bulk_actions_print_pdf(\'print_combine_pdf\'); window.location = \''.base_url().'admin/consignment\'">
        <button type="button" class="btn btn-info api_link_box_none" style="background-color:#8800a5 !important; border-color:#8800a5 !important; margin-left:5px;">
            <li class="fa fa-print"></li> '.lang('Print_PDF').'
        </button>
        </a>
    ';

    echo '
        <a class="api_display_none" id="set_delivering" href="'.admin_url('consignment/set_delivering_bulk').'" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-truck"></i> '.lang('Set_Delivering').'
        </a>

        <a href="javascript:void(0);" onclick="api_bulk_actions_set_delivery(\'action-form\',\'val[]\',\'\');">    
        <button type="button" class="btn btn-info api_margin_5_im api_link_box_none" style="background-color:#004f93  !important; border-color:#004f93  !important" >
            <li class="fa fa-truck"></li> '.lang('Set_Delivering').'
        </button>
        </a>
    </div>
    ';

    echo '
        <div class="api_clear_both"></div>
    </div>
    ';
?>

                <div id="form">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("Start_Date", "Start_Date"); ?>
                                <?php echo form_input('start_date', (isset($_GET['start_date']) ? $_GET['start_date'] : ''), 'class="form-control datetime" autocomplete="off" id="start_date"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("End_Date", "End_Date"); ?>
                                <?php echo form_input('end_date', (isset($_GET['end_date']) ? $_GET['end_date'] : ''), 'class="form-control datetime" autocomplete="off" id="end_date"'); ?>
                            </div>
                        </div>

<?php
                        echo '
                            <div class="col-md-2">
                                <div class="form-group">
                                    '.lang("status", "status").'
                        ';
                        $tr0a[''] = lang("All_Status");
                        for ($i=0;$i<count($sale_status);$i++) {
                            if ($sale_status[$i]['value'] != '')
                                $tr0a[$sale_status[$i]['value']] = $sale_status[$i]['name'];
                        }
                        echo form_dropdown('sale_status', $tr0a, (isset($_GET['sale_status']) ? $_GET['sale_status'] : ''), 'class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("warehouse", "warehouse").'
                        ';                        
                        $tr4[''] = lang("All_Warehouses");
                        for ($i=0;$i<count($api_warehouse);$i++) {
                            if ($api_warehouse[$i]['name'] != '')
                                $tr4[$api_warehouse[$i]['id']] = $api_warehouse[$i]['name'];
                        }
                        echo form_dropdown('warehouse_id', $tr4, (isset($_GET['warehouse_id']) ? $_GET['warehouse_id'] : ''), ' class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';                        

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("Search", "Search").'
                                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'" onkeydown="if (event.keyCode == 13) document.action-form.submit()" />
                        ';
                        echo '
                                </div>
                            </div>
                        ';

                    echo '
                    </div>
                    ';
                    echo '
                    <div class="row">
                    ';

?>
                                              
                        <div class="col-md-12">
                            <div class="form-group pull-right api_padding_left_10">
                                <a href="<?php echo base_url().'admin/consignment'; ?>">
                                    <input type="button" name="reset" value="reset" class="btn btn-danger">
                                </a>
                            </div>
                            <div class="form-group pull-right">
                                <input type="button" name="submit_report" onclick="api_form_submit('<?php echo 'admin/consignment'.$temp_url; ?>')" value="Submit" class="btn btn-primary">
                            </div>
                        </div>  
                    </div>    

                </div>
                <!-- /form search -->
                <div class="clearfix"></div>

                <div class="table-responsive">
                    <div id="SLData_wrapper" class="dataTables_wrapper form-inline" role="grid">
                        <div class="row">
                            <div class="col-md-6 text-left">
                                <div class="short-result">
                                     <label>Show</label>
                                         <select  id="sel_id" name="per_page" onchange="api_form_submit('<?php echo 'admin/consignment'.$temp_url; ?>')" style="width:80px;">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     '1000'=>'1000',                                          
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                             </div>

                        </div>     
                    </div>   

                    <div class="clearfix api_height_15"></div>
<?php
    $row_headers = array(
        'checkbox'      => [
                            'label' => '<input class="checkbox checkft" type="checkbox" name="check"/>', 
                            'sort_fn' => false,
                            'style' => 'min-width:30px; width: 30px; text-align: center;'
                        ],
        'date'          => ['label' => lang("date"), 'style' => ''],
        'due_date'      => ['label' => lang("due_date"), 'style' => ''],
        'reference_no'  => ['label' => lang("reference_no"), 'style' => ''],
        'customer'      => ['label' => lang("customer"), 'style' => ''],
        'sale_status'      => ['label' => lang("Status"), 'style' => ''],
        'sold_quantity'   => ['label' => lang("Sold_Quantity"), 'style' => '', 'sort_fn' => false],
        'remained_quantity' => ['label' => lang("Remained_Quantity"), 'style' => '', 'sort_fn' => false],
        'returned_quantity' => ['label' => lang("Returned_Quantity"), 'style' => '', 'sort_fn' => false],
        'grand_total'   => ['label' => lang("total_amount"), 'style' => ''],
        'action'        => [
                            'label' => lang("actions"), 
                            'style' => 'width:80px; text-align:center;', 
                            'sort_fn' => false
                        ],
    );
?>

                    <table id="SLData" class="table table-bordered table-hover table-striped" cellpadding="0" cellspacing="0" border="0">
                        <thead>
                        <tr class="primary">

<?php foreach ($row_headers as $key => $row) :?>
    <?php
   
    $th_class="class='pointer'";
    $e_click = true;
    if(isset($row['sort_fn']) && $row['sort_fn'] == false) {
        $th_class="";
        $e_click = false;
    }
    $style = isset($row['style']) && $row['style'] != '' ? 'style="'.$row['style'].'"' : '';
    ?>
    <th <?=$th_class?> <?= $style ?> >
        <?php if($e_click ) :?>
            <?php if ($sort_by == $key && $sort_order == "ASC") : ?>
                    <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                        <span class="fa fa-sort-down"></span>
                    </label>
            <?php elseif ($sort_by == $key and $sort_order=="DESC") : ?>    
                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'ASC')" id="fa_sort_up"  aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort-up"></span>
                </label>
            <?php else : ?> 
                <label class="font-normal" onclick="formactionsubmit('<?= $key ?>', 'DESC')" id="fa_sort_down" aria-hidden="true">
                    <?php echo $row['label']; ?>
                    <span class="fa fa-sort"></span>
                </label>
            <?php endif; ?> 
        <?php else : ?>
            <label class="font-normal" aria-hidden="true">
                <?php echo $row['label']; ?>
            </label>
        <?php endif; ?> 
    </th>
<?php endforeach; ?>

                            </th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

<?php
for ($i=0;$i<count($select_data);$i++) {
    if ($select_data[$i]['due_date'] != '0000-00-00')
        $temp_due_date = date('d/m/Y', strtotime($select_data[$i]['due_date']));            
    else
        $temp_due_date = ''; 

    echo '
    <tr id="'.$select_data[$i]['id'].'" class="invoice_link_consignment">
        <td style="min-width:30px; width: 30px; text-align: center;">
            <div class="text-center">
                <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
            </div>
        </td>
        <td align="center" class="api_td_width_auto">
            '.date('d/m/Y', strtotime($select_data[$i]['date'])).'
            </br>
            '.date('H:i:s', strtotime($select_data[$i]['date'])).'
        </td>
        <td align="center" class="api_td_width_auto">
            '.$temp_due_date.'
        </td>        
        <td align="center" class="api_td_width_auto">
            '.$select_data[$i]['reference_no'].'
        </td>
    ';

    $config_data = array(
        'table_name' => 'sma_companies',
        'select_table' => 'sma_companies',
        'translate' => '',
        'select_condition' => "id = ".$select_data[$i]['customer_id'],
    );
    $temp = $this->site->api_select_data_v2($config_data);
    $temp2 = '';
    if ($temp[0]['sale_consignment_auto_insert'] == 'yes')
        $temp2 = '
            <br>
            <label class="label label-primary">
                '.lang('Auto_Add').'
            </label>
        ';

    echo '
        <td>
            '.$select_data[$i]['customer'].'
            '.$temp2.'
        </td>
    ';

    $sale_status = $select_data[$i]['sale_status'];
    $temp_display = '';
    if ($sale_status == 'print needed' || $sale_status == 'pending' || $sale_status == '') {
        $temp_display .= '<span class="label label-info" style="background-color:#8800a5 !important;">'.lang('Print_Needed').'</span>';
    }    
    elseif ($sale_status == 'completed') {
        $temp_display .= '<span class="label label-success">'.lang('Completed').'</span>';
    }
    elseif ($sale_status == 'preparing') {
        $temp_display .= '<span class="label label-info">'.lang('Preparing').'</span>';
    } 
    elseif ($sale_status == 'delivering') {
        $temp_display .= '<span class="label label-info" style="background-color:#004f93 !important;">'.lang('Delivering').'</span>';
    }    
    elseif ($sale_status == 'delivered') {
        $temp_display .= '<span class="label label-info" style="background-color:#002d54 !important;">'.lang('Delivered').'</span>';
    }   

    echo '
        <td align="center">
            '.$temp_display.'
        </td>
    ';

    $temp = $this->site->api_select_some_fields_with_where("*
        "
        ,"sma_consignment_sale_items"
        ,"consignment_id = ".$select_data[$i]['id']
        ,"arr"
    );
    if (count($temp) > 0) {
        $temp_sales = '
            <a class="api_link_box_none" href="'.base_url().'admin/consignment/consignment_sale_history/'.$select_data[$i]['id'].'" title="Sales" data-toggle="modal" data-target="#myModal"> 
                <span class="label label-info">S</span>
            </a>
        ';
    }
    else
        $temp_sales = '';

    $config_data = array(
        'id' => $select_data[$i]['id'],
        'reference_no' => $select_data[$i]['reference_no'],
    );
    $api_get_consignment_qty = $this->consignment_model->api_get_consignment_qty($config_data);

    echo '
        <td class="api_td_width_auto" align="right">
            '.$this->site->api_number_format($api_get_consignment_qty['sold_qty'],2).'
        </td>
    ';
    echo '
        <td class="api_td_width_auto" align="right">
            '.$api_get_consignment_qty['available_qty'].'/'.$api_get_consignment_qty['total_qty'].'
        </td>
    ';
    echo '
        <td class="api_td_width_auto" align="right">
            '.$this->site->api_number_format($api_get_consignment_qty['returned_qty'],2).'
        </td>
    ';

    echo '
        <td class="api_td_width_auto" align="right">
            '.$this->sma->formatMoney($select_data[$i]['grand_total']).'
        </td>
    ';          

    if ($temp_returned > 0)
        $temp_return = '
            <a class="api_link_box_none" href="'.base_url().'admin/returns?search='.$select_data[$i]['reference_no'].'" target="_blank" title="'.lang('Returned_Items').'"> 
                <span class="label label-danger">R</span>
            </a>
        ';
    else
        $temp_return = '';

    $edit_link = anchor('admin/consignment/edit/'.$select_data[$i]['id'], '<i class="fa fa-edit"></i> ' . lang('edit'), 'class="sledit"');



    //if ($select_data[$i]['sale_status'] != 'completed')
        $add_sale_link = anchor('admin/consignment/add_sale_consignment/'.$select_data[$i]['reference_no'], 
                '<i class="fa fa-file-text-o"></i> ' . lang('add_sale'), 'data-toggle="modal" data-target="#myModal"');
    // else
    //     $add_sale_link = anchor('admin/consignment/add_sale_consignment/'.$select_data[$i]['reference_no'], 
    //             '<i class="fa fa-file-text-o"></i> ' . lang('add_sale'), '');

    $view_sale_link = anchor('admin/sales?search='.$select_data[$i]['reference_no'], 
                '<i class="fa fa-eye"></i> ' . lang('view_sales'), '');

    if ($select_data[$i]['sale_status'] != 'completed')
        $add_return_link = anchor('admin/consignment/add_sale_consignment/'.$select_data[$i]['reference_no'].'?return=1', 
                '<i class="fa fa-random"></i> ' . lang('add_return'), 'data-toggle="modal" data-target="#myModal"');
    else
        $add_return_link = anchor('admin/consignment/add_sale_consignment/'.$select_data[$i]['reference_no'].'?return=1', 
            '<i class="fa fa-file-text-o"></i> ' . lang('add_return'), '');

    $view_return_link = anchor('admin/returns?search='.$select_data[$i]['reference_no'], 
                '<i class="fa fa-eye"></i> ' . lang('view_returns'), '');


    $delete_link = "
        <a href='#' class='po' 
            title='<b>".lang("delete_consignment")."</b>' 
            data-content=\"
                <p>".lang('r_u_sure')."</p>
                <a class='btn btn-danger' href='".admin_url('consignment/delete/'.$select_data[$i]['id'])."'>
                    ".lang('i_m_sure')."
                </a> 
                <button class='btn po-close'>".lang('no')."</button>\" 
                data-html=\"true\" data-placement=\"left\">
            <i class='fa fa-trash-o'></i>
            ".lang('delete_consignment')."
        </a>
    ";

    $temp_display = '';
    $temp_display .= '
        <li>'.$edit_link.'</li>
        <li>'.$add_sale_link.'</li>
        <li>'.$view_sale_link.'</li>
        <li>'.$add_return_link.'</li>
        <li>'.$view_return_link.'</li>
        <li>
            <a href="javascript:void(0);" onclick="window.open(\''.base_url().'admin/consignment/pdf/'.$select_data[$i]['id'].'?print=1\', \'_blank\'); window.location = \''.base_url().'admin/consignment\'">          
                <i class="fa fa-print"></i> '.lang('Print_PDF').'
            </a>
        </li>
        <li>'.$delete_link.'</li>
    ';
    echo '
        <td style="width:80px; text-align:center;">
            <div class="text-center">
                <div class="btn-group text-left">
                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" 
                            data-toggle="dropdown">
                        '.lang('actions').'
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        '.$temp_display.'
                    </ul>
                </div>
            </div>
            '.$temp_sales.'
            '.$temp_return.'                
        </td>
    ';

    echo '
    </tr>
    ';
}
if (count($select_data) <= 0) {
    echo '
    <tr>
    <td colspan="11">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
echo '
    </tbody>
';
?>                            
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show.' ';?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?= form_close() ?>

<?php

echo admin_form_open('consignment/sale_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();

?>


<style>    
    .modal-body h2{
        font-size: 20px;
        margin-top: 8px;
    }
    .modal-body p{
        font-size: 18px;
        margin-bottom: 17px;
    }
    #tbl-update_to_due {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_paid {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_partail {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    h4.modal-title{
        font-size: 18px;
    }
    .panel-heading-left {
        float: left;
    }
    .panel-heading-right {
        float: right;
    }
    .pointer{
        cursor:pointer;
    }
    .font-normal{
        font-style: normal;
    }
    
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 5px 10px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #428bca;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        font-size: 13px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: -5px 0;
        border-radius: 4px;
    }
    .txt-f{
        color:#333 !important
    }
    #loading{
        display:none !important;
        visibility : hidden;
    }
</style>

<script>
    function api_bulk_actions(action){
        $("#api_action").val(action);
        document.api_form_action.submit();
    } 
    function bulk_actions_delete(){
        $("#api_action").val('delete');
        document.api_form_action.submit();
    }
    function bulk_actions_generate_reference_no(){
        $("#api_action").val('generate_reference_no');
        document.api_form_action.submit();
    }     
    function api_bulk_actions_print_pdf(action){    
        $("#api_form_action").attr('target', '_blank');
        $('#api_action').val(action);
        document.api_form_action.submit(); 
    }
    function api_bulk_actions_set_delivery(frm,chkname,url){
        var nocheck = true;
        var selected_id = 0;
        var temp = '';
        
        eles = document.forms[frm].elements;
        for (var i=0;i<eles.length;i++){
            if (eles[i].name == chkname){
                if(eles[i].checked==true){
                    selected_id = eles[i].value;
                    nocheck = false;
                    temp = temp + eles[i].value + '-';
                }
            } 
        }
        if (nocheck == true) {
            alert('Please select a consignment');
            window.location = '<?= base_url() ?>admin/consignment';
        }
        else {
            $("#set_delivering").attr('href', '<?= base_url() ?>admin/consignment/set_delivering_bulk/' + temp);
            $('#set_delivering').click();
        }    
    }

    function formactionsubmit(sort_by, sort_order) {
        document.getElementById("sort_by").value = sort_by;
        document.getElementById("sort_order").value = sort_order;
        document.forms[0].submit();
    }
    function api_form_submit(action){
        $("#action-form").attr('action', action);
        $('#form_action').val('');
        $("#action-form").submit();
    }         
<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>    
</script>



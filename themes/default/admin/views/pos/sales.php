<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        oTable = $('#POSData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('pos/getSales'.($warehouse_id ? '/'.$warehouse_id : '')) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "receipt_link";
                return nRow;
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            },
            {"mRender": fld}, null, null, null, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": currencyFormat}, {"mRender": row_status}, {"mRender": pay_status}, {"bSortable": false}],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                    paid += parseFloat(aaData[aiDisplay[i]][6]);
                    balance += parseFloat(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                nCells[7].innerHTML = currencyFormat(parseFloat(balance));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text"},
            {column_number: 8, filter_default_label: "[<?=lang('sale_status');?>]", filter_type: "text", data: []},
            {column_number: 9, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer");

        $(document).on('click', '.duplicate_pos', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            if (localStorage.getItem('positems')) {
                bootbox.confirm("<?= $this->lang->line('leave_alert') ?>", function (gotit) {
                    if (gotit == false) {
                        return true;
                    } else {
                        window.location.href = link;
                    }
                });
            } else {
                window.location.href = link;
            }
        });
        $(document).on('click', '.email_receipt', function () {
            var sid = $(this).attr('data-id');
            var ea = $(this).attr('data-email-address');
            var email = prompt("<?= lang("email_address"); ?>", ea);
            if (email != null) {
                $.ajax({
                    type: "post",
                    url: "<?= admin_url('pos/email_receipt') ?>/" + sid,
                    data: { <?= $this->security->get_csrf_token_name(); ?>: "<?= $this->security->get_csrf_hash(); ?>", email: email, id: sid },
                    dataType: "json",
                        success: function (data) {
                        bootbox.alert(data.msg);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_request_failed'); ?>');
                        return false;
                    }
                });
            }
        });
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo admin_form_open('sales/sale_actions', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= lang('pos_sales') . ' (' . ($warehouse_id ? $warehouse->name : lang('all_warehouses')) . ')'; ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip"  data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?= admin_url('pos') ?>"><i class="fa fa-plus-circle"></i> <?= lang('add_sale') ?></a></li>
                        <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" class="bpo" title="<b><?= $this->lang->line("delete_sales") ?></b>" data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_sales') ?></a></li>
                    </ul>
                </li>
                <?php if (!empty($warehouses)) { ?>
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?= lang("warehouses") ?>"></i></a>
                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?= admin_url('pos/sales') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                            <li class="divider"></li>
                            <?php
                            foreach ($warehouses as $warehouse) {
                                echo '<li><a href="' . admin_url('pos/sales/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                    <table id="POSData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th><?= lang("date"); ?></th>
                            <th><?= lang("reference_no"); ?></th>
                            <th><?= lang("biller"); ?></th>
                            <th><?= lang("customer"); ?></th>
                            <th><?= lang("grand_total"); ?></th>
                            <th><?= lang("paid"); ?></th>
                            <th><?= lang("balance"); ?></th>
                            <th><?= lang("sale_status"); ?></th>
                            <th><?= lang("payment_status"); ?></th>
                            <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?= lang("grand_total"); ?></th>
                            <th><?= lang("paid"); ?></th>
                            <th><?= lang("balance"); ?></th>
                            <th class="defaul-color"></th>
                            <th class="defaul-color"></th>
                            <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
var initAppendingDOM;
var inc = 0;
/*******************
 * Author: TEP Afril
 * 18th May, 2018
 * odadcambodia.com
********************/
$(document).ready(function () {
    var grand_total = Number(0);
    var paid = Number(0);
    var balance = Number(0);
    var arrIndex = [];

    var updateData = function(){
        $('#custom_g_t').html(numberWithCommas(grand_total));
        $('#custom_paid').html(numberWithCommas(paid));
        $('#custom_balance').html(numberWithCommas(balance));
    };

    numberWithCommas = (x) => {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    var sumData = function( gbl, vbl, sign ){
        gbl = Number(gbl);
        vbl = vbl.replace(/\$/g,'');
        vbl = vbl.replace(/\,/g,'');
        vbl = Number(vbl);

        switch(sign)
        {
            case "+":
                gbl += (Number(vbl));
            break;

            case "-":
                gbl -= (Number(vbl));
            break;
        }
        gbl = gbl.toFixed(2);
        return gbl;
    };

    var resetData = function(){
        grand_total = '0.00';
        paid = '0.00';
        balance = '0.00';
        updateData();
    };

    initAppendingDOM = function(gt,pd,bl,idx){
        domStr = '<div style="width:15vw;position:fixed;top:50px;right:280px;z-index:1000;opacity:100">'+
        '<a id="expand_sum_table" class="btn btn-success btn-sm" style="top:45px;right:30px;position:fixed;"><i style="" class="fa fa-2x fa-arrows-alt"></i></a>'+
        '<div id="sum_table_container" style="display: none;">'+
        '<a id="close_sum_table" class="btn btn-sm btn-danger" style="padding: 2px 5px;cursor:pointer;top:45px;right:270px;position:fixed;"><i class="fa fa-times"></i></a>'+
        '<table  class="table table-bordered table-hover table-striped dataTable">'+
        '<thead>'+
        '<tr>'+
        '<td style="background-color:#333;border-color:#000;">'+gt+'</td>'+
        '</tr>'+
        '</thead>'+
        '<tbody>'+
        '<tr style="font-weight:bold;">'+
        '<td id="custom_g_t" style="background-color:#d1d1d1;">0.00</td>'+
        '</tr>'+
        '</tbody>'+
        '</table>'+
        '</div>'+
        '</div>';
        $('.box').append(domStr);
        arrIndex = idx;
    };

    $('body').on('ifChecked','.icheckbox_square-blue',function(){
        inc++;

        var row = $(this).parent().parent().parent();
        var td_g_t = $(row[0]).children().eq(arrIndex[0]);
        var td_paid = $(row[0]).children().eq(arrIndex[1]);
        var td_balance = $(row[0]).children().eq(arrIndex[2]);


        var selected_g_t = $(td_g_t).text();
        var selected_paid = $(td_paid).text();
        var selected_balance = $(td_balance).text();

        var temp = grand_total;

        grand_total = sumData(grand_total,selected_g_t,'+');
        paid = sumData(paid,selected_paid,'+');
        balance = sumData(balance,selected_balance,'+');
        updateData();
    });

    $('body').on('ifUnchecked','.icheckbox_square-blue',function(){


        var row = $(this).parent().parent().parent();

        var td_g_t = $(row[0]).children().eq(arrIndex[0]);
        var td_paid = $(row[0]).children().eq(arrIndex[1]);
        var td_balance = $(row[0]).children().eq(arrIndex[2]);

        var selected_g_t = $(td_g_t).text();
        var selected_paid = $(td_paid).text();
        var selected_balance = $(td_balance).text();

        grand_total = sumData(grand_total,selected_g_t,'-');
        paid = sumData(paid,selected_paid,'-');
        balance = sumData(balance,selected_balance,'-');

        updateData();
    });

    $('body').on('click','#close_sum_table',function(){
        $('#sum_table_container').hide(300,function(){
            $('#expand_sum_table').show(300);
        });
        });
        $('body').on('click','#expand_sum_table',function(){
        $('#expand_sum_table').hide(300,function(){
            $('#sum_table_container').show(300);
        });
    });
    $('body').on('change', 'select[name="POData_length"]', function (e) {
        resetData();
    });
    $('body').on('click', '.pagination a', function (e) {
        resetData();
    });

    $(document).ready(function(){
        initAppendingDOM('<?= lang("total_amount"); ?>','<?= lang("paid"); ?>','<?= lang("balance"); ?>',[6,7,8]);
    });


});

</script>


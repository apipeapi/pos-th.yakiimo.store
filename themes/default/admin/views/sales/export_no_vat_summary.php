<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("sales/export_no_vat_summary/".$vat, $attrib); 
        if ($vat != '') $temp = 'VAT'; else $temp = 'No VAT'
        ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="customer">
            			<?php echo lang("customer").' '.$temp; ?></label>
                        <div class="controls"> 
            				<?php
                        $tr[''] = lang("Please_select_a_customer");
                        foreach ($company as $temp) {
                            if ($temp->company != '')
                                $tr[$temp->id] = $temp->company.' &nbsp;&nbsp;&nbsp;&nbsp;('.$temp->name.')';
                        }
                        echo form_dropdown('customer', $tr, '', 'data-placeholder="'.lang("Please_select_a_customer").'" class="form-control" required="required" onchange="api_temp_customer_change(this.value);"');                            
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 api_display_none">
                    <div class="form-group">
                        <label class="control-label" for="add_ons_company_branch">
                        <?php echo lang("Company_Branch"); ?></label>
                        <div class="controls" id="company_branch_wrapper"> 
                            <?php
                            $tr2[''] = lang('Please_select_a_company_branch');
                            echo form_dropdown('add_ons_company_branch', $tr2, '', 'data-placeholder="'.lang('Please_select_company_branch').'" id="add_ons_company_branch" class="form-control" disabled="diabled" ');                            
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("start_date", "start_date"); ?>
                        <?php echo form_input('start_date', isset($_POST['start_date']) ? $_POST['start_date'] : "", 'class="form-control date" required="required" id="start_date" autocomplete="off"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("end_date", "end_date"); ?>
                        <?php echo form_input('end_date', isset($_POST['end_date']) ? $_POST['end_date'] : "", 'class="form-control date" required="required" id="end_date" autocomplete="off"'); ?>
                    </div>
                </div>

                <?php
                    if ($vat != '') {
                        $temp_1 = set_radio('vat_type', 'yes', TRUE);
            
                        echo '
                        <div class="form-group col-md-6">
                            '.lang("Invoice_Type", "vat_type").'
                            <div class="">            
                                <div class="api_height_10"></div>
                                <input type="radio" id="vat_type" name="vat_type" value="yes" '.$temp_1.' />
                                SL
                            </div>
                        </div>                
                        ';
                    }
                    else {

                        $temp_2 = set_radio('vat_type', 'no', true);                        
                        echo '
                        <div class="form-group col-md-6">
                            '.lang("Invoice_Type", "vat_type").'
                            <div class="">            
                                <div class="api_height_10"></div>
                                <span class="api_padding_left_10">
                                    <input type="radio" id="vat_type" name="vat_type" value="no" '.$temp_2.' />
                                </span>
                                DO
                            </div>
                        </div>                
                        ';         
                    }           


                    $temp_1 = set_radio('rebate', 'yes', TRUE);
                    $temp_2 = set_radio('rebate', 'no');
                    echo '
                    <div class="form-group col-md-6">
                        '.lang("Rebate", "rebate").'
                        <div class="">            
                            <div class="api_height_10"></div>
                            <input type="radio" id="rebate" name="rebate" value="yes" '.$temp_1.' />
                            Yes
                            <span class="api_padding_left_10">
                                <input type="radio" id="rebate" name="rebate" value="no" '.$temp_2.' />
                            </span>
                            No
                        </div>
                    </div>                
                    ';

                ?>                                

            </div>

        </div>
        <div class="modal-footer">
            <?php 
                echo form_submit('Export', lang('Export'), 'class="btn btn-primary"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">

function api_temp_customer_change(id){
    var postData = [];
    postData['id'] = id;    
    
    var result = $.ajax
    (
        {
            url: '<?php echo base_url(); ?>admin/sales/api_temp_customer_change?id=' + postData['id'],
            type: 'GET',
            secureuri:false,
            dataType: 'html',
            data:postData,
            async: false,
            error: function (response, status, e)
            {
                alert(e);
            }
        }
    ).responseText;
//var myWindow = window.open("", "MsgWindow", "width=700, height=400");
//myWindow.document.write(result);     
    var array_data = String(result).split("api-ajax-request-multiple-result-split");
    var result_text = array_data[1];

    $('#company_branch_wrapper').html(result_text);
}

</script>
<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script  src="<?php echo base_url().'/assets/api/js/public.js'; ?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'/assets/api/css/public.css'; ?>" type="text/css"/>

<?php 
    $config_data = array(
        'table_name' => 'sma_payments',
        'select_table' => 'sma_payments',
        'translate' => '',
        'select_condition' => "sale_id = ".$inv->id,
    );
    $select_amount = $this->site->api_select_data_v2($config_data);
    $k_paid = 0;
    for ($j=0;$j<count($select_amount);$j++){
        $my_amount = $select_amount[$j]['amount'];
        $k_paid += $my_amount;
    } 
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payment'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'name' => 'myForm');
        echo admin_form_open_multipart("sales/add_payment/" . $inv->id, $attrib); ?>
        <div class="modal-body">
            <p id="remaining_amount" class="">
            
            </p>

            <div class="row">
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= lang("date", "date"); ?>
                            <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" autocomplete="off" required="required"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-sm-6 api_display_none">
                    <div class="form-group">
                        <?= lang("reference_no", "reference_no"); ?>
                        <?= form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $payment_ref), 'class="form-control tip" id="reference_no"'); ?>
                    </div>
                </div>

                <input type="hidden" value="<?php echo $inv->id; ?>" name="sale_id"/>
            </div>
            <div class="clearfix"></div>
            <div id="payments">
                <div class="well well-sm well_1">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("amount", "amount_1"); ?>
                                            <input name="amount-paid" type="text" id="amount_1" 
                                            class="pa form-control kb-pad amount api_numberic_input" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= lang("paying_by", "paid_by_1"); ?>
                                    <select name="paid_by" id="paid_by_1" class="form-control paid_by" required="required" onchange="api_bank_in_form_action(this.value);">
                                        <?= $this->sma->paid_opts(); ?>
                                    </select>
                                    <script>
                                        $('#api_option_ar').addClass('api_display_none');
                                    </script>
                                </div>
                            </div>

                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group gc" style="display: none;">
                            <?= lang("gift_card_no", "gift_card_no"); ?>
                            <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>

                            <div id="gc_details"></div>
                        </div>
                        <div class="pcc_1" style="display:none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="pcc_no" type="text" id="pcc_no_1" class="form-control"
                                               placeholder="<?= lang('cc_no') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">

                                        <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control"
                                               placeholder="<?= lang('cc_holder') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type" placeholder="<?= lang('card_type') ?>">
                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                            <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                        </select>
                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input name="pcc_month" type="text" id="pcc_month_1" class="form-control"
                                               placeholder="<?= lang('month') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <input name="pcc_year" type="text" id="pcc_year_1" class="form-control"
                                               placeholder="<?= lang('year') ?>"/>
                                    </div>
                                </div>
                                <!--<div class="col-md-3">
                                    <div class="form-group">
                                        <input name="pcc_ccv" type="text" id="pcc_cvv2_1" class="form-control" placeholder="<?= lang('cvv2') ?>" />
                                    </div>
                                </div>-->
                            </div>
                        </div>
                        <div class="pcheque_1" style="display:none;">
                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                <input name="cheque_no" type="text" id="cheque_no_1" class="form-control cheque_no"/>
                            </div>
                        </div>
                    </div>
                        <?php
                        $array_field = array('customer_account_number','company_account_number','transfer_amount','transfer_reference_number');
                        // ABA Input
$api_bank_in_form = '<div id="api_bank_in_form">';
foreach ($array_field as $item) {
    $api_bank_in_form .= '
        <div class="col-md-6 '.$temp_padding.'">
        <div class="form-group">
            '.lang($item, $item).'
            '.form_input("add_ons_$item", '', 'class="form-control" id="'.$item.'"').'
        </div>
        </div>
    ';
}
$api_bank_in_form .= '</div>';
echo $api_bank_in_form;

// ABA QR
$form_aba_qrcode = '<div id="aba_qr_bank_in_form">';
foreach ($array_field as $item) {
    while ($item === 'transfer_reference_number') {
        $form_aba_qrcode .= '
            <div class="col-md-12 '.$temp_padding.'">                                 
            <div class="form-group">
                '.lang($item, $item).'
                '.form_input("add_ons_aba_qr_$item", '', 'class="form-control" id="aba_qr_'.$item.'"').'
            </div>
            </div>
        ';
        break;
    }
}
$form_aba_qrcode .= '</div>';
echo $form_aba_qrcode;

$api_aba_daiki_form .= '
<div id="api_aba_daiki_form">
';
$api_aba_daiki_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('ABA_Transaction_Id', 'ABA_Transaction_Id').'
        '.form_input('add_ons_aba_daiki_transaction_id', '', 'class="form-control" id="aba_daiki_transaction_id" ').'
    </div>
    </div>
';
$api_aba_daiki_form .= '
</div>
';

echo $api_aba_daiki_form;

$api_pipay_form .= '
<div id="api_pipay_form">
';
$api_pipay_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Pipay_Transaction_Id', 'Pipay_Transaction_Id').'
        '.form_input('add_ons_pipay_transaction_id', '', 'class="form-control" id="pipay_transaction_id" ').'
    </div>
    </div>
';
$api_pipay_form .= '
</div>
';

echo $api_pipay_form;

$api_wing_form .= '
<div id="api_wing_form">
';
$api_wing_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Wing_Transaction_Id', 'Wing_Transaction_Id').'
        '.form_input('add_ons_wing_transaction_id', '', 'class="form-control" id="wing_transaction_id" ').'
    </div>
    </div>
';
$api_wing_form .= '
</div>
';

echo $api_wing_form;

$api_payway_form .= '
<div id="api_payway_form">
';
$api_payway_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Payway_Transaction_Id', 'Payway_Transaction_Id').'
        '.form_input('add_ons_payway_transaction_id', '', 'class="form-control" id="payway_transaction_id" ').'
    </div>
    </div>
';
$api_payway_form .= '
</div>
';

echo $api_payway_form;

$api_wing_daiki_form .= '
<div id="api_wing_daiki_form">
';
$api_wing_daiki_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Wing_Daiki_Transaction_Id', 'Wing_Daiki_Transaction_Id').'
        '.form_input('add_ons_wing_daiki_transaction_id', $payment->wing_daiki_transaction_id, 'class="form-control" id="wing_daiki_transaction_id" ').'
    </div>
    </div>
';
$api_wing_daiki_form .= '
</div>
';
echo $api_wing_daiki_form;

$api_acode_daiki_form .= '
<div id="api_acode_daiki_form">
';
$api_acode_daiki_form .= '   
    <div class="col-md-12 '.$temp_padding.'">                                 
    <div class="form-group">
        '.lang('Acode_Daiki_Transaction_Id', 'Acode_Daiki_Transaction_Id').'
        '.form_input('add_ons_acode_daiki_transaction_id', $payment->acode_daiki_transaction_id, 'class="form-control" id="acode_daiki_transaction_id" ').'
    </div>
    </div>
';
$api_acode_daiki_form .= '
</div>
';
echo $api_acode_daiki_form;

                        ?>                               
          
                    
                    <div class="clearfix"></div>
                </div>

            </div>

            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false" class="form-control file">
            </div>

            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_button('add_payment', lang('add_payment'), 'class="btn btn-primary" id="Btn_disabled" onclick="document.myForm.submit();"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id != <?=$inv->customer_id?>) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            var due = <?=$inv->grand_total-$inv->paid?>;
                            if (due > data.balance) {
                                $('#amount_1').val(formatDecimal(data.balance));
                            }
                            $('#gc_details').html('<small>Card No: <span style="max-width:60%;float:right;">' + data.card_no + '</span><br>Value: <span style="max-width:60%;float:right;">' + currencyFormat(data.value) + '</span><br>Balance: <span style="max-width:60%;float:right;">' + currencyFormat(data.balance) + '</span></small>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });
        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            var array_hide = ['.pcheque_1','.pcc_1','.pcash_1','.gc']
            for(var i=0;i<array_hide.length;i++){
                $(array_hide[i]).hide('fast');
            }
            if (p_val == 'cash') {
                $('.pcash_1').show();
                $('#amount_1').focus();
            } else if (p_val == 'aba_qr') {
                $('.pcash_1').show();
                $('#aba_qr_transfer_reference_number').focus();
            } else if (p_val == 'CC') {
                $('.pcc_1').show();
                $('#pcc_no_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else if (p_val == 'gift_card') {
                $('.gc').show();
                $('#gift_card_no').focus();
            }
        });
        $('#pcc_no_1').change(function (e) {
            var pcc_no = $(this).val();
            localStorage.setItem('pcc_no_1', pcc_no);
            var CardType = null;
            var ccn1 = pcc_no.charAt(0);
            if (ccn1 == 4)
                CardType = 'Visa';
            else if (ccn1 == 5)
                CardType = 'MasterCard';
            else if (ccn1 == 3)
                CardType = 'Amex';
            else if (ccn1 == 6)
                CardType = 'Discover';
            else
                CardType = 'Visa';

            $('#pcc_type_1').select2("val", CardType);
        });
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });
    function api_bank_in_form_action(action){
        var array_hide = ['#aba_qr_bank_in_form','#api_bank_in_form','#api_pipay_form','#api_wing_form','#api_payway_form','#api_aba_daiki_form','#api_wing_daiki_form','#api_acode_daiki_form']
        for(var i=0;i<array_hide.length;i++){
            $(array_hide[i]).hide('fast');
        }
        if (action == 'aba'){
            $('#api_bank_in_form').show('fast');
            $('#transfer_amount').val($('#amount_1').val())
        } 
        else if (action == 'aba_qr') {
            $('#aba_qr_bank_in_form').show('fast');
            $('#aba_qr_transfer_amount').val($('#amount_1').val())
            $('#aba_qr_transfer_reference_number').focus();
        } 
        else if (action == 'acleda') {
            $('#api_bank_in_form').show('fast');
        }
        else if (action == 'pipay') {
            $('#api_pipay_form').show('fast');
            $('#pipay_transaction_id').focus();
        }
        else if (action == 'wing') {
            $('#api_wing_form').show('fast');
            $('#wing_transaction_id').focus();
        }
        else if (action == 'payway') {
            $('#api_payway_form').show('fast');
            $('#payway_transaction_id').focus();
        }
        else if (action == 'aba_daiki') {
            $('#api_aba_daiki_form').show('fast');
            $('#aba_daiki_transaction_id').focus();
        }             
        else if (action == 'wing_daiki') {
            $('#api_wing_daiki_form').show('fast');
            $('#wing_transaction_id').focus();
        }    
        else if (action == 'acode_daiki') {
            $('#api_acode_daiki_form').show('fast');
            $('#acode_transaction_id').focus();
        }                                          
    }
    api_bank_in_form_action('');

   

</script>

<script>
    var grand_total = <?= $inv->grand_total; ?>;
    var amount = <?= $k_paid; ?>;
    var total = grand_total - amount;
    document.getElementById('amount_1').value = total.toFixed(2);
    document.getElementById('remaining_amount').innerHTML = 'Remaining amount: $' + total.toFixed(2);
    if (total <= 0 ) {
        document.getElementById('amount_1').value = 0;
        document.getElementById("Btn_disabled").disabled = true;
        $('#remaining_amount').addClass('api_color_red');
    }

    function calculate_amount() {
        var grand_total = <?= $inv->grand_total; ?>;
        var amount = <?= $k_paid; ?>;
        var total = grand_total - amount;        
        var get_amount_1 = parseFloat(document.getElementById('amount_1').value);
        if (parseFloat(document.getElementById('amount_1').value) < 0)
            document.getElementById('amount_1').value = 0;
        if (get_amount_1 > total.toFixed(2)) {
            document.getElementById("Btn_disabled").disabled = true;
            $('#remaining_amount').addClass('api_color_red');
        } else {
            if (get_amount_1 != 0) {
                document.getElementById("Btn_disabled").disabled = false;
                $('#remaining_amount').removeClass('api_color_red');
            }
        }        
    }

    calculate_amount();
    $("#amount_1").keyup(function() {
        calculate_amount();
    })
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

    $temp_url = '';
    if ($_GET['page'] != '') 
        $temp_url .= '?page='.$_GET['page']; 
    else 
        $temp_url .= '?page=1'; 
    if ($_GET['search'] != '') $temp_url .= '&sort_by='.$_GET['search'];
    if ($_GET['sort_by'] != '') $temp_url .= '&sort_by='.$_GET['sort_by'];
    if ($_GET['sort_order'] != '') $temp_url .= '&sort_order='.$_GET['sort_order'];
    if ($per_page != '') $temp_url .= '&per_page='.$per_page;

    echo admin_form_open('products/quantity_adjustments'.$temp_url,'id="action-form" name="action-form" method="GET"');

?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= $page_title ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('products/add_adjustment').'?remove=1' ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('Add_Adjustment') ?>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" onclick="api_bulk_actions('export_excel');">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>                        
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<b><?= lang("delete_adjustment") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' onclick='bulk_actions_delete();'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                            <i class="fa fa-trash-o"></i> <?= lang('Delete_Adjustment') ?>
                             </a>
                         </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>

<?php
                        echo '
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    '.lang("warehouse", "warehouse").'
                        ';
                        $tr3[''] = lang("All_Warehouses");
                        for ($i=0;$i<count($api_warehouse);$i++) {
                            if ($api_warehouse[$i]['name'] != '')
                                $tr3[$api_warehouse[$i]['id']] = $api_warehouse[$i]['name'];
                        }
                        echo form_dropdown('warehouse', $tr3, (isset($_GET['warehouse']) ? $_GET['warehouse'] : ''), ' class="form-control"');
                        echo '
                                </div>
                            </div>
                        ';                        

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("created_by", "created_by").'
                        ';
                        $tr4[''] = lang("All_Users");
                        for ($i=0;$i<count($api_user);$i++) {
                            if ($api_user[$i]['fullname'] != '')
                                $tr4[$api_user[$i]['id']] = $api_user[$i]['fullname'];
                        }
                        echo form_dropdown('created_by', $tr4, (isset($_GET['created_by']) ? $_GET['created_by'] : ""), 'class="form-control" id="created_by"');
                        echo '
                                </div>
                            </div>
                        ';                        

                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("product", "product").'
                        ';
                        $tr5[''] = lang("All_Products");
                        for ($i=0;$i<count($api_product);$i++) {
                            if ($api_product[$i]['product_name'] != '')
                                $tr5[$api_product[$i]['id']] = $api_product[$i]['product_name'];
                        }
                        echo form_dropdown('product', $tr5, (isset($_GET['product']) ? $_GET['product'] : ""), 'class="form-control" id="product"');
                        echo '
                                </div>
                            </div>
                        ';


                        echo '
                            <div class="col-md-3">
                                <div class="form-group">
                                '.lang("reference_no", "reference_no").'
                                <input type="text" class="form-control" id="search" name="search" value="'.(isset($_GET['search']) ? $_GET['search'] : "").'"  />
                        ';
                        echo '
                                </div>
                            </div>
                        </div>
                        ';

?>                        
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group pull-right api_padding_left_10">
                                    <a href="<?php echo base_url().'admin/products/quantity_adjustments'; ?>">
                                        <input type="button" name="reset" value="reset" class="btn btn-danger">
                                    </a>
                                </div>
                                <div class="form-group pull-right">
                                    <input type="submit" id="api_submit" name="submit" value="Submit" class="btn btn-primary">
                                </div>
                            </div>
                        </div>

                <div class="table-responsive">
                    <div id="dmpData_wrapper" class="dataTables_wrapper form-inline" role="grid">


                        <div class="row">
                            <div class="col-md-12 text-left">
                                <div class="short-result">
                                     <label>Show</label>
                                         <select  id="sel_id" name="per_page" style="width: 80px;" onchange="this.form.submit()">
                                             <?php
                                                 $per_page_arr = array(
                                                     '10'=>'10',
                                                     '25'=>'25',
                                                     '50'=> '50',
                                                     '100'=>'100',
                                                     '200'=>'200',
                                                     '500'=>'500',
                                                     '1000'=>'1000',
                                                     'All'=>$total_rows_sale
                                                 );
                                                
                                                 foreach($per_page_arr as $key =>$value){
                                                     $select = $value == $per_page?'selected':'';
                                                     echo '<option value="'.$value.'" '.$select.'>';
                                                             echo $key;
                                                     echo '</option>';
                                                 }

                                             ?>
                                         </select>
                                 </div>
                             </div>

                            <div class="api_clear_both api_height_15"></div>

                        </div>     
                    </div>   
                    
                    <table id="" class="table table-bordered table-condensed table-hover table-striped dataTable" aria-describedby="dmpData_info">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th class="pointer" style="width:80px;"> 
                                <?php if($sort_by == "date" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('date', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("date"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "date" and $sort_order=="DESC"){?>    
                                    <i class="font-normal" onclick="formactionsubmit('date', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("date"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('date', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("date"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>
                            <th class="pointer"> 
                                <?php if($sort_by == "reference_no" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('reference_no', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("reference_no"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "reference_no" and $sort_order=="DESC"){?>    
                                    <i class="font-normal" onclick="formactionsubmit('reference_no', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("reference_no"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('reference_no', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("reference_no"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>
                            <th class="pointer"> 
                                <?php if($sort_by == "warehouse_name" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('warehouse_name', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("warehouse"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "warehouse_name" and $sort_order=="DESC"){?>    
                                    <i class="font-normal" onclick="formactionsubmit('warehouse_name', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("warehouse"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('warehouse_name', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("warehouse"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>
                            <th class="">
                                <?php
                                    echo lang('Details');
                                ?>
                            </th>
                            <th class="pointer"> 
                                <?php if($sort_by == "created_by" and $sort_order=="ASC"){?>
                                    <i class="font-normal" onclick="formactionsubmit('created_by', 'DESC')" id="fa_sort"  aria-hidden="true"><?php echo lang("created_by"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-down"></span></i>
                                <?php }elseif($sort_by == "created_by" and $sort_order=="DESC"){?>    
                                    <i class="font-normal" onclick="formactionsubmit('created_by', 'ASC')" id="fa_sort_up"  aria-hidden="true"><?php echo lang("created_by"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort-up"></span></i>
                                <?php } else{?> 
                                    <i class="font-normal" onclick="formactionsubmit('created_by', 'DESC')" id="fa_sort_down" aria-hidden="true"><?php echo lang("created_by"); ?>&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-sort"></span></i>
                                <?php } ?>  
                            </th>
                            <th style="min-width:65px; text-align:center;"><?= lang("actions") ?></th>
                        </tr>
                        </thead>
                        <tbody role="alert" aria-live="polite" aria-relevant="all">

                            <?php
for ($i=0;$i<count($select_data);$i++) {

                                    $delete_link = "<a href='#' class='tip po' title='<b>" . lang("delete") . "</b>' data-content=\"<p>"
                                    . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/delete_adjustment/$1') . "'>"
                                    . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                                    . lang('delete_product') . "</a>";

                                    ?>
                                <tr id='<?php echo $select_data[$i]['id']; ?>' class="adjustment_link">
                                    <td style="min-width:30px; width: 30px; text-align: center;">
                                        <div class="text-center">
                                            <?php
                                            echo '
                                            <input class="checkbox multi-select skip" type="checkbox" name="val[]" value="'.$select_data[$i]['id'].'" onclick="api_setValueCheckbox(\'val[]\',\'check_value\',\'action-form\',\'-\')"/>
                                            ';
                                            ?>
                                        </div>
                                    </td>
                                    <td align="center">
                                        <?= date('d/m/Y H:i:s', strtotime($select_data[$i]['date'])) ?>
                                    </td>
                                    <td align="center"><?= $select_data[$i]['reference_no'] ?></td>
                                    <td align="center"><?= $select_data[$i]['warehouse_name'] ?></td>
                                    <td class="api">
<?php
    $temp_2 = $this->site->api_select_some_fields_with_where("
        *
        "
        ,"sma_adjustment_items"
        ,"adjustment_id = ".$select_data[$i]['id']." order by id asc limit 11"
        ,"arr"
    );
    for ($i2=0;$i2<count($temp_2);$i2++) {
        if ($i2 < 10) {
            $temp_3 = $this->site->api_select_some_fields_with_where("
                id, name, code
                "
                ,"sma_products"
                ,"id = ".$temp_2[$i2]['product_id']
                ,"arr"
            );
            if (floor($temp_2[$i2]['quantity']) != $temp_2[$i2]['quantity'])
                $temp = number_format($temp_2[$i2]['quantity'],2);
            else
                $temp = number_format($temp_2[$i2]['quantity'],0);

            if ($temp_2[$i2]['type'] == 'addition') {
                $temp = '<span class="label label-success">+ '.$temp.'</span>';
            }
            else
                $temp = '<span class="label label-danger">- '.$temp.'</span>';

            if ($temp_2[$i2]['quantity'] == 0)
                $temp = '<span class="label label-default">0</span>';


            if (count($temp_3) > 0)
                echo $temp_3[0]['name'].' ('.$temp_3[0]['code'].')&nbsp;&nbsp; '.$temp.'<br><br>';
            else
                echo 'N/A ('.lang('Product_deleted').')&nbsp;&nbsp; '.$temp.'<br><br>';
        }
        else
            echo 'etc...';
    }

?>
                                    </td>
                                    <td align="center"><?= $select_data[$i]['created_by_name'] ?></td>

                                    <td style="width:80px; text-align:center;">

<?php

    echo '
        <a class="tip api_padding_right_5" href="'.base_url().'admin/products?adjustment_track='.$select_data[$i]['id'].'" target="_blank" title="'.lang('View_Adjustment_Products').'">
            <i class="fa fa-eye"></i>
        </a>
    ';
?>

                           <a href="#" class="bpo" title="<b><?= lang("Delete_Adjustment") ?></b>"
                                data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='<?php echo $select_data[$i]['id']; ?>' onclick='api_temp_delete(this.id);'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                data-html="true" data-placement="left">
                            <i class="fa fa-trash-o"></i>

                                    </td>                                    
                                </tr>   
                                    <?php
}
                            ?>
<?php
if (count($select_data) <= 0) {
    $temp = 7;
    echo '
    <tr>
    <td colspan="'.$temp.'">
        '.lang('No_record_found').'
    </td>
    </tr>
    ';
}
?>                            
                        </tbody>

                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft skip" type="checkbox" name="check" onclick="api_select_check_all(this,'val[]'); api_setValueCheckbox('val[]','check_value','action-form','-')"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="width:65px; text-align:center;"><?= lang("actions") ?></th>
                        </tr>
                        </tfoot>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel-heading-left">
                                <p class="data-total">
                                    <?php echo $show;?>
                                </p>
                            </div>   
                            <div class="panel-heading-right">
                                <p>
                                   <?php echo $this->pagination->create_links(); //pagination links ?>
                                </p>
                            </div>  
                            <div class="panel-heading-clear-fix"></div>   
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="warehouse_id" value="<?php echo $warehouse_id;?>" id="warehouse_id"/>
<input type="hidden" name="show_data" value="<?php echo $show_data;?>" id="show_data"/>
<input type="hidden" name="sort_by" value="<?php echo $sort_by;?>" id="sort_by"/>
<input type="hidden" name="sort_order" value="<?php echo $sort_order;?>" id="sort_order"/>
<?php
echo form_close();

echo admin_form_open('products/adjustment_actions','id="api_form_action" name="api_form_action" method="post"');
echo '
    <div class="api_display_none">
            <input type="text" name="check_value" value="" id="check_value"/>
            <input type="text" name="api_action" value="" id="api_action"/>
            <button type="button" onclick="document.api_form_action.submit();" value="ok">ok</button>
    </div>
';
echo form_close();
?>

<style>    
    .modal-body h2{
        font-size: 20px;
        margin-top: 8px;
    }
    .modal-body p{
        font-size: 18px;
        margin-bottom: 17px;
    }
    #tbl-update_to_due {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_paid {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    #tbl-update_to_partail {
        position: relative;
        padding: 0px 15px 15px 15px !important;
        margin-top:15px;
    }
    h4.modal-title{
        font-size: 18px;
    }
    .panel-heading-left {
        float: left;
    }
    .panel-heading-right {
        float: right;
    }
    .pointer{
        cursor:pointer;
    }
    .font-normal{
        font-style: normal;
    }
    
    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 5px 10px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #428bca;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        font-size: 13px;
    }
    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: -5px 0;
        border-radius: 4px;
    }
    .txt-f{
        color:#333 !important
    }
    #loading{
        display:none !important;
        visibility : hidden;
    }
</style>

<script>
     
function formactionsubmit(sort_by, sort_order) {
    document.getElementById("sort_by").value = sort_by;
    document.getElementById("sort_order").value = sort_order;
    $("#api_submit").click();
}


function api_bulk_actions(action){
    $("#api_action").val(action);
    $("#api_form_action").submit();
} 
function bulk_actions_delete(){
    document.api_form_action.api_action.value = 'delete';
    $("#api_action").val('delete');
    $("#api_form_action").submit();
} 
function api_temp_delete(id){
    window.location = '<?= base_url().'admin/products/delete_adjustment/' ?>' + id;
} 

<?php
    $temp = $_GET['page'];
    if ($temp == '') $temp = 1;
    echo '
        $(".pagination .active").html(\'<a href="javascript:void(0);">'.$temp.'</a>\');
    ';
?>

$('body').on('click', '.adjustment_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
    $('#myModal').modal({remote: '<?php echo base_url(); ?>admin/products/view_adjustment/' + $(this).parent('.adjustment_link').attr('id')});
    $('#myModal').modal('show');
});

</script>
<style type="text/css">
    .adjustment_link {
        cursor: pointer;
    }    
    .adjustment_link td:nth-child(0), .adjustment_link td:nth-child(2), .adjustment_link td:nth-child(7){
        cursor: default;
    }
</style>


<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php
    if ($select_data[0]['id'] != '') {
        $temp_1 = lang('Edit').' '.$api_list_view['object_title'];
        $temp_2 = '/'.$select_data[0]['id'];
        $temp_3 = lang('Update');
        $temp_4 = '';
        $temp_5 = '';
        $temp_6 = 'api_display_none';
    }
    else {
        $temp_1 = lang('Add').' '.$api_list_view['object_title'];
        $temp_2 = '';
        $temp_3 = lang('Add');
        $temp_4 = '';
        $temp_5 = '';
        $temp_6 = '';
    }
?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $temp_1; ?></h4>
        </div>
        <?php         

        $attrib = array('data-toggle' => 'validator', 'role' => 'form',  'id' => $form_name, 'name' => $form_name);
        echo admin_form_open_multipart($api_list_view['controller']."/edit_".$api_list_view['object_name'].$temp_2, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                          
<?php

echo '
    <div class="col-md-12">
        <div class="form-group">
            '.lang("name", "name").' *
            <input type="text" class="form-control" name="name" required="required" value="'.$select_data[0]['name'].'"  />
        </div>
    </div>
';

if ($select_data[0]['start_date'] == '')
    $select_data[0]['start_date'] = date('Y-m-d');
echo '
<div class="col-md-6">
    <div class="form-group">
        '.lang("start_date", "start_date").'
        '.form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $this->sma->hrld($select_data[0]['start_date'])), 'class="form-control datetime" id="'.$form_name.'_start_date" autocomplete="off"').'
    </div>
</div>
';

if ($select_data[0]['end_date'] == '')
    $select_data[0]['end_date'] = date('Y-m-d');    
echo '
<div class="col-md-6">
    <div class="form-group">
        '.lang("end_date", "end_date").'
        '.form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $this->sma->hrld($select_data[0]['end_date'])), 'class="form-control datetime" id="'.$form_name.'_end_date" autocomplete="off"').'
    </div>
</div>
';


echo '
<div class="col-md-6">
    <div class="form-group">
        <label class="control-label">
            '.lang("Discount_Type").'
        </label>
        <div class="controls"> 
';
            $type_discount['percentage'] = lang('Percentage');
            $type_discount['fixed_price'] = lang('Fixed Price');
            echo form_dropdown('type_discount', $type_discount, $select_data[0]['type_discount'], 'class="form-control" id="'.$form_name.'_type_discount" onchange="'.$form_name.'_type_discount_change();";');
echo '
        </div>
    </div>    
</div>
';

if ($select_data[0]['min_qty'] == '')
    $select_data[0]['min_qty'] = 0;
echo '
<div class="col-md-6">
    <div class="form-group">
        '.lang("Minimum_Qty_Order", "Minimum_Qty_Order").'
        '.form_input('min_qty', (isset($_POST['min_qty']) ? $_POST['min_qty'] : $select_data[0]['min_qty']), 'class="form-control api_numberic_input" autocomplete="off"').'
    </div>
</div>
';

echo '
    <div class="col-md-6">
        <div class="form-group">
            '.lang("Discount_Rate", "Discount_Rate").'
            <div class="controls"> 
';
            for ($i=0;$i<=100;$i++) {
                $tr_rate[$i] = $i.'%';
            }
            echo form_dropdown('rate', $tr_rate, (isset($_POST['rate']) ? $_POST['rate'] : ($select_data[0]['rate'] ? $select_data[0]['rate'] : 0)), 'class="form-control" id="'.$form_name.'_rate"');
echo '
            </div>
        </div> 
    </div>
';

echo '
    <div class="col-md-6">
        <div class="form-group all">
            '.lang("Fixed_Price", "Fixed_Price").'
            <div class="input-group">
                '.form_input('fixed_price', (isset($_POST['fixed_price']) ? $_POST['fixed_price'] : ($select_data[0]['fixed_price'] ? $select_data[0]['fixed_price'] : 0)), 'class="form-control api_numberic_input" id="'.$form_name.'_fixed_price"').'
                <span class="input-group-addon" style="padding: 1px 10px;">
                    <i class="fa fa-usd"></i>
                </span>                            
            </div>
        </div>
    </div>
';




if ($select_data[0]['status'] == '' || $select_data[0]['status'] == 'enabled') {
    $temp_1 = set_radio('status', 'enabled', TRUE); 
    $temp_2 = set_radio('status', 'disabled');
}
else {
    $temp_1 = set_radio('status', 'enabled');
    $temp_2 = set_radio('status', 'disabled', TRUE);                                
}
echo '
<div class="form-group col-md-12">
    '.lang("Status", "Status").'
    <div class="">
        <div class="api_height_10"></div>
        <input type="radio" id="" name="status" value="enabled" '.$temp_1.' />
        '.lang('Enabled').'
        <span class="api_padding_left_10">
            <input type="radio" id="" name="status" value="disabled" '.$temp_2.' />
        </span>
        '.lang('Disabled').'
    </div>
</div>                
';

?>

            </div>

        </div>
        <div class="modal-footer">
            <?php
                echo form_submit('Add', $temp_3, 'class="btn btn-primary"'); 
            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script src="<?php echo base_url(); ?>assets/api/js/public.js"></script>

<script>
<?php
echo '
function '.$form_name.'_type_discount_change() {
    if (document.getElementById("'.$form_name.'_type_discount").value == "percentage") {
        $("#'.$form_name.'_rate").removeAttr("disabled");
        $("#'.$form_name.'_fixed_price").attr("disabled","disabled");
    }
    if (document.getElementById("'.$form_name.'_type_discount").value == "fixed_price") {
        $("#'.$form_name.'_rate").attr("disabled","disabled");
        $("#'.$form_name.'_fixed_price").removeAttr("disabled");
    }
}
'.$form_name.'_type_discount_change();
';
?>
</script>










<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>

            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-12">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?= lang("Warehouse"); ?>: <?= lang($inv->warehouse_name); ?><br>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">

                    <thead>

                    <tr>
                        <th><?= lang("no"); ?></th>
                        <th><?= lang("Product Name (Code, Variant)"); ?></th>
                        <th align="right"><?= lang("Before_Adjustment"); ?></th>
                        <th align="right"><?= lang("quantity"); ?></th>
                        <th align="right"><?= lang("After_Adjustment"); ?></th>
                        <th align="right">
                            <?= lang("Modification_Qty_From").'<br>'.$this->sma->hrld($inv->date); ?>
                        </th>
                        <th><?= lang("Current_Quantity"); ?></th>
                    </tr>

                    </thead>

                    <tbody>

                    <?php 
                    $r = 1;
                    foreach ($rows as $row):
                    ?>
                        <tr>
                            <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                            <td style="vertical-align:middle;">
                                <?php 
                                if ($row->variant != '')
                                    $temp_variant = ', '.$row->variant;

                                if ($row->product_code != '')
                                    echo $row->product_name.' ('.$row->product_code.$temp_variant.')';
                                else
                                    echo 'N/A ('.lang('Product_delected').')';
                                ?>
                            </td>
                            <td align="right" class="api_td_width_auto">
                                <?= $this->site->api_number_format($row->quantity_before, 2) ?>
                            </td>
                            <td class="api_td_width_auto" style="text-align:right; vertical-align:middle;">
                                <?php

                                if ($row->quantity > 0) {
                                    if ($row->type == 'addition')
                                        echo '
                                            <span class="label label-success">+ '.$this->site->api_number_format($row->quantity, 2).'</span>
                                        ';
                                    if ($row->type == 'subtraction')
                                        echo '
                                            <span class="label label-danger">
                                                - '.$this->site->api_number_format($row->quantity, 2).'
                                            </span>
                                        ';
                                }
                                else
                                    echo '
                                        <span class="label label-default">
                                            0
                                        </span>
                                    ';

                                ?>
                            </td>
                            <td align="right" class="api_td_width_auto">
                                <?= $this->site->api_number_format($row->quantity_after, 2) ?>
                            </td>
                            <td align="right" class="api_td_width_auto">
                                <?php

    //---------------------------------------------
    if ($inv->{'adjustment_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';        
    if ($inv->{'adjustment_'.$row->product_id} > 0)
        $temp_display = '
            <a href="'.base_url().'admin/products/quantity_adjustments?adjustment_track='.$inv->{'adjustment_track_'.$row->product_id}.'" target="_blank">
                <span class="label label-success">+'.$inv->{'adjustment_'.$row->product_id}.'</span>
            </a>
        ';
    if ($inv->{'adjustment_'.$row->product_id} < 0)
        $temp_display = '
            <a href="'.base_url().'admin/products/quantity_adjustments?adjustment_track='.$inv->{'adjustment_track_'.$row->product_id}.'" target="_blank">
                <span class="label label-danger">'.$inv->{'adjustment_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Adjustment').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------

    //---------------------------------------------
    if ($inv->{'sale_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';
    else
        $temp_display = '
            <a href="'.base_url().'admin/sales?adjustment_sale_track='.$inv->{'adjustment_sale_track'.$row->product_id}.'" target="_blank">
                <span class="label label-danger">- '.$inv->{'sale_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Sale').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------

    //---------------------------------------------
    if ($inv->{'consignment_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';
    else
        $temp_display = '
            <a href="'.base_url().'admin/consignment?adjustment_consignment_track='.$inv->{'adjustment_consignment_track'.$row->product_id}.'" target="_blank">
                <span class="label label-danger">- '.$inv->{'consignment_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Sale_Consignment').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------

    //---------------------------------------------
    if ($inv->{'purchase_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';
    else
        $temp_display = '
            <a href="'.base_url().'admin/purchases?adjustment_purchase_track='.$inv->{'adjustment_purchase_track'.$row->product_id}.'" target="_blank">
                <span class="label label-success">+ '.$inv->{'purchase_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Purchase').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------

    //---------------------------------------------
    if ($inv->{'consignment_purchase_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';
    else
        $temp_display = '
            <a href="'.base_url().'admin/purchases?mode=consignment&adjustment_consignment_purchase_track='.$inv->{'adjustment_consignment_purchase_track'.$row->product_id}.'" target="_blank">
                <span class="label label-success">+ '.$inv->{'consignment_purchase_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Purchase_Consignment').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------

    //---------------------------------------------
    if ($inv->{'transfer_'.$row->product_id} == '')
        $temp_display = '
            <span class="label label-default">0</span>
        ';        
    if ($inv->{'transfer_'.$row->product_id} > 0)
        $temp_display = '
            <a href="'.base_url().'admin/transfers?adjustment_transfer_track='.$inv->{'adjustment_transfer_track_'.$row->product_id}.'" target="_blank">
                <span class="label label-success">+'.$inv->{'transfer_'.$row->product_id}.'</span>
            </a>
        ';
    if ($inv->{'transfer_'.$row->product_id} < 0)
        $temp_display = '
            <a href="'.base_url().'admin/transfers?adjustment_transfer_track='.$inv->{'adjustment_transfer_track_'.$row->product_id}.'" target="_blank">
                <span class="label label-danger">'.$inv->{'transfer_'.$row->product_id}.'</span>
            </a>
        ';
    echo '
        <div>
            '.lang('Transfer').':
            '.$temp_display.'
        </div>
    ';
    //---------------------------------------------


                                ?>
                            </td>
                            <td align="right" class="api_td_width_auto">
                            <?php
                                $config_data = array(
                                    'table_name' => 'sma_products',
                                    'select_table' => 'sma_products',
                                    'translate' => '',
                                    'select_condition' => "id = ".$row->product_id,
                                );
                                $temp = $this->site->api_select_data_v2($config_data);
                                if ($inv->warehouse_id == 1)
                                    echo $this->site->api_number_format($temp[0]['quantity'], 2);
                                else
                                    echo $this->site->api_number_format($temp[0]['quantity_'.$inv->warehouse_id], 2);
                            ?>                                
                            </td>                            
                        </tr>
                        <?php
                        $r++;
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>

            <div class="row">
                <div class="col-xs-7">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-xs-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                        <?php if ($inv->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

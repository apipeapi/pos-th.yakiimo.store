<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var api_adjustment_items = '<?php echo $api_adjustment_items?>';    
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    $(document).ready(function () {
        if (localStorage.getItem('remove_qals')) {
            if (localStorage.getItem('qaitems')) {
                localStorage.removeItem('qaitems');
            }
            if (localStorage.getItem('qaref')) {
                localStorage.removeItem('qaref');
            }
            if (localStorage.getItem('qawarehouse')) {
                localStorage.removeItem('qawarehouse');
            }
            if (localStorage.getItem('qanote')) {
                localStorage.removeItem('qanote');
            }
            if (localStorage.getItem('qadate')) {
                localStorage.removeItem('qadate');
            }
            localStorage.removeItem('remove_qals');
        }
        <?php if ($adjustment) { ?>
        localStorage.setItem('qadate', '<?= $this->sma->hrld($adjustment->date); ?>');
        localStorage.setItem('qaref', '<?= $adjustment->reference_no; ?>');
        localStorage.setItem('qawarehouse', '<?= $adjustment->warehouse_id; ?>');
        localStorage.setItem('qanote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($adjustment->note)); ?>');
        localStorage.setItem('qaitems', JSON.stringify(<?= $adjustment_items; ?>));
        localStorage.setItem('remove_qals', '1');
        <?php } ?>
        
        $("#add_item").autocomplete({
            source: '<?= admin_url('products/qa_suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_adjustment_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_adjustment'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/edit_adjustment/".$adjustment->id, $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("date", "qadate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($adjustment->date)), 'class="form-control input-tip datetime" id="qadate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("reference_no", "qaref"); ?>
                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $adjustment->reference_no), 'class="form-control input-tip" id="qaref"'); ?>
                            </div>
                        </div>

                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("warehouse", "qawarehouse"); ?>
                                    <?php
                                    $temp_script = '';
                                    foreach ($warehouses as $warehouse) {
                                        if ($adjustment->warehouse_id == $warehouse->id)
                                            $wh[$warehouse->id] = $warehouse->name;
                                        $temp_script .= 'api_warehouse["'.$warehouse->id.'"] = "'.$warehouse->name.'"; ';
                                    }

                                    if ($_GET['warehouse'] > 0)
                                        $temp = $_GET['warehouse'];
                                    elseif (isset($_POST['warehouse']))
                                        $temp = $_POST['warehouse'];
                                    else
                                        $temp = $adjustment->warehouse_id;

                                    echo form_dropdown('warehouse', $wh, $temp, 'id="qawarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                            <?php } else {
                                $warehouse_input = array(
                                    'type' => 'hidden',
                                    'name' => 'warehouse',
                                    'id' => 'qawarehouse',
                                    'value' => $this->session->userdata('warehouse_id'),
                                    );

                                echo form_input($warehouse_input);
                            } ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>

                        <div class="clearfix"></div>


                        <div class="col-md-12 api_display_none" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("products"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="qaTable" class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>

<th class="col-md-1" style="text-align:right !important;"><?= lang("Total_Qty"); ?></th>
<?php
    for ($i=0;$i<count($api_warehouse);$i++) {
        echo '
            <th class="col-md-1" style="text-align:right !important;">'.lang($api_warehouse[$i]['code']."_Qty").'</th>
        ';
    }
?>                                                   
                                            <th class="col-md-1"><?= lang("Warehouse"); ?></th>
                                            <th class="col-md-2 api_display_none"><?= lang("Variant"); ?></th>
                                            <th class="col-md-1"><?= lang("type"); ?></th>
                                            <th class="col-md-1"><?= lang("quantity"); ?></th>
                                            <th class="col-md-1"><?= lang("Actual_Qty"); ?></th>
                                            <th style="max-width: 30px !important; text-align: center;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>                                        
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("note", "qanote"); ?>
                                    <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        <div class="col-md-12">
                            <div
                                class="fprom-group"><?php echo form_submit('edit_adjustment', lang("submit"), 'id="edit_adjustment" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>

<script>
var api_warehouse = [];
<?php echo $temp_script; ?>

function loadItems() {
    if (localStorage.getItem('qaitems')) {
        count = 1;
        an = 1;
        $("#qaTable tbody").empty();
        qaitems = JSON.parse(localStorage.getItem('qaitems'));
        sortedItems =  qaitems;

        $.each(sortedItems, function () {
            var item = this;
            item.order = item.order ? item.order : new Date().getTime();
            sortedItems[item.item_id].order = item.order;
        });  

        var temp = [];
        var i = 0;
        $.each(sortedItems, function () {
            var item = this;
            temp[i] = item.order;
            i++;
        });

        temp.sort(); 
        var sortedItems2 = [];
        for (var i = 0; i < temp.length; i++) {
            $.each(sortedItems, function () {
                var item = this;
                if (temp[i] == item.order)
                    sortedItems2[i] = item;
            });    
        }
        sortedItems = sortedItems2;
        
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type ? item.row.type : 'subtraction';

            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");
            if(item.options !== false) {
                $.each(item.options, function () {
                    if (item.row.option == this.id)
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    else
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }

            var row_no = item.id;
            var item_quantity = 0;
            if (item.row.quantity)
                item_quantity = item.row.quantity;             

            var item_actual_qty = 0;
            var temp_warehouse_id = $('#qawarehouse').val();
            if (isNaN(item.row.actual_qty))
                item_actual_qty = item.row['quantity_' + temp_warehouse_id] - parseFloat(item_qty);
            else
                item_actual_qty = item.row.actual_qty;

            var temp_disabled_1 = '';
            var temp_disabled_2 = '';
            if (item.type_disabled == 1) {
                if (type == 'subtraction')
                    temp_disabled = '<option value="subtraction" selected>'+type_opt.subtraction+'</option>';
                else
                    temp_disabled = '<option value="addition" selected>'+type_opt.addition+'</option>';
            }
            else
                temp_disabled = '<option value="subtraction"'+(type == 'subtraction' ? ' selected' : '')+'>'+type_opt.subtraction+'</option><option value="addition"'+(type == 'addition' ? ' selected' : '')+'>'+type_opt.addition+'</option>';

            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');

            tr_html = '<td><input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><span class="sname" id="name_' + row_no + '">' + item_name + ' ('+ item_code + ')' + '</span></td>';

            tr_html += '<td align="right">'+ item.row['total_quantity'] + '</td>';
            <?php
                for ($i=0;$i<count($api_warehouse);$i++) {

                    if ($api_warehouse[$i]['id'] == 1)
                        echo '
                            tr_html += \'<td align="right">\'+ item_quantity +\'</td>\';
                        ';
                    else
                        echo '
                            tr_html += \'<td align="right">\'+ item.row[\'quantity_'.$api_warehouse[$i]['id'].'\'] +\'</td>\';
                        ';
                }
            ?>

            tr_html += '<td align="center" class="api_warhouse_name">' + temp_warehouse_id + '</td>';
            tr_html += '<td class="api_display_none">'+(opt.get(0).outerHTML)+'</td>';
            tr_html += '<td><select name="type[]" class="form-contol select rtype" style="width:100%;">' + temp_disabled + '</select></td>';

            tr_html += '<td><input class="form-control text-center rquantity api_numberic_input" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + parseFloat(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onkeyup="api_qty_update(\'' + row_no + '\', \'' + item_actual_qty + '\',\'' + type + '\', \'' + parseFloat(item_qty) + '\');" autocomplete="off"></td>';
            tr_html += '<td><input class="form-control text-center api_numberic_input" name="actual_qty[]" type="text" value="' + parseFloat(item_actual_qty) + '" id="actual_qty_' + row_no + '" onkeyup="api_actual_qty_update(\'' + row_no + '\', \'' + item_actual_qty + '\',\'' + type + '\', \'' + parseFloat(item_qty) + '\');" autocomplete="off"></td>';

            tr_html += '<td class="text-center"><i class="fa fa-times tip qadel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#qaTable");
            count += parseFloat(item_qty);
            an++;
        });
        $('.api_warhouse_name').html(api_warehouse[$('#qawarehouse').val()]);

        var col = 3;
        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'">Total</th><th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
        if (site.settings.product_serial == 1) { tfoot += '<th></th>'; }
        tfoot += '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th></tr>';
        $('#qaTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        set_page_focus();
    }
}

</script>




<div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <i class="fa fa-2x">&times;</i>
        </button>
        <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
            <i class="fa fa-print"></i> <?= lang('print'); ?>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?= $product->product_name; ?></h4>
    </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-xs-5">
                    <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $product->image ?>"
                    alt="<?= $product->product_name ?>" class="img-responsive img-thumbnail"/>

                    <div id="multiimages" class="padding10">
                        <?php if (!empty($images)) {
                            echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                            foreach ($images as $ph) {
                                echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                if ($Owner || $Admin || $GP['products-edit']) {
                                    echo '<a href="#" class="delimg" data-item-id="'.$ph->id.'"><i class="fa fa-times"></i></a>';
                                }
                                echo '</div>';
                            }
                        }
                        ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-7">
                    <div class="table-responsive">
                        <table class="table table-borderless table-striped dfTable table-right-left">
                            <tbody>
                                <tr>
                                    <td colspan="2" style="background-color:#FFF;"></td>
                                </tr>
                                <tr>
                                    <td style="width:30%;"><?= lang("barcode_qrcode"); ?></td>
                                    <td style="width:70%;"><?= $barcode ?>
                                        <?php $this->sma->qrcode('link', urlencode(site_url('products/view/' . $product->id)), 1); ?>
                                        <img
                                        src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                        alt="<?= $product->product_name ?>" class="pull-right"/></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("customer_name"); ?></td>
                                        <td><?= lang($product->customer_name); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("sale_person"); ?></td>
                                        <td><?= $product->sale_person; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("product_code_id"); ?></td>
                                        <td><?= $product->product_code; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("product_name"); ?></td>
                                        <td><?= $product->product_name; ?></td>
                                    </tr>
                                    <?php //if ($product->subcategory_id) { ?>
                                    <tr>
                                        <td><?= lang("the_normal_price"); ?></td>
                                        <td><?= $this->sma->formatMoney($product->normal_price); ?></td>
                                    </tr>
                                    <?php //} ?>
                                    <tr>
                                        <td><?= lang("the_special_price"); ?></td>
                                        <td><?= $this->sma->formatMoney($product->special_price); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("discount_price"); ?></td>
                                        <td><?= $product->discount_price; ?>%</td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("the_start_date"); ?></td>
                                        <td><?= $product->start_date; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("the_end_date"); ?></td>
                                        <td><?= $product->end_date; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= lang("status_product_discount"); ?></td>
                                        <?php if($product->end_date >= (date("Y-m-d"))){?>
                                        <td>Not Expired</td>
                                        <?php }else{?>
                                        <td style="color:red">Expired</td>
                                        <?php     
                                        } ?>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix"></div>
    <div class="col-xs-12">
        <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
        <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
    </div>
</div>
<?php if (!$Supplier || !$Customer) { ?>
    <div class="buttons">
        <div class="btn-group btn-group-justified">
            <?php if($product->type == 'standard') { ?>
                <div class="btn-group"><a data-target="#myModal2" data-toggle="modal"
                    href="<?= site_url('products/add_adjustment/' . $product->id) ?>"
                    class="tip btn btn-warning" title="<?= lang('adjust_quantity') ?>"><i
                    class="fa fa-filter"></i> <span
                    class="hidden-sm hidden-xs"><?= lang('adjust_quantity') ?></span>
                </a>
            </div>
            <?php } ?>
            <div class="btn-group">
                <a data-toggle="modal"  data-target="#myModal2" href="<?= admin_url('system_settings/edit_product_discount/' . $product->id) ?>" class="tip btn btn-warning tip"  title="<?= lang('edit_product_discount') ?>">
                    <i class="fa fa-edit"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                </a>
            </div>
            <div class="btn-group">
                <a href="#" class="tip btn btn-danger bpo" title="<b><?= lang("delete_product_discount") ?></b>"
                    data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('system_settings/delete_product_discount/' . $product->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                    data-html="true" data-placement="top">
                    <i class="fa fa-trash-o"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                </a>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $(document).ready(function () {
        $('.tip').tooltip();
    });
    </script>
<?php } ?>
</div>
</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.change_img').click(function(event) {
        event.preventDefault();
        var img_src = $(this).attr('href');
        $('#pr-image').attr('src', img_src);
        return false;
    });
});
</script>

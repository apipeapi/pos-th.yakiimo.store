<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
<?php
    if ($select_data[0]['id'] != '') {
        $temp_title = lang('edit').' '.lang('Company_Branch');
    }
    else {
        $temp_title = lang('add').' '.lang('Company_Branch');
    }
?>            
            <h4 class="modal-title" id="myModalLabel"><?php echo $temp_title; ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => $form_name, 'name' => $form_name);
        echo admin_form_open_multipart("customers/company_branch_edit/".$select_data[0]['id'], $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
				<div class="col-md-12">
					<div class="form-group">
                        <div class="form-group">
                            <?= lang("Branch_Name", "branch_name"); ?>
                            <?php echo form_input('branch_name', $select_data[0]['title'], 'class="form-control" id="branch_name" required="required"'); ?>
                        </div>
					</div>
				</div>
			    <div class="col-md-12">
					<div class="form-group">
                        <label class="control-label" for="company">
						<?php echo lang("company"); ?> *</label>
                        <div class="controls"> 
							<?php                   

                        $tr[''] = lang("please_select_a_company");
                        foreach ($company as $temp) {
                            if ($temp->company != '')
                                $tr[$temp->id] = $temp->company;
                        }
                        echo form_dropdown('company', $tr, $select_data[0]['parent_id'], 'data-placeholder="'.lang("please_select_a_company").' id="slcustomerId" class="form-control" required="required"');                            
                            ?>
                        </div>
                    </div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
                        <div class="form-group">
                            <?= lang("Branch_Number", "branch_number"); ?>
                            <?php echo form_input('branch_number', $select_data[0]['branch_number'], 'class="form-control" id="branch_number"'); ?>
                        </div>
					</div>
				</div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="form-group">
                            <?= lang("address", "address"); ?>
                            <?php echo form_input('address', $select_data[0]['address'], 'class="form-control" id="address" required="required"'); ?>
                        </div>
                    </div>
                </div>                
            </div>


        </div>
        <div class="modal-footer">
            <?php echo form_submit('button', $temp_title, 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<?php

$this->db->delete('sma_currencies', "rate = 0");

$temp_hour = date('H');

if ($temp_hour >= 8) {
    $temp = $this->site->api_select_some_fields_with_where("
        *
        "
        ,"sma_currencies"
        ,"code = 'KHR' and YEAR(date) = YEAR('".date('Y-m-d')."') and MONTH(date) = MONTH('".date('Y-m-d')."') and DAY(date) = DAY('".date('Y-m-d')."') order by date desc limit 1"
        ,"arr"
    );
    if (count($temp) <= 0) {

        $temp4 = file_get_contents('https://www.nbc.org.kh/english/economic_research/exchange_rate.php');

        $temp2 = explode('Exchange Rate on :',$temp4);
        $temp3 = explode('Official Exchange Rate',$temp2[1]);
        $temp3[0] = str_replace('<font color="#FF3300">', '', $temp3[0]);
        $temp3[0] = str_replace('</font>', '', $temp3[0]);
        $temp3[0] = substr($temp3[0],0,11);

        if (trim($temp3[0]) != date('Y-m-d'))
            $yesterday_kh_rate = 1;
        else
            $yesterday_kh_rate = 0;
                
        $temp2 = explode('Official Exchange Rate :',$temp4);
        $temp3 = explode('KHR / USD',$temp2[1]);
        $temp3[0] = str_replace('<font color="#FF3300">', '', $temp3[0]);
        $temp3[0] = str_replace('</font>', '', $temp3[0]);

        $temp5 = array(
            'date' => date('Y-m-d'),
            'code' => 'KHR',
            'name' => 'Khmer Riel',
            'rate' => trim($temp3[0]),
            'auto_update' => 0,
            'symbol' => 'KHR',
        );
        $this->db->insert('sma_currencies', $temp5);

        $temp = $this->site->api_select_some_fields_with_where("
            id
            "
            ,"sma_currencies"
            ,"code = 'KHR' order by id desc limit 1"
            ,"arr"
        );
        if ($temp[0]['id'] > 0) {
            $config_data = array(
                'table_name' => 'sma_currencies',
                'id_name' => 'id',
                'field_add_ons_name' => 'add_ons',
                'selected_id' => $temp[0]['id'],
                'add_ons_title' => 'yesterday_kh_rate',
                'add_ons_value' => $yesterday_kh_rate,                    
            );
            $this->site->api_update_add_ons_field($config_data);      
        }
    }
}

?>
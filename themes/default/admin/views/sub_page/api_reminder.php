<?php

$select_data = $this->site->api_select_some_fields_with_where("id
    "
    ,"sma_consignment"
    ,"DATE(due_date) = CURDATE() OR CURDATE() = DATE(due_date) - INTERVAL getTranslate(add_ons,'reminder_day','".f_separate."','".v_separate."') DAY"
    ,"arr"
);

$temp = 0;
if (count($select_data) > 0) {
    $temp = 20;
    echo admin_form_open('consignment', 'name="frm_reminder_consignment" method="get"');
    echo '<input type="hidden" name="search" value="Today Reminder" />';    
    echo '
        <div id="af_header_scroll_to_top" title="'.lang('Consignment').'" onclick="document.frm_reminder_consignment.submit();"></div>
        <style>
        #af_header_scroll_to_top {
            background-image:url('.base_url().'assets/images/reminder_bell.gif);
            background-size:100%;    
            position: fixed;
            bottom: 10px;
            width: 50px;
            height:55px;
            right: '.$temp.'px;
            z-index: 99;
            border: none;
            outline: none;
            cursor: pointer;
        }
        </style>
    ';
    echo '</form>';
}

$select_data = $this->site->api_select_some_fields_with_where("id
    "
    ,"sma_purchases"
    ,"DATE(due_date) = CURDATE() OR CURDATE() = DATE(due_date) - INTERVAL 3 DAY"
    ,"arr"
);

$temp2 = 0;
if (count($select_data) > 0) {
    if ($temp == 20)
        $temp2 = 70;
    else
        $temp2 = 20;
    echo admin_form_open('purchases', 'name="frm_reminder_purchase" method="get"');
    echo '<input type="hidden" name="search" value="Today Reminder" />';
    echo '
        <div id="af_header_scroll_to_top_2" title="'.lang('Purchase').'" onclick="document.frm_reminder_purchase.submit();"></div>
        <style>
        #af_header_scroll_to_top_2 {
            background-image:url('.base_url().'assets/images/reminder_bell_2.gif);
            background-size:100%;    
            position: fixed;
            bottom: 10px;
            width: 50px;
            height:55px;
            right: '.$temp2.'px;
            z-index: 99;
            border: none;
            outline: none;
            cursor: pointer;
        }
        </style>
    ';
    echo '</form>';
}

$select_data = $this->site->api_select_some_fields_with_where("*
    "
    ,"sma_currencies"
    ,"add_ons like '%:yesterday_kh_rate:{1}:%' order by date desc limit 1"
    ,"arr"
);
$temp3 = 0;

if (count($select_data) > 0) {
    if ($temp == 0 && $temp2 == 0)
        $temp3 = 20;        

    if ($temp == 20 && $temp2 == 0)
        $temp3 = 70;

    if ($temp == 20 && $temp2 == 70)
        $temp3 = 130;

    if ($temp2 == 20)
        $temp3 = 70;    
    echo '
        <div id="af_header_scroll_to_top_3" title="'.lang('Check_Currency').'" onclick="window.location=\''.base_url().'admin/system_settings/currencies\';"></div>
        <style>
        #af_header_scroll_to_top_3 {
            background-image:url('.base_url().'assets/images/dollar.gif);
            background-size:100%;    
            position: fixed;
            bottom: 20px;
            width: 25px;
            height:36px;
            right: '.$temp3.'px;
            z-index: 99;
            border: none;
            outline: none;
            cursor: pointer;
        }
        </style>
    ';
}

?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<section class="page-contents page-products">

<div class="container">
            <div class="col-sm-12 col-md-12">
                        <div id="loading">
                            <div class="wave">
                                <div class="rect rect1"></div>
                                <div class="rect rect2"></div>
                                <div class="rect rect3"></div>
                                <div class="rect rect4"></div>
                                <div class="rect rect5"></div>
                            </div>
                        </div>
                        <div id="grid-selector">
                            <div id="grid-menu" class="hidden-xs hidden-sm">
                                <?= lang('grid'); ?>:
                                <ul>
                                    <li class="two-col active"><i class="fa fa-th-large"></i></li>
                                    <li class="three-col"><i class="fa fa-th"></i></li>
                                </ul>
                            </div>
                            <div id="grid-sort">
                                <?= lang('sort'); ?>:
                                <div class="sort">
                                    <select name="sorting" id="sorting" class="selectpicker" data-style="btn-sm" data-width="150px">
                                        <option value="name-asc"><?= lang('name_asc'); ?></option>
                                        <option value="name-desc"><?= lang('name_desc'); ?></option>
                                        <option value="price-asc"><?= lang('price_asc'); ?></option>
                                        <option value="price-desc"><?= lang('price_desc'); ?></option>
                                        <option value="id-desc"><?= lang('id_desc'); ?></option>
                                        <option value="id-asc"><?= lang('id_asc'); ?></option>
                                        <option value="views-desc"><?= lang('views_desc'); ?></option>
                                        <option value="views-asc"><?= lang('views_asc'); ?></option>
                                    </select>
                                </div>
                            </div>
                            <span class="page-info"></span>
                        </div>

                        <div class="clearfix"></div>
                        <div id="results" class="grid"></div>
                        <div class="clearfix"></div>

                        <div class="col-md-6">
                            <span class="page-info line-height-xl hidden-xs hidden-sm"></span>
                        </div>
                        <div class="col-md-6">
                            <div id="pagination" class="pagination-right"></div>
                        </div>

        </div>
</div>
</section>

<style>
@media screen and (min-width: 100px) and (max-width:768px) {
.product-name{
    min-height: 40px;
    max-height: 40px;
    overflow:hidden;
}
.product{
    min-height: 352px;
    max-height: 352px;    
}
}
@media screen and (min-width: 769px){
.product{
    min-height: 260px;
}
}

</style>




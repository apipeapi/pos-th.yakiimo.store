<?php

$temp_hour = date('H');

if ($temp_hour >= 12) {
    $temp = $this->site->api_select_some_fields_with_where("
        *
        "
        ,"sma_currencies"
        ,"code = 'KHR' and YEAR(date) = YEAR('".date('Y-m-d')."') and MONTH(date) = MONTH('".date('Y-m-d')."') order by date desc limit 1"
        ,"arr"
    );
    if (count($temp) > 0) {
        $temp2 = $this->site->api_select_some_fields_with_where("
            *
            "
            ,"sma_currencies"
            ,"code = 'KHR' and YEAR(date) = YEAR('".date('Y-m-d')."') and MONTH(date) = MONTH('".date('Y-m-d')."') and DAY(date) = DAY('".date('Y-m-d')."') and rate != 0 order by date desc limit 1"
            ,"arr"
        );
        if (count($temp2) > 0) {
            //echo $temp2[0]['rate'].'=';
            
        }
        else {
            $temp4 = file_get_contents('https://www.nbc.org.kh/english/economic_research/exchange_rate.php');
            $temp2 = explode('Official Exchange Rate :',$temp4);
            $temp3 = explode('KHR / USD',$temp2[1]);
            $temp3[0] = str_replace('<font color="#FF3300">', '', $temp3[0]);
            $temp3[0] = str_replace('</font>', '', $temp3[0]);

            $temp5 = array(
                'date' => date('Y-m-d'),
                'code' => 'KHR',
                'name' => 'Khmer Riel',        
                'rate' => trim($temp3[0]),
                'auto_update' => 0,
                'symbol' => 'KHR',
            );
            $this->db->update('sma_currencies', $temp5, 'id = '.$temp[0]['id']);
        }
    }
    else {

        
        $temp4 = file_get_contents('https://www.nbc.org.kh/english/economic_research/exchange_rate.php');
        $temp2 = explode('Official Exchange Rate :',$temp4);
        $temp3 = explode('KHR / USD',$temp2[1]);
        $temp3[0] = str_replace('<font color="#FF3300">', '', $temp3[0]);
        $temp3[0] = str_replace('</font>', '', $temp3[0]);

        $temp5 = array(
            'date' => date('Y-m-d'),
            'code' => 'KHR',
            'name' => 'Khmer Riel',        
            'rate' => trim($temp3[0]),
            'auto_update' => 0,
            'symbol' => 'KHR',
        );
        $this->db->insert('sma_currencies', $temp5);

    }
}

?>
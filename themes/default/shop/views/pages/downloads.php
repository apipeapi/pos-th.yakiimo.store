<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-download margin-right-sm"></i> <?= lang('downloads'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -10px;-10px">
                                    <img src="<?= base_url().'assets/api/image/af_header_menu_yamaha_toggle_icon.png' ?>" />
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>

    <?php
    if (!empty($downloads)) {
        echo '<table class="table table-striped table-hover table-va-middle">';
        echo '<thead><tr><th>'.lang('description').'</th><th class="text-center">'.lang('actions').'</th></tr></thead>';
        $r = 1;
        foreach ($downloads as $download) {
            ?>
            <tr class="product">
                <td class="col-xs-9"><?= $download->product_code.' - '.$download->product_name; ?></td>
                <td class="col-xs-3 text-center">
                    <div class="btn-group" role="group">
                        <a href="<?= shop_url('shop/downloads/'.$download->product_id.'/'.md5($download->product_id)); ?>" class="btn btn-sm btn-theme"><i class="fa fa-shopping-cart"></i> <?= lang('download'); ?></a>
                    </div>
                </td>
            </tr>
            <?php
            $r++;
        }
        echo '</table>';
        ?>
        <div class="row" style="margin-top:15px;">
            <div class="col-md-6">
                <span class="page-info line-height-xl hidden-xs hidden-sm">
                    <?= str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')); ?>
                </span>
            </div>
            <div class="col-md-6">
            <div id="pagination" class="pagination-right"><?= $pagination; ?></div>
            </div>
        </div>
        <?php
    } else {
        echo '<strong>'.lang('no_data_to_display').'</strong>';
    }
    ?>

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                            

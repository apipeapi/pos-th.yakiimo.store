<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-building margin-right-sm"></i> <?= lang('my_addresses'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_13"></div>

    <?php
    if ($this->Settings->indian_gst) { $istates = $this->gst->getIndianStates(); }
    if (!empty($addresses)) {
        echo '<div class="row">';
        echo '<div class="col-sm-12 text-bold">'.lang('select_address_to_edit').'</div>';
        $r = 1;
        foreach ($addresses as $address) {
            ?>
            <div class="col-sm-6">
                <a href="#" class="link-address edit-address" data-id="<?= $address->id; ?>">
                        <?= $address->line1; ?><br>
                        <?= $address->line2; ?><br>
                        <?= $address->city; ?>
                        <?= $this->Settings->indian_gst && isset($istates[$address->state]) ? $istates[$address->state].' - '.$address->state : $address->state; ?><br>
                        <?= $address->postal_code; ?> <?= $address->country; ?><br>
                        <?= lang('phone').': '.$address->phone; ?>
                        <span class="count"><i><?= $r; ?></i></span>
                        <span class="edit"><i class="fa fa-edit"></i></span>
                    </a>
            </div>
            <?php
            $r++;
        }
        echo '</div>';
    }
    if (count($addresses) < 6) {
        echo '<div class="row margin-top-lg">';
        echo '<div class="col-sm-12"><a href="#" id="add-address" class="btn btn-primary btn-sm">'.lang('add_address').'</a></div>';
        echo '</div>';
    }
    if ($this->Settings->indian_gst) {
    ?>
    <script>
        var istates = <?= json_encode($istates); ?>
    </script>
    <?php
    } else {
        echo '<script>var istates = false; </script>';
    }
    ?>

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                            

<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-9 col-md-10 api_padding_right_0_pc">
                        
                        <div class="col-md-12 api_padding_0_mobile">
                            <h4 class="margin-top-md title text-bold">
                                <span><i class="fa fa-heart-o margin-right-sm"></i> <?= lang('wishlist'); ?></span>            
                                <div id="api_customer_menu_panel_btn" class="api_float_right api_pointer api_screen_show_768" onclick="$('#api_customer_menu_panel').toggle('slideToggle?');" style="top: -10px; margin-top: -15px;">
                                    <button class="btn btn-danger navbar-btn"><div class="fa fa-bars fa-lg"></div></button>
                                </div>
                            </h4>
                        </div>

                        <div id="api_customer_menu_panel" class="col-md-2 api_padding_right_0_pc api_padding_0_mobile api_screen_hide_768">
                            <div class="api_height_10 api_screen_hide_768"></div>
                            <?php include('themes/default/shop/views/pages/customer_menu_panel.php'); ?>                            
                        </div>
                        <div class="col-md-10 api_padding_0_mobile">
                            <div class="api_height_5"></div>

    <?php
    if (!empty($items)) {

$useragent=$_SERVER['HTTP_USER_AGENT'];
$api_view_array = array();
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {

//=Mobile=====================================================================================    
$temp_display .= '                                
<table id="api_cart_table_mobile" width="100%" class="table table-condensed table-striped table-cart margin-bottom-no" border="0">
';

    if (is_array($items))
    foreach ($items as $item) {
        if ($item->id != '') {
            if ($item->quantity > 0)
                $temp_stock = '<span class="api_color_green">'.lang('in_stock').'</span>';
            else
                $temp_stock = '<span class="api_color_red">'.lang('out_of_stock').'</span>';
$temp_display .= '                                
<tr class="product">
<td valign="top" style="vertical-align: top!important;">
    <table width="100%" border="0">
        <tr>
        <td valign="top" align="center" width="100px;" >
            <a href="'.base_url().'product/'.$item->slug.'">
                <span class="cart-item-image pull-center api_height_80">
                <img src="'.base_url().'assets/uploads/thumbs/'.$item->image.'" alt=""></span>
            </a>
        </td>
        <td valign="top" style="vertical-align: top!important; padding-top:10px;">
            <a href="'.base_url().'product/'.$item->slug.'">
                '.$item->name.'
            </a>
            <div class="api_height_5"></div>
            <div>
                '.$this->sma->formatMoney($item->price).'
            </div>
            '.$temp_stock.'            
        </td>
        </tr>
        <tr>
        <td valign="middle" align="left" class="api_padding_top_10">
            <div class="api_display_inline_block"><strong>'.lang('Order').'</strong> </div> <div class="api_display_inline_block"><strong><input type="tel" id="api_list_order_'.$item->id.'" class="form-control api_numberic_input text-center api_width_50" value="'.$item->order_number.'" onchange="api_change_favorite_order(\''.$item->id.'\',this.value);" ></div>
        </td>    
        <td valign="middle" class="api_padding_top_10">
            <div class="api_float_left api_padding_left_15">
                <div class="api_display_inline_block"><strong>'.lang('qty').'</strong> </div> <div class="api_display_inline_block"><input type="tel" id="api_list_qty_'.$item->id.'" class="form-control api_numberic_input text-center" value="1" style="width:50px !important;"></div>
            </div>        
            <div class="api_float_left api_padding_left_15 api_padding_top_2">
                <button type="button" class="tip btn btn-sm btn-theme add-to-cart-2 api_width_50" data-id="'.$item->id.'" title="'.lang('add_to_cart').'"><i class="fa fa-shopping-cart"></i></button>
            </div>
            <div class="api_float_left api_padding_left_0 api_padding_top_2">
                <button type="button" class="btn btn-sm btn-danger remove-wishlist api_width_50" data-id="'.$item->id.'"><i class="fa fa-trash-o"></i></button>
            </div>
            <div class="api_clear_both"></div>
        </td>    
        </tr>
    </table>
</td>    
</tr>
';
        }
    }

$temp_display .= '                                
</table>
';
echo $temp_display;
//=Mobile=====================================================================================    

}
else {
//=PC=====================================================================================    
        echo '<div class="table-responsive"><table width="100%" class="table table-striped table-hover table-va-middle">';
        echo '<thead><tr><th style="text-align:center;">'.lang('Order').'</th><th>'.lang('photo').'</th><th>'.lang('description').'</th><th style="text-align:center;">'.lang('qty').'</th><th>'.lang('price').'</th>
        <th style="text-align:center;width:1%; white-space:nowrap;">'.lang('in_stock').'</th><th style="text-align:center !important;">'.lang('actions').'</th></tr></thead>';
        $r = 1;
        foreach ($items as $item) {
            ?>
            <tr class="product">
                <td class="col-xs-1">
                    <input type="tel" id="api_list_order_<?php echo $item->id; ?>" class="form-control api_numberic_input text-center" value="<?php echo $item->order_number; ?>" onchange="api_change_favorite_order('<?php echo $item->id; ?>',this.value);">
                </td>
                <td class="col-xs-1">
                <a href="<?php echo base_url().'product/'.$item->slug; ?>">
                <img src="<?= base_url('assets/uploads/thumbs/'.$item->image); ?>" alt="" class="img-responsive">
                </a>
                </td>
                <td class="col-xs-5"><?= '<a href="'.base_url().'product/'.$item->slug.'">'.$item->name.'</a><br>'.$item->details; ?></td>
                <td class="col-xs-1">
                     <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">
                    <input type="tel" id="api_list_qty_<?php echo $item->id; ?>" class="form-control api_numberic_input text-center" value="1" style="width:50px !important;">
                    </div>
                </td>
                <td class="col-xs-1">
                <?php
                $item->promotion = 0;
                if ($item->promotion) {
                    echo '<del class="text-red">'.$this->sma->convertMoney($item->price).'</del><br>';
                    echo $this->sma->convertMoney($item->promo_price);
                } else {
                    echo $this->sma->convertMoney($item->price);
                }
                ?>
                </td>
                <td align="center" class="col-xs-1"><?= $item->quantity > 0 ? lang('yes') : lang('no'); ?></td>
                <td class="col-xs-2">
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                      <div class="btn-group" role="group">
                        <button type="button" class="tip btn btn-sm btn-theme add-to-cart-2" data-id="<?= $item->id; ?>" title="<?= lang('add_to_cart'); ?>"><i class="fa fa-shopping-cart"></i></button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-sm btn-danger remove-wishlist" data-id="<?= $item->id; ?>"><i class="fa fa-trash-o"></i></button>
                    </div>
                </div>
                </td>
            </tr>
            <?php
            $r++;
        }
        echo '</table></div>';
//=PC=====================================================================================    
}


} else {
    echo '<div class="api_height_10"></div><strong>'.lang('wishlist_empty').'</strong>';
}
?>
    

<?php
if (!empty($items)) {
?>
<div class="row" style="margin-top:15px;">
    <div class="col-md-6">
        <span class="page-info line-height-xl hidden-xs hidden-sm">
            <?= str_replace(['_page_', '_total_'], [$page_info['page'], $page_info['total']], lang('page_info')); ?>
        </span>
    </div>
    <div class="col-md-6">
    <div id="pagination" class="pagination-right"><?= $pagination; ?></div>
    </div>
</div>       
<?php
}
?>
    

                        </div>
                    </div>


                    <div class="col-sm-3 col-md-2">
                        <?php include(b'themes/default/shop/views/pages/sidebar2.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                            
<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;

function api_change_favorite_order(id,value){
    var postData = {
        "id" : id,
        "value" : value,
    };
    
    var result = $.ajax
    (
    	{
    		url:site.base_url + "shop/api_change_favorite_order",
    		type: 'GET',
    		secureuri:false,
    		dataType: 'html',
    		data:postData,
    		async: false,
    		error: function (response, status, e)
    		{
    			alert(e);
    		}
    	}
    ).responseText;
//var myWindow = window.open("", "MsgWindow", "width=700, height=400");
//myWindow.document.write(result); 

	var array_data = String(result).split("api-ajax-request-multiple-result-split");
    if (array_data[1] == 'error')
        window.location = "<?php echo base_url(); ?>login";
    else
        window.location = "<?php echo base_url(); ?>shop/wishlist";
}

</script>


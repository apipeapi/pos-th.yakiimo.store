<?php

$select_data = $this->site->api_select_some_fields_with_where("id
    "
    ,"sma_consignment"
    ,"DATE(due_date) = CURDATE() OR CURDATE() = DATE(due_date) - INTERVAL getTranslate(add_ons,'reminder_day','".f_separate."','".v_separate."') DAY"
    ,"arr"
);
if (count($select_data) > 0) {
    $temp = '100';
    echo admin_form_open('consignment', 'name="frm_reminder_consignment" method="get"');
    echo '<input type="hidden" name="search" value="Today Reminder" />';    
    echo '
        <div id="af_header_scroll_to_top" onclick="document.frm_reminder_consignment.submit();"></div>
        <style>
        #af_header_scroll_to_top {
            background-image:url('.base_url().'assets/images/reminder_bell.gif);
            background-size:100%;    
            position: fixed;
            bottom: 10px;
            width: 100px;
            height:105px;
            right: 20px;
            z-index: 99;
            border: none;
            outline: none;
            cursor: pointer;
        }
        </style>
    ';
    echo '</form>';
}
else
    $temp = '20';

$select_data = $this->site->api_select_some_fields_with_where("id
    "
    ,"sma_purchases"
    ,"DATE(due_date) = CURDATE() OR CURDATE() = DATE(due_date) - INTERVAL 3 DAY"
    ,"arr"
);
if (count($select_data) > 0) {
    echo admin_form_open('purchases', 'name="frm_reminder_purchase" method="get"');
    echo '<input type="hidden" name="search" value="Today Reminder" />';
    echo '
        <div id="af_header_scroll_to_top_2" onclick="document.frm_reminder_purchase.submit();"></div>
        <style>
        #af_header_scroll_to_top_2 {
            background-image:url('.base_url().'assets/images/reminder_bell_2.gif);
            background-size:100%;    
            position: fixed;
            bottom: 10px;
            width: 100px;
            height:105px;
            right: '.$temp.'px;
            z-index: 99;
            border: none;
            outline: none;
            cursor: pointer;
        }
        </style>
    ';
    echo '</form>';
}

?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
$api_view_array = array();
if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6';
    $api_view_array['col_class_odd'] = 'api_padding_right_7';
    $api_view_array['col_class_even'] = 'api_padding_left_7';
    $api_view_array['product_class'] = 'api_padding_0';
    $api_view_array['title_class'] = 'api_padding_5';
    $api_view_array['title_style'] = 'min-height:50px; max-height: 50px !important; overflow:hidden;';
    $api_view_array['quantity_class'] = 'api_display_none';
    $api_view_array['price_class_break'] = '<div class="api_clear_both"></div>';
    $api_view_array['price_style'] = 'float: left !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important; text-align: left !important; width: 100% !important;';    
    $api_view_array['select_qty_style'] = 'width: 100% !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important;';    
    $api_view_array['label_qty_class'] = 'api_display_none';
    $api_view_array['select_style'] = 'width:100% !important;';
    $api_view_array['btn_class'] = 'api_padding_right_5 api_padding_left_5';
    $api_view_array['btn_break'] = '<div class="api_clear_both api_height_5"></div>';
    $api_view_array['label_btn'] = lang('Add');
    $api_view_array['carousel_col'] = 2;
    $api_view_array['carousel_col_row'] = 2;
    $api_view_array['carousel_col_row_2'] = 2;
    $api_view_array['select_option'] = '';
    
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5 api_padding_0" style="max-height:75px; min-height:75px; overflow:hidden;">
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>   
    ';
    
    $api_view_array['carousel_featured_script'] = '
$("#carousel-example").carousel({
  interval: 1000 * 15
});
document.getElementById("carousel-example").addEventListener("touchstart", e => {
  touchstartX = e.changedTouches[0].screenX
})
document.getElementById("carousel-example").addEventListener("touchend", e => {
  touchendX = e.changedTouches[0].screenX
  handleGesure("carousel-example")
})
    ';
    for ($i=0;$i<count($main_categories);$i++) {
        if (${'products_'.$main_categories[$i]['id']}) {    
            $api_view_array['carousel_script'] .= '
            
$("#carousel-'.$main_categories[$i]['id'].'").carousel({
  interval: 1000 * 1500
});
document.getElementById("carousel-'.$main_categories[$i]['id'].'").addEventListener("touchstart", e => {
  touchstartX = e.changedTouches[0].screenX
})
document.getElementById("carousel-'.$main_categories[$i]['id'].'").addEventListener("touchend", e => {
  touchendX = e.changedTouches[0].screenX
  handleGesure("carousel-'.$main_categories[$i]['id'].'")
})

            ';
        }
    }
}
else {
    $api_view_array['title_style'] = 'min-height:40px; max-height: 40px !important; overflow:hidden;';
    $api_view_array['col_class'] = 'col-md-4 col-sm-6';
    $api_view_array['label_btn'] = lang('add_to_cart');
    $api_view_array['carousel_col'] = 3;
    $api_view_array['carousel_col_row'] = 3;
    $api_view_array['carousel_col_row_2'] = 3;
    $api_view_array['carousel_script'] = '';
    $api_view_array['select_option'] = '<option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5" style="max-height:70px; overflow:hidden;">
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>    
    ';
}
?>
<?php if(!empty($slider)) { ?>
<section class="slider-container">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-pause="hover">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                        }
                        $sr++;
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" alt="">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                        }
                        $sr++;
                    }
                    ?>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('next'); ?></span>
                </a>
            </div>
        </div>
    </div>
</section>
<?php } ?>

<section class="page-contents">
    <div class="container">
        <div class="row">

            <div class="col-xs-12">

                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg">
                            <?= lang('featured_products'); ?>
                        </h3>
                    </div>
                    <?php
                    if (count($featured_products) > $api_view_array['carousel_col']) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right ">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" id="carousel-example_prev" href="#carousel-example"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" id="carousel-example_next" href="#carousel-example"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>

                <div id="carousel-example" class="carousel slide" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if ($featured_products) { 
                            $r = 0;
                            foreach (array_chunk($featured_products, $api_view_array['carousel_col_row']) as $fps) {
                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>" style="min-height: 315px !important; max-height: 315px !important;">
                                    <div class="">
                                        <?php
                                        $k = 1;
                                        foreach ($fps as $fp) {
if (($k%2) != 0) $class_record = $api_view_array['col_class_odd']; else $class_record = $api_view_array['col_class_even'];                                         
                                            
$temp_sold_out = array();
$temp_sold_out[3] = '<option value="1">1</option>';              
if ($fp->quantity <= 0) {
    $temp_sold_out[0] = 'api_opacity_6';
    $temp_sold_out[1] = '
        <div class="api_absolute_center">
            <div class="api_sold_out_tag" style="visibility:hidden;">
                Sold Out
            </div>
        </div>      
    ';
    $temp_sold_out[2] = 'disabled';
    $temp_sold_out[3] = '<option value="0">0</option>';
    $temp_sold_out[4] = 'api_display_none';
    $temp_sold_out[5] = $api_view_array['sorry'];
}                                            
                                            ?>

<div class="page-products product-container <?= $api_view_array['col_class'].' '.$class_record; ?>">
    <div class="product api_margin_bottom_0 <?= $api_view_array['product_class']; ?>">        
        <div class="product-top"> 
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
                    <img class="img-responsive <?= $temp_sold_out[0]; ?>" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                    <?= $temp_sold_out[1]; ?>
                </a>                
            </div>
            <div class="product-desc <?= $api_view_array['title_class']; ?>" style="<?php echo $api_view_array['title_style']; ?>">
                <div class="product_name" style="font-weight:700;">
                    <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                </div>
                <p></p>          
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price" style="<?= $api_view_array['price_style']; ?>">
                    <?php
                    if ($fp->promotion) {
                        echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price).'</del><br>';
                        echo $this->sma->convertMoney($fp->promo_price);
                    } else {
                        echo $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price);
                    }
                    ?>
                </div>
            <?php } ?>        
            
            <?= $api_view_array['price_class_break']; ?>
            <?= $temp_sold_out[5]; ?>
            <div class="product-rating <?= $temp_sold_out[4]; ?>" style="<?= $api_view_array['select_qty_style']; ?>">
                <div class="form-group" style="margin-bottom:0;">
                    <div style="display:inline-block;margin:auto; <?= $api_view_array['select_style']; ?>">        
                        <div class="<?= $api_view_array['label_qty_class']; ?>" style="float:left;padding-right:10px;line-height:34px; font-size:14px;">Quantity</div>
                        <div id="api_qty_box_product_featured_<?= $fp->id; ?>" style="float:left;padding-right:2px; width:60px; <?= $api_view_array['select_style']; ?>">
                            <select id="api_quantity_product_featured_<?= $fp->id; ?>" <?= $temp_sold_out[2]; ?> name="quantity" onchange="api_qty_box_more(<?= $fp->id; ?>,'product_featured');" class="form-control quantity-input" >
                                <?= $temp_sold_out[3]; ?><option value="2">2</option>
                                <option value="3">3</option><option value="4">4</option>
                                <option value="5">5</option>
                                <?= $api_view_array['select_option']; ?>
                                <option value="more">More</option>
                            </select>
                        </div>                                        
                    </div>        
                </div>        
            </div>        
            <div class="clearfix"></div>
            
            <div class="product-cart-button <?= $temp_sold_out[4]; ?> <?= $api_view_array['btn_class']; ?>">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i class="fa fa-heart-o"></i></button>
                    <button class="btn btn-theme add-to-cart" <?= $temp_sold_out[2]; ?> data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> <?= $api_view_array['label_btn']; ?></button>
                </div>
            </div>
            <?= $api_view_array['btn_break']; ?>            
            
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                            $k++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
                       
                       
<?php
    for ($i=0;$i<count($main_categories);$i++) {
        if (${'products_'.$main_categories[$i]['id']}) {
?>
			<div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg text-capitalize">
                            <?= $main_categories[$i]['name']; ?>
                        </h3>
                    </div>
                    <?php
                    if (count(${'products_'.$main_categories[$i]['id']}) > $api_view_array['carousel_col']) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" id="<?php echo 'carousel-'.$main_categories[$i]['id'].'_prev'; ?>" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" id="<?php echo 'carousel-'.$main_categories[$i]['id'].'_next'; ?>" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                
                <div id="<?php echo 'carousel-'.$main_categories[$i]['id']; ?>" class="carousel slide" data-ride="carousel" data-pause="hover">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if (${'products_'.$main_categories[$i]['id']}) {
                            $r = 0;
                            foreach (array_chunk(${'products_'.$main_categories[$i]['id']}, $api_view_array['carousel_col_row_2']) as $fps) {
                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>" style="min-height: 315px !important; max-height: 315px !important;">
                                    <div class="">
                                        <?php
                                        $k = 1;
                                        foreach ($fps as $fp) {
if (($k%2) != 0) $class_record = $api_view_array['col_class_odd']; else $class_record = $api_view_array['col_class_even'];                                            

$temp_sold_out = array();
$temp_sold_out[3] = '<option value="1">1</option>';              
if ($fp->quantity <= 0) {
    $temp_sold_out[0] = 'api_opacity_6';
    $temp_sold_out[1] = '
        <div class="api_absolute_center">
            <div class="api_sold_out_tag">
                Sold Out
            </div>
        </div>      
    ';
    $temp_sold_out[2] = 'disabled';
    $temp_sold_out[3] = '<option value="0">0</option>';
    $temp_sold_out[4] = 'api_display_none';
    $temp_sold_out[5] = $api_view_array['sorry'];    
}                                               
                                            ?>

<div class="page-products product-container <?= $api_view_array['col_class'].' '.$class_record; ?>">
    <div class="product api_margin_bottom_0 <?= $api_view_array['product_class']; ?>">
        <div class="product-top">
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
                    <img class="img-responsive <?= $temp_sold_out[0]; ?>" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                    <?= $temp_sold_out[1]; ?>
                </a>
            </div>
            <div class="product-desc <?= $api_view_array['title_class']; ?>" style="<?php echo $api_view_array['title_style']; ?>">
                <div class="product_name" style="font-weight:700;">
                    <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                </div>
                <p></p>
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price" style="<?= $api_view_array['price_style']; ?>">
                    <?php
                    if ($fp->promotion) {
                        echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price).'</del><br>';
                        echo $this->sma->convertMoney($fp->promo_price);
                    } else {
                        echo $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price);
                    }
                    ?>
                </div>
            <?php } ?>        
            
            <?= $api_view_array['price_class_break']; ?>
            <?= $temp_sold_out[5]; ?>
            <div class="product-rating <?= $temp_sold_out[4]; ?>" style="<?= $api_view_array['select_qty_style']; ?>">
                <div class="form-group" style="margin-bottom:0;">
                    <div style="display:inline-block;margin:auto; <?= $api_view_array['select_style']; ?>">        
                        <div class="<?= $api_view_array['label_qty_class']; ?>" style="float:left;padding-right:10px;line-height:34px; font-size:14px;">Quantity</div>
                        <div id="api_qty_box_product_featured_<?= $fp->id; ?>" style="float:left;padding-right:2px; width:60px; <?= $api_view_array['select_style']; ?>">
                            <select id="api_quantity_product_featured_<?= $fp->id; ?>" <?= $temp_sold_out[2]; ?> name="quantity" onchange="api_qty_box_more(<?= $fp->id; ?>,'product_featured');" class="form-control quantity-input" >
                                <?= $temp_sold_out[3]; ?><option value="2">2</option>
                                <option value="3">3</option><option value="4">4</option>
                                <option value="5">5</option>
                                <?= $api_view_array['select_option']; ?>
                                <option value="more">More</option>
                            </select>

                        </div>                                        
                    </div>        
                </div>        
            </div>        
            <div class="clearfix"></div>
            
            <div class="product-cart-button <?= $temp_sold_out[4]; ?> <?= $api_view_array['btn_class']; ?>">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i class="fa fa-heart-o"></i></button>
                    <button class="btn btn-theme add-to-cart" <?= $temp_sold_out[2]; ?> data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> <?= $api_view_array['label_btn']; ?></button>
                </div>
            </div>
            <?= $api_view_array['btn_break']; ?>            
            
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                            $k++;
                                        }
                                        ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
			
			
<?php
        }
    }
?>			
	   </div>
    </div>
</section>

<script>
let touchstartX = 0
let touchendX = 0

function handleGesure(id) {
  if (touchendX < (touchstartX - 100)) 
    $("#" + id + "_next").click();
  else 
    if (touchendX > (touchstartX + 100)) 
        $("#" + id + "_prev").click();
}
<?= $api_view_array['carousel_featured_script']; ?>
<?php echo $api_view_array['carousel_script']; ?>
</script>
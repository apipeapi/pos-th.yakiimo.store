<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" >
<?php
    if ($this->session->userdata('user_id')) {
        $temp_display_1 = "col-sm-9 col-md-10"; 
        $temp_display_2 = "col-sm-3 col-md-2 col-xs-12";
    }
    else {
        $temp_display_1 = "col-md-12"; 
        $temp_display_2 = "api_display_none";
    }
    if ($this->api_shop_setting[0]['require_login'] == 1) {
        $temp_display_11 = 'api_display_none';
    }
    else {
        $temp_display_11 = '';
    }

?>

<?php
if ($_GET['register'] != 1) {
    echo '
        <div class="row">
    ';
   
        echo '
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="'.base_url().'login" aria-controls="login" >
                        '.lang('login').'
                        </a>
                    </li>
                    
                    <li role="presentation" class="'.$temp_display_11.'">
                        <a id="api_register"  href="'.base_url().'login?register=1" aria-controls="register">
                            '.lang('registers').'
                        </a>
                    </li>
                </ul>
        ';
        


        echo '         
        <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
            <div role="tabpanel" class="tab-pane fade in active" id="login">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="well margin-bottom-no">
        ';
            include('login_form_2.php');
            echo $temp_display_login_form;
            
        echo '
                        </div>
                    </div>
        ';
      
                echo '
                    <div class="col-sm-6 '.$temp_display_11.'">
                        <div class="api_height_15 visible-sm visible-xs"></div>
                        <h4 class="title">
                            <span>'.lang('register_new_account').'</span>
                        </h4>
                        <p>
                            '.lang('register_account_info').'
                        </p>
                    </div>
                ';
        echo '
                    <div class="api_clear_both"></div>
                </div>
            </div>
        </div>
        ';

    echo '
        </div>
    ';
}
else {
    echo '
        <div class="row">
    ';
        echo '
            <div class="col-md-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation">
                        <a href="'.base_url().'login" aria-controls="login" >
                        '.lang('login').'
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a id="api_register"  href="'.base_url().'login?register=1" aria-controls="register">
                            '.lang('registers').'
                        </a>
                    </li>
                </ul>
        ';

        echo '         
        <div class="tab-content padding-lg white bordered-light" style="margin-top:-1px;">
            <div role="tabpanel" class="tab-pane fade in active" id="login">
                <div class="row">
                    <div class="col-sm-12">
        ';
?>
    <?php $attrib = array('class' => 'validate', 'role' => 'form');
    echo form_open("register", $attrib); ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('first_name', 'first_name'); ?>
                <div class="controls">
                    <?= form_input('first_name', '', 'class="form-control" id="first_name" required="required" pattern=".{3,10}"'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('last_name', 'last_name'); ?>
                <div class="controls">
                    <?= form_input('last_name', '', 'class="form-control" id="last_name" required="required"'); ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group <?= $temp_display_1a; ?>">
                <?= lang('company', 'company'); ?>
                <?= form_input('company', set_value('company'), 'class="form-control tip" id="company" '); ?>
            </div>
            <?= $temp_display_2a; ?>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('phone', 'phone'); ?>
                <div class="controls">
                    <?= form_input('phone', '', 'class="form-control" id="phone" required="required"'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= lang('Delivery_Address', 'address'); ?>
                <?= form_input('address', '', 'class="form-control tip" id="address" required="required" autocompleted="off"'); ?>
            </div>
        </div>  
        <div class="col-md-6">
            <div class="form-group">
                <?= lang("City", "City"); ?>
                <?php
                    $config_data = array(
                        'none_label' => lang("Select_a_city"),
                        'table_name' => 'sma_city',
                        'space' => ' &rarr; ',
                        'strip_id' => '',        
                        'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                        'condition' => 'order by title_'.$this->api_shop_setting[0]['api_lang_key'].' asc',
                        'condition_parent' => ' and parent_id = 268',
                        'translate' => 'yes',
                        'no_space' => 1,
                    );                        
                    $this->site->api_get_option_category($config_data);
                    $temp_option = $_SESSION['api_temp'];
                    for ($i=0;$i<count($temp_option);$i++) {                        
                        $temp = explode(':{api}:',$temp_option[$i]);
                        $temp_10 = '';
                        if ($temp[0] != '') {
                            $config_data_2 = array(
                                'id' => $temp[0],
                                'table_name' => 'sma_city',
                                'field_name' => 'title_'.$this->api_shop_setting[0]['api_lang_key'],
                                'parent_id' => '268',
                                'translate' => 'yes',
                            );
                            $_SESSION['api_temp'] = array();
                            $this->site->api_get_category_arrow($config_data_2);          
                            for ($i2 = count($_SESSION['api_temp']) - 1; ; $i2--) {
                                if ($i2 == 0) {
                                    break;
                                }
                                $temp_arrow = '';
                                if ($i2 > 1)
                                    $temp_arrow = ' &rarr; ';
                                $temp_10 .= $_SESSION['api_temp'][$i2].$temp_arrow;
                            }   
                        }
                        $tr_city[$temp[0]] = $temp_10.$temp[1];
                    }
                    echo form_dropdown('city_id', $tr_city, $customer->city_id, 'class="form-control" required="required"');
                ?>
            </div>
        </div>
        <div class="col-md-6 api_display_none">
            <div class="form-group">
                <?= lang('country', 'country'); ?>
                <?= form_input('country', 'Cambodia', 'class="form-control tip" id="country"'); ?>
            </div>
        </div>                                                                       
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('email', 'email'); ?>
                <div class="controls">
                    <input type="email" id="email" name="email" class="form-control" required="required"/>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('username', 'username'); ?>
                <?= form_input('username', set_value('username'), 'class="form-control tip" id="username" required="required"'); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('password', 'passwordr'); ?>
                <div class="controls">
                    <?= form_password('password', '', 'class="form-control tip" id="passwordr" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                    <span class="help-block"><?= lang('password_hint'); ?></span>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <?= lang('confirm_password', 'password_confirm'); ?>
                <div class="controls">
                    <?= form_password('password_confirm', '', 'class="form-control" id="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
<?php
    echo '
        <button type="button" class="btn btn-default" onclick="
        var postData = {
            \'website_address\' : \''.base_url().'\',
            \'close\' : \''.lang('Close').'\',
            \'use_this_password\' : \''.lang('use_this_password').'\',
            \'generated_password\' : \''.lang('generated_password').'\',
        };        
        api_generate_password(postData);
        ">
            '.lang('Generate_Password').'
        </button>
        <div class="api_height_30"></div>
    ';

    echo '
        <div class="col-sm-6 api_padding_0">
            <div class="api_height_15"></div>
            '.form_submit('register', lang('registers'), 'class="btn btn-primary"').'
        </div>  
    ';

    if ($this->api_shop_setting[0]['facebook_login'] == 1) {
        echo '
        <div class="col-sm-6 api_padding_top_15">
            <div class="api_text_align_center api_link_box_none">
                <div class="fb-login-button" data-width="" data-size="large" data-button-type="login_with" data-layout="rounded" data-auto-logout-link="false" data-use-continue-as="true" scope="public_profile,email" onlogin="checkLoginState();" ></div>
            </div>
        </div>
        ';
    }

    echo form_close(); 

?>

<?php


        echo '
                    </div>
                </div>
            </div>
        </div>
        ';

    echo '
        </div>
    ';
}


?>



<div class="<?= $temp_display_2 ?>">
    <?php
    if ($this->session->userdata('user_id') > 0)
        include('themes/default/shop/views/pages/sidebar2.php'); 
    ?>
</div>

            </div>
        </div>
    </div>
</section>




<script>
function api_use_this_password(){
    document.getElementById('passwordr').value = document.getElementById('api_modal_body').innerHTML;
    document.getElementById('password_confirm').value = document.getElementById('api_modal_body').innerHTML;
}


$(document).ready(function(){
    function alignModal(){
        var modalDialog = $(this).find("#api_modal");
        /* Applying the top margin on modal dialog to align it vertically center */
        modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
    }
    // Align modal when it is displayed
    $("#api_modal").on("shown.bs.modal", alignModal);
    
    // Align modal when user resize the window
    $(window).on("resize", function(){
        $("#api_modal:visible").each(alignModal);
    });   
    alignModal();

});

</script>
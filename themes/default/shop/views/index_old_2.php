<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
$useragent=$_SERVER['HTTP_USER_AGENT'];
$api_view_array = array();
if ($this->session->userdata('api_mobile') == 1) {
    $api_view_array['col_class'] = 'col-md-4 col-sm-6 col-xs-6';
    $api_view_array['col_class_odd'] = 'api_padding_right_7';
    $api_view_array['col_class_even'] = 'api_padding_left_7';
    $api_view_array['product_class'] = 'api_padding_0';
    $api_view_array['title_class'] = 'api_padding_5';
    $api_view_array['title_style'] = 'min-height:50px; max-height: 50px !important; overflow:hidden;';
    $api_view_array['quantity_class'] = 'api_display_none';
    $api_view_array['price_class_break'] = '<div class="api_clear_both"></div>';
    $api_view_array['price_style'] = 'float: left !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important; text-align: left !important; width: 100% !important;';
    $api_view_array['select_qty_style'] = 'width: 100% !important; padding-bottom:0px; padding-left:5px !important; padding-right:5px !important;';
    $api_view_array['label_qty_class'] = 'api_display_none';
    $api_view_array['select_style'] = 'width:100% !important;';
    $api_view_array['btn_class'] = 'api_padding_right_5 api_padding_left_5';
    $api_view_array['btn_break'] = '<div class="api_clear_both api_height_5"></div>';
    $api_view_array['label_btn'] = lang('Add');
    $api_view_array['carousel_col'] = 2;
    $api_view_array['carousel_col_row'] = 2;
    $api_view_array['carousel_col_row_2'] = 2;
    $api_view_array['select_option'] = '';
    
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5 api_padding_0" style=" overflow:hidden;">
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>   
    ';
    
    $api_view_array['carousel_featured_script'] = '
$("#carousel-example").carousel({
  interval: 1000 * 15
});
$(".api_pause_carousel").on("click touchstart", function() {
    $("#carousel-example").carousel("pause");
});
document.getElementById("carousel-example").addEventListener("touchstart", e => {
  touchstartX = e.changedTouches[0].screenX
})
document.getElementById("carousel-example").addEventListener("touchend", e => {
  touchendX = e.changedTouches[0].screenX
  handleGesure("carousel-example")
})
    ';
    if (is_array($main_categories)) {
        for ($i=0;$i<count($main_categories);$i++) {
            if (${'products_'.$main_categories[$i]['id']}) {
                $api_view_array['carousel_script'] .= '
            
$("#carousel-'.$main_categories[$i]['id'].'").carousel({
  interval: 1000 * 15
});
$(".api_pause_carousel_" + "'.$main_categories[$i]['id'].'").on("click touchstart", function() {
    $("#carousel-'.$main_categories[$i]['id'].'").carousel("pause");
});
document.getElementById("carousel-'.$main_categories[$i]['id'].'").addEventListener("touchstart", e => {
  touchstartX = e.changedTouches[0].screenX
})
document.getElementById("carousel-'.$main_categories[$i]['id'].'").addEventListener("touchend", e => {
  touchendX = e.changedTouches[0].screenX
  handleGesure("carousel-'.$main_categories[$i]['id'].'")
})

            ';
            }
        }
    }
} else {
    $api_view_array['title_style'] = 'min-height:40px; max-height: 40px !important; overflow:hidden;';
    $api_view_array['col_class'] = 'col-md-4 col-sm-6';
    $api_view_array['label_btn'] = lang('add_to_cart');
    $api_view_array['carousel_col'] = 3;
    $api_view_array['carousel_col_row'] = 3;
    $api_view_array['carousel_col_row_2'] = 3;
    $api_view_array['carousel_script'] = '';
    $api_view_array['select_option'] = '<option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
    $api_view_array['sorry'] = '
        <div class="col-md-12 api_padding_bottom_5" style="max-height:72px; overflow:hidden;">
            <div class="" style="color: #f44336 !important; height:57px; font-weight: 700; text-align:center; visibility:hidden">
                Sorry for inconvenient
            </div>         
            <div class="" style="color: #f44336 !important; font-weight: 700; text-align:center;">
                Sorry for inconvenient
            </div>
        </div>    
    ';
}
?>
<?php
    if ($this->api_shop_setting[0]['front_slider'] != 1 || $this->api_shop_setting[0]['product_import_date'] == 1) {
        $slider = array();

        if ($this->session->userdata('api_selected_import_date') != '' && base_url() == 'https://air.phsarjapan.com/') {
            $temp = $this->shop_model->api_display_import_date($this->api_shop_setting[0]['product_import_date_list']);
            echo '
                <div class="container-fluid" style="background-color:#f8f8f8 !important;">
                    <div class="container api_padding_0">
                        <br>
                        <div class="col-md-12 api_padding_0_mobile">
                            '.$temp['display'].'
                        </div>
                    </div>
                </div>            
            ';
        }
    }
?>


<?php
//Slide===============================================================================
?>   
<?php if (!empty($slider)) { ?>
<section class="slider-container">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-pause="hover">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (is_file('assets/uploads/'.$slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                            $sr++;
                        }
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" alt="">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                        }
                        $sr++;
                    }
                    ?>
                </div>
<!--
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?//= lang('next'); ?></span>
                </a>
-->
            </div>
        </div>
    </div>
</section>
<?php } ?>
<?php
//Slide===============================================================================
?>   






<?php
    include 'themes/default/shop/views/sub_page/api_carousel.php';
?>




<?php
    if (is_array($main_categories)) {
        for ($i=0;$i<count($main_categories);$i++) {
            if (${'products_'.$main_categories[$i]['id']}) {
                ?>
			<div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-9">
                        <h3 class="margin-top-no text-size-lg text-capitalize iphearum">
                        <?php
                            if (count(${'products_'.$main_categories[$i]['id']}) > 0) {
                                for ($i_3=0;$i_3<count(${'products_'.$main_categories[$i]['id']});$i_3++) {
                                    $config_data = array(
                                        'id' => ${'products_'.$main_categories[$i]['id']}[$i_3]->id,
                                    );
                                    $temp_hide = $this->shop_model->api_get_product_display($config_data);
                                    if ($temp_hide['result'] == 'hide') {
                                        unset(${'products_'.$main_categories[$i]['id']}[$i_3]);
                                    }
                                }
                                $temp_arr = array_values(${'products_'.$main_categories[$i]['id']});
                                ${'products_'.$main_categories[$i]['id']} = $temp_arr;
                            }
                if (count(${'products_'.$main_categories[$i]['id']}) > 0) {
                    echo $main_categories[$i]['name'];
                } ?>                            
                        </h3>
                    </div>
                    <?php
                    if (count(${'products_'.$main_categories[$i]['id']}) > $api_view_array['carousel_col']) {
                        ?>
                        <div class="col-xs-3">
                            <div class="controls pull-right">
                                <a class="left fa fa-chevron-left btn btn-xs btn-default" id="<?php echo 'carousel-'.$main_categories[$i]['id'].'_prev'; ?>" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="prev"></a>
                                <a class="right fa fa-chevron-right btn btn-xs btn-default" id="<?php echo 'carousel-'.$main_categories[$i]['id'].'_next'; ?>" href="<?php echo '#carousel-'.$main_categories[$i]['id']; ?>"
                                data-slide="next"></a>
                            </div>
                        </div>
                        <?php
                    } ?>
                </div>
                
                <div id="<?php echo 'carousel-'.$main_categories[$i]['id']; ?>" class="carousel slide" data-ride="carousel" data-pause="hover">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        if (${'products_'.$main_categories[$i]['id']}) {
                            $r = 0;
                            foreach (array_chunk(${'products_'.$main_categories[$i]['id']}, $api_view_array['carousel_col_row_2']) as $fps) {
                                ?>
                                <div class="item row <?= empty($r) ? 'active' : ''; ?>" style="min-height: 315px !important; max-height: 315px !important;">
                                    <div class="">
                                        <?php
                                        $k = 1;
                                foreach ($fps as $fp) {
                                    if (($k%2) != 0) {
                                        $class_record = $api_view_array['col_class_odd'];
                                    } else {
                                        $class_record = $api_view_array['col_class_even'];
                                    }

                                    $temp_sold_out = array();
                                    $temp_sold_out[3] = '<option value="1">1</option>';
                                    if ($this->api_shop_setting[0]['out_of_stock'] == 1) {
                                        if ($fp->quantity <= 0) {
                                            $temp_sold_out[0] = 'api_opacity_6';
                                            $temp_sold_out[1] = '
            <div class="api_absolute_center">
                <div class="api_sold_out_tag">
                    Sold Out
                </div>
            </div>      
        ';
                                            $temp_sold_out[2] = 'disabled';
                                            $temp_sold_out[3] = '<option value="0">0</option>';
                                            $temp_sold_out[4] = 'api_display_none';
                                            $temp_sold_out[5] = $api_view_array['sorry'];
                                        }
                                    } ?>

<div class="page-products product-container <?= $api_view_array['col_class'].' '.$class_record; ?>">
    <div class="product api_margin_bottom_0 <?= $api_view_array['product_class']; ?>">
        <div class="product-top">
            <div class="product-image">
                <a href="<?= site_url('product/'.$fp->slug); ?>">
<?php
    if (is_file('assets/uploads/'.$fp->image)) {
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/'.$fp->image).'" alt="">
        ';
    } else {
        echo '
            <img class="img-responsive '.$temp_sold_out[0].'" src="'.base_url('assets/uploads/no_image.png').'" alt="">
        ';
    }
                                    echo $temp_sold_out[1]; ?>                    
                </a>
            </div>
            <div class="product-desc <?= $api_view_array['title_class']; ?>" style="<?php echo $api_view_array['title_style']; ?>">
                <div class="product_name" style="font-weight:700;">
                    <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                </div>
                <p></p>
            </div>            
        </div>
        <div class="clearfix"></div>
             
        <div class="product-bottom" style="padding-top: 0px; margin-top:0px;">

            <?php if (!$shop_settings->hide_price) { ?>
                <div class="product-price" style="<?= $api_view_array['price_style']; ?> width: 100% !important;" >
                    <?php
                        $config_data = array(
                            'id' => $fp->id,
                            'customer_id' => $this->session->userdata('company_id'),
                        );
                        $temp = $this->site->api_calculate_product_price($config_data);
                        
                        if ($this->sma->isPromo_v3($fp)) {
                            echo '
                                <del class="text-red">'.$this->sma->convertMoney($temp['temp_price']).'</del> 
                                '.$this->sma->convertMoney($temp['price']).'
                            ';
                        } else {
                            echo $this->sma->convertMoney($temp['price']);
                        }
                    ?>
                </div>
            <?php } ?>        
            <div class="api_clear_both"></div>
            <?= $api_view_array['price_class_break']; ?>
            <?= $temp_sold_out[5]; ?>
            <div class=" <?= $temp_sold_out[4]; ?>" style="<?= $api_view_array['select_qty_style']; ?>" style="width: 100% !important;">
                <div class="form-group" style="margin-bottom:5px;">
                    <div class="input-group">                
                    
<span class="input-group-addon pointer btn-minus api_pause_carousel_<?php echo $main_categories[$i]['id']; ?>"><span class="fa fa-minus"></span></span>
<input type="tel" name="quantity" class="form-control text-center api_numberic_input quantity-input api_pause_carousel_<?php echo $main_categories[$i]['id']; ?>" value="1" required="required">
<span class="input-group-addon pointer btn-plus api_pause_carousel_<?php echo $main_categories[$i]['id']; ?>"><span class="fa fa-plus"></span></span>
                    </div>
                </div>        
            </div>        
            <div class="clearfix"></div>
<?php
    if ($fp->is_favorite == 1) {
        $temp_is_favorite = 'fa-heart';
    } else {
        $temp_is_favorite = 'fa-heart-o';
    } ?>             
            <div class="product-cart-button <?= $temp_sold_out[4]; ?> <?= $api_view_array['btn_class']; ?>">
                <div class="btn-group" role="group" aria-label="...">
                    <button class="btn btn-info add-to-wishlist" data-id="<?= $fp->id; ?>"><i id="wishlist_heart_<?php echo $fp->id; ?>" class="fa <?php echo $temp_is_favorite; ?>"></i></button>
                    <button class="btn btn-theme add-to-cart" <?= $temp_sold_out[2]; ?> data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> <?= $api_view_array['label_btn']; ?></button>
                </div>
            </div>
            <?= $api_view_array['btn_break']; ?>            
            
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>                                                    

                                            <?php
                                            $k++;
                                } ?>
                                    </div>
                                </div>
                                <?php
                                $r++;
                            }
                        } ?>
                    </div>
                </div>
            </div>
			
			
<?php
            }
        }
    }
?>			
	   </div>
    </div>
</section>

<script>
let touchstartX = 0
let touchendX = 0

function handleGesure(id) {
  if (touchendX < (touchstartX - 100)) 
    $("#" + id + "_next").click();
  else 
    if (touchendX > (touchstartX + 100)) 
        $("#" + id + "_prev").click();
}
<?= $api_view_array['carousel_featured_script']; ?>
<?php echo $api_view_array['carousel_script']; ?>
</script>


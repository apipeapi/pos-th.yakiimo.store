<?php
$temp_display = '';
if ($this->session->userdata('api_order_send_mail') != '') {
    if ($this->session->userdata('api_order_send_mail') == 'send') {
        $temp_display = '
    <script>
        $("#api_swal3-question").hide();
        $("#api_swal3-warning").hide();
        $("#api_swal3-info").hide();
        $("#api_swal3-success").show();
        $("#api_modal_2_title").html(\'<div class="api_font_size_24">'.lang('Your Order Is Accepted.').'</div>\');
        $("#api_modal_2_body").html(\'<div class="api_padding_top_15 api_font_size_16">'.lang('text_order_accepted').'<br>'.lang('text_order_accepted_2').'<br>'.lang('text_order_accepted_3').'</div>\');

        $("#api_modal_2_btn_ok").show();
        $("#api_modal_2_btn_close").hide();
        $(".api_swal3-spacer").show();

        $("#api_modal_2_btn_ok").html("<button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled btn-success api_link_box_none\' data-dismiss=\'modal\' style=\'\'>'.lang('Okay').'</button>");    
        $("#api_modal_2_trigger").click();
    </script>
        ';
    }
    else {
        $temp_display = '
    <script>
        $("#api_swal3-question").hide();
        $("#api_swal3-warning").show();
        $("#api_swal3-info").hide();
        $("#api_swal3-success").hide();
        $("#api_modal_2_title").html("'.lang('Your Order Is Accepted.').'");
        $("#api_modal_2_body").html(\'<div class="api_padding_top_15">'.lang('We can not sent your order details to your email.').'</div>\');>

        $("#api_modal_2_btn_ok").show();
        $("#api_modal_2_btn_close").hide();
        $(".api_swal3-spacer").show();

        $("#api_modal_2_btn_ok").html("<button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled btn-warning api_link_box_none\' data-dismiss=\'modal\' style=\'\'>'.lang('Okay').'</button>");    
        $("#api_modal_2_trigger").click();
    </script>
        ';
    }
}
echo $temp_display;
$this->session->set_userdata('api_order_send_mail', '');  


?>
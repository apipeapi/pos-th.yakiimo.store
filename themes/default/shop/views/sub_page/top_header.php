<style>
    #top{
        height: 34px;
        box-shadow: 0px -1px 1px 1px;
        margin-bottom: 3px;
    }
    #top ul li a{
        color:white;
    }
</style>
<nav id="top" class="bg-blue">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-4 hidden-xs" style="top:10px">
                <ul class="list-inline list-unstyled head-social text-white bg-pink">
                    <li>
                        <a href="https://aeononlineshopping.com/index.php?route=account/register"><i class="fa fa-address-book-o"></i> Register</a>
                    </li>
                    <li>
                        <a href="https://aeononlineshopping.com/index.php?route=account/login">
                        <i lass="fa fa-sign-in"></i> Login</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
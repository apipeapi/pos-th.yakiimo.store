
<?php

$temp_display = '';

$form_name = 'frm_set_information';
$attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => $form_name, 'name' => $form_name);
$temp_display .= form_open_multipart("main/api_facebook_register", $attrib);

$temp_display .= '
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("name", "name").'
                    '.form_input('name', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("phone", "phone").'
                    '.form_input('phone', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>
';

if ($this->session->userdata('api_facebook_login_email') == '')
$temp_display .= '
        <div class="col-md-12">
            <div class="form-group">
                <div class="form-group">
                    '.lang("email", "email").'
                    '.form_input('email', '', 'class="form-control" required="required"').'
                </div>
            </div>
        </div>        
';

$temp_display .= '
        <div class="col-md-12">
            <div class="form-group">
                '.lang("Delivery_Address", "address").'
                '.form_textarea('address', '', 'class="form-control" required="required" style="height: 100px;"').'
            </div>
        </div>                
    </div>
';

echo '
<button type="button" id="api_modal_set_information_trigger" class="api_display_none" data-backdrop="static" data-toggle="modal" data-target="#api_modal_set_information">Open Modal</button>
<!-- Modal -->
<div id="api_modal_set_information" class="modal fade" role="dialog" style="margin-top:100px;">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title api_modal_title">
            '.lang('Please_provide_your_information_for_our_delivery_service').'
        </h4>
      </div>
      <div class="modal-body">
        '.$temp_display.'
      </div>
      <div class="modal-footer">
        '.form_submit('submit', lang('Login'), 'class="btn btn-primary"').'
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            '.lang('Cancel').'
        </button>        
      </div>
    </div>
  </div>
</div>
';

echo form_close();

if ($this->session->userdata('api_facebook_login_email') != '' && $_GET['api_action'] == 'need_more_informations#_=_' || $_GET['api_action'] == 'need_more_informations')
    echo '
<script>
    document.frm_set_information.name.value = "'.$this->session->userdata('api_facebook_login_email').'";
    $("#api_modal_set_information_trigger").click();
</script>    
    ';
?>
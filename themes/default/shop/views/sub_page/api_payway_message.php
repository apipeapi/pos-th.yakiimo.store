<?php
$temp_display = '';
if ($this->session->userdata('api_payment_response')['api_payment_type'] == 'payway') {
    if ($this->session->userdata('api_payment_response')['status'] == 0) {
        $temp_display = '
            <script>
                $("#api_swal3-question").hide();
                $("#api_swal3-warning").hide();
                $("#api_swal3-info").hide();
                $("#api_swal3-success").show();
                $("#api_modal_2_title").html(\'<div class="api_font_size_24">'.lang('payway_title').'</div>\');
                $("#api_modal_2_body").html(\'<div class="api_padding_top_15 api_font_size_16">'.lang('Your payment was processed succesfully').'</div><div class="api_padding_top_20 api_padding_bottom_20"><b>'.number_format($inv->grand_total,2).' '.$default_currency->code.'</b></div><div class="api_font_size_16">'.lang('Paid to').' <img width="40" src="'.base_url().'assets/images/payway_merchant_logo.png" /></div>\');

                $("#api_modal_2_btn_ok").show();
                $("#api_modal_2_btn_close").hide();
                $(".api_swal3-spacer").show();

                $("#api_modal_2_btn_ok").html("<button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled btn-success api_link_box_none\' data-dismiss=\'modal\' style=\'\'>'.lang('Okay').'</button>");    
                $("#api_modal_2_trigger").click();
            </script>
        ';
    }
    else {
        if ($this->session->userdata('api_payment_response')['api_payment_cancel'] != 1) {
            $temp_display = '
                <script>
                    $("#api_swal3-question").hide();
                    $("#api_swal3-warning").show();
                    $("#api_swal3-info").hide();
                    $("#api_swal3-success").hide();
                    $("#api_modal_2_title").html(\'<div class="api_font_size_24">'.lang('Payment Failed').'</div>\');
                    $("#api_modal_2_body").html(\'<div class="api_padding_top_15 api_font_size_16">'.lang('Your payment was not succesfully processed').'</div></div>\');

                    $("#api_modal_2_btn_ok").show();
                    $("#api_modal_2_btn_close").hide();
                    $(".api_swal3-spacer").show();

                    $("#api_modal_2_btn_ok").html("<button type=\'button\' role=\'button\' tabindex=\'0\' class=\'api_swal3-cancel api_swal3-styled btn-warning api_link_box_none\' data-dismiss=\'modal\' style=\'\'>'.lang('Okay').'</button>");    
                    $("#api_modal_2_trigger").click();
                </script>
            ';
        }
    }
}
echo $temp_display;
$data_session = array();
$this->session->set_userdata('api_payment_response', $data_session);  
$this->session->set_userdata('api_order_send_mail', '');  


?>
<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
    width: 70%;
    margin: auto;
  }
</style>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
            <?php
            // $item = '<div class="item">';
            // $sart = 0;
            // $end = 7;
            foreach ($list_categories as $key => $value) {
                // if ($value['display']!="no") {
                    // if ($key>=$start && $key=$end) {
                    //     echo $item;
                    //     $start = $end;
                    //     $end = $key;
                    // }
                    $value['image']!=""?$image = base_url("assets/uploads/".$value["image"].'"'):$image = base_url('assets/uploads/no_image.png') ?>
                    <div class="item">
                    <li class="category-element <?=$start.'-'.$end.'-'.$key?>">
                        <a class="category-link" href="<?=base_url('category/'.$value['slug'])?>">
                            <div class="category-image-spacer">
                                <div class="category-image rounded" style="background-image: url(<?=$image?>)">
                                    <div class="image-background" style="opacity: 0;"></div>
                                    <img src="<?=$image?>" alt="" >
                                </div>
                                <noscript aria-hidden="true">
                                    <div class="category-image" style="background-image: url('<?=$image?>')">
                                        <img src="<?=$image?>" alt="">
                                    </div>
                                </noscript>
                            </div>
                            <h5 class="category-name"><?=lang($value['name'])?></h5>
                        </a>
                    </li>
                    </div>
                <?php
                // }
            }
        ?>
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
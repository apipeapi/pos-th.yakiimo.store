<?php
echo '
<style>
.payment_credit_card{
    font-size:16px;
    color:#555555;
    font-weight:600;
}
.payment_credit_card_2{
    font-size:15px;
    color:#c2c2c2;        
}
.credit_card_image_1{
    width:55px;
    border-radius:10px;
}
.credit_card_image_2{    
    padding:10px;
}

.api_payment_box{
    margin-left:5px;
    margin-right:5px;
    margin-bottom:15px;
    width:105px;
    height:88px;
    border:0px solid red;
    display:inline-block;
}
.api_payment_box_table{
    border:1px solid #c4c4c4;    
    cursor:pointer;
}
.api_payment_box_table_selected{
    border:1px solid #FFCC33;    
    cursor:pointer;
}
.api_payment_box_table:hover{
    border:1px solid #f30000 !important;    
}
</style>
';
if ($this->api_shop_setting[0]['air_base_url'] == base_url())
    $temp = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_gateways_details"
        ,"payment_gateway_id = 6 order by id asc"
        ,"arr"
    );
else 
    $temp = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_gateways_details"
        ,"payment_gateway_id = 5 order by id asc"
        ,"arr"
    );


if ($temp[0]['value'] == 'sandbox') {
    $url = $temp[3]['value'];
    echo '
        <link rel="stylesheet" href="https://payway-staging.ababank.com/checkout-popup.html?file=css"/>
        <script src="https://payway-staging.ababank.com/checkout-popup.html?file=js"></script>
    ';
}
if ($temp[0]['value'] == 'live') {
    $url = $temp[4]['value'];
    echo '
        <link rel="stylesheet" href="https://payway.ababank.com/checkout-popup.html?file=css"/>
        <script src="https://payway.ababank.com/checkout-popup.html?file=js"></script>
    ';
}

$temp_user = array();
if ($this->session->userdata('user_id') > 0) {
    $temp_user = $this->site->api_select_some_fields_with_where("
        *     
        "
        ,"sma_users"
        ,"id = ".$this->session->userdata('user_id')
        ,"arr"
    );
}

$payway_grand_total = $grand_total - $this->api_shop_setting[0]['shipping'];

echo '
<div id="aba_main_modal" class="aba-modal">
    <div class="aba-modal-content">
        <form method="POST" target="aba_webservice" action="'.$url.'" id="aba_merchant_request" name="api_form_payway">
            <input type="hidden" name="hash" value="'.$hash.'" id="hash"/>
            <input type="hidden" name="tran_id" value="'.$sale_id.'" id="tran_id"/><br/>
            <input type="hidden" name="amount" value="'.$payway_grand_total.'" id="amount"/><br/>
            <input type="hidden" name="firstname" value="'.$temp_user[0]['first_name'].'"/><br/>
            <input type="hidden" name="lastname" value="'.$temp_user[0]['last_name'].'"/><br/>
            <!-- <input type="hidden" name="phone" value="'.$temp_user[0]['phone'].'"/><br/> -->
            <input type="hidden" name="email" value="'.$temp_user[0]['email'].'"/><br/>
            <input type="hidden" name="shipping" value="'.$this->api_shop_setting[0]['shipping'].'"/>
            <input type="hidden" name="payment_option" value="'.$temp_payment_option.'"/>
            <!-- <input type="hidden" name="type" value="PreAuth"> -->
        </form>
    </div>
</div>        
';

?>
